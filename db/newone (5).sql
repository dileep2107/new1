-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 17, 2020 at 03:20 AM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newone`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_emp_access`
--

DROP TABLE IF EXISTS `admin_emp_access`;
CREATE TABLE IF NOT EXISTS `admin_emp_access` (
  `index_id` int(10) NOT NULL AUTO_INCREMENT,
  `emp_id` int(10) DEFAULT NULL,
  `org_id` int(10) NOT NULL,
  `module_id` int(10) NOT NULL,
  `submodule_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`index_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_emp_access`
--

INSERT INTO `admin_emp_access` (`index_id`, `emp_id`, `org_id`, `module_id`, `submodule_id`, `date`, `time`) VALUES
(17, 1, 0, 3, 8, '2020-02-13', '08:24:02'),
(16, 1, 0, 1, 10, '2020-02-13', '08:24:02'),
(15, 1, 0, 1, 1, '2020-02-13', '08:24:02'),
(18, 1, 0, 3, 9, '2020-02-13', '08:24:02');

-- --------------------------------------------------------

--
-- Table structure for table `admin_module`
--

DROP TABLE IF EXISTS `admin_module`;
CREATE TABLE IF NOT EXISTS `admin_module` (
  `module_id` int(10) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` enum('active','old') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_module`
--

INSERT INTO `admin_module` (`module_id`, `module_name`, `date`, `time`, `status`) VALUES
(1, 'Registration', '2019-02-24', '25:20:00', 'active'),
(2, 'Public Content', '2019-02-21', '43:00:00', 'old'),
(3, 'User', '2019-02-22', '22:00:00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `admin_submodule`
--

DROP TABLE IF EXISTS `admin_submodule`;
CREATE TABLE IF NOT EXISTS `admin_submodule` (
  `submodule_id` int(10) NOT NULL AUTO_INCREMENT,
  `FK_module_id` int(10) NOT NULL,
  `submodule_name` varchar(200) NOT NULL,
  `link` varchar(5000) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` enum('active','old') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`submodule_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_submodule`
--

INSERT INTO `admin_submodule` (`submodule_id`, `FK_module_id`, `submodule_name`, `link`, `date`, `time`, `status`) VALUES
(1, 1, 'Registration', 'admin/registration', '2019-02-23', '15:08:00', 'active'),
(2, 1, 'Category', 'admin/blog/category', '2019-02-13', '29:00:00', 'old'),
(3, 1, 'Blog View', 'admin/blog/blog_view', '2018-04-20', '04:00:00', 'old'),
(4, 1, 'Blog Comment View', 'admin/blog/blog_comment_view', '2018-04-20', '25:20:00', 'old'),
(5, 2, 'Public Content', 'admin/configure_access/public_content', '2019-02-12', '13:00:00', 'active'),
(6, 2, 'Public Gallery', 'admin/configure_access/add_gallery_with_link', '2019-02-22', '23:00:00', 'active'),
(7, 2, 'Gallery', 'admin/configure_access/add_gallery', '2019-02-22', '11:00:00', 'active'),
(8, 3, 'Add User', 'admin/profile/index', '2019-02-20', '08:00:00', 'active'),
(9, 3, 'User Profile', 'admin/Profile/user_profile_list', '2019-02-20', '08:00:00', 'active'),
(10, 1, 'Candidate list', 'admin/registration/candidate_list', '2019-02-23', '15:08:00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `basic_foundation_course`
--

DROP TABLE IF EXISTS `basic_foundation_course`;
CREATE TABLE IF NOT EXISTS `basic_foundation_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `first_attempt_test` text,
  `second_attempt_test` text,
  `third_attempt_test` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_foundation_course`
--

INSERT INTO `basic_foundation_course` (`id`, `candidate_id`, `first_attempt_test`, `second_attempt_test`, `third_attempt_test`) VALUES
(7, 7, '[{\"name_trg\":\"Hindi\",\"basic_course_date\":\"2020-02-18\",\"basic_course_mark\":\"gv\",\"basic_course_result\":\"gvg\",\"basic_course_sign\":\"vgv\"},{\"name_trg\":\"English\",\"basic_course_date\":\"2020-02-25\",\"basic_course_mark\":\"gv\",\"basic_course_result\":\"gv\",\"basic_course_sign\":\"vgvg\"},{\"name_trg\":\"Maths\",\"basic_course_date\":\"2020-02-18\",\"basic_course_mark\":\"gv\",\"basic_course_result\":\"gv\",\"basic_course_sign\":\"gvgv\"},{\"name_trg\":\"GA\",\"basic_course_date\":\"2020-02-24\",\"basic_course_mark\":\"gvgv\",\"basic_course_result\":\"gv\",\"basic_course_sign\":\"vg\"},{\"name_trg\":\"G\'Sc\",\"basic_course_date\":\"2020-02-26\",\"basic_course_mark\":\"fg\",\"basic_course_result\":\"hfh\",\"basic_course_sign\":\"fgf\"},{\"name_trg\":\"OIM\",\"basic_course_date\":\"2020-02-19\",\"basic_course_mark\":\"fg\",\"basic_course_result\":\"gf\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"MH\",\"basic_course_date\":\"2020-02-25\",\"basic_course_mark\":\"fggf\",\"basic_course_result\":\"gf\",\"basic_course_sign\":\"gh\"}]', '[{\"name_trg\":\"Hindi\",\"basic_course_date\":\"2020-02-26\",\"basic_course_mark\":\"gh\",\"basic_course_result\":\"gf\",\"basic_course_sign\":\"gfhgf\"},{\"name_trg\":\"English\",\"basic_course_date\":\"2020-02-27\",\"basic_course_mark\":\"gf\",\"basic_course_result\":\"gf\",\"basic_course_sign\":\"gfhg\"},{\"name_trg\":\"Maths\",\"basic_course_date\":\"2020-02-12\",\"basic_course_mark\":\"g\",\"basic_course_result\":\"ghh\",\"basic_course_sign\":\"ggh\"},{\"name_trg\":\"GA\",\"basic_course_date\":\"2020-02-20\",\"basic_course_mark\":\"gh\",\"basic_course_result\":\"ghgh\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"G\'Sc\",\"basic_course_date\":\"2020-02-27\",\"basic_course_mark\":\"gg\",\"basic_course_result\":\"gh\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"OIM\",\"basic_course_date\":\"2020-02-12\",\"basic_course_mark\":\"g\",\"basic_course_result\":\"gh\",\"basic_course_sign\":\"ghg\"},{\"name_trg\":\"MH\",\"basic_course_date\":\"2020-02-03\",\"basic_course_mark\":\"gh\",\"basic_course_result\":\"ghg\",\"basic_course_sign\":\"hgh\"}]', '[{\"name_trg\":\"Hindi\",\"basic_course_date\":\"2020-02-10\",\"basic_course_mark\":\"gh\",\"basic_course_result\":\"ghgh\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"English\",\"basic_course_date\":\"2020-02-17\",\"basic_course_mark\":\"ghg\",\"basic_course_result\":\"hgh\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"Maths\",\"basic_course_date\":\"2020-02-10\",\"basic_course_mark\":\"gh\",\"basic_course_result\":\"gh\",\"basic_course_sign\":\"ghgh\"},{\"name_trg\":\"GA\",\"basic_course_date\":\"2020-02-25\",\"basic_course_mark\":\"gh\",\"basic_course_result\":\"ghgh\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"G\'Sc\",\"basic_course_date\":\"2020-02-19\",\"basic_course_mark\":\"ghg\",\"basic_course_result\":\"hgh\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"OIM\",\"basic_course_date\":\"2020-02-26\",\"basic_course_mark\":\"ghg\",\"basic_course_result\":\"hgh\",\"basic_course_sign\":\"gh\"},{\"name_trg\":\"MH\",\"basic_course_date\":\"2020-02-18\",\"basic_course_mark\":\"gh\",\"basic_course_result\":\"gf\",\"basic_course_sign\":\"gf\"}]'),
(8, 8, '[{\"name_trg\":\"Hindi\",\"basic_course_date\":\"2020-02-11\",\"basic_course_mark\":\"ZXC\",\"basic_course_result\":\"XZC\",\"basic_course_sign\":\"XZC\"},{\"name_trg\":\"English\",\"basic_course_date\":\"2020-02-25\",\"basic_course_mark\":\"ZXC\",\"basic_course_result\":\"XZC\",\"basic_course_sign\":\"XZC\"},{\"name_trg\":\"Maths\",\"basic_course_date\":\"2020-02-27\",\"basic_course_mark\":\"ZXC\",\"basic_course_result\":\"XZC\",\"basic_course_sign\":\"XZC\"},{\"name_trg\":\"GA\",\"basic_course_date\":\"2020-02-07\",\"basic_course_mark\":\"XCZ\",\"basic_course_result\":\"XCZ\",\"basic_course_sign\":\"XCZ\"},{\"name_trg\":\"G\'Sc\",\"basic_course_date\":\"2020-02-22\",\"basic_course_mark\":\"ZXCSA\",\"basic_course_result\":\"D\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"OIM\",\"basic_course_date\":\"2020-02-08\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SDA\",\"basic_course_sign\":\"DAS\"},{\"name_trg\":\"MH\",\"basic_course_date\":\"2020-02-19\",\"basic_course_mark\":\"SD\",\"basic_course_result\":\"ASD\",\"basic_course_sign\":\"SAD\"}]', '[{\"name_trg\":\"Hindi\",\"basic_course_date\":\"2020-02-27\",\"basic_course_mark\":\"XZC\",\"basic_course_result\":\"XZC\",\"basic_course_sign\":\"XZC\"},{\"name_trg\":\"English\",\"basic_course_date\":\"2020-02-11\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"Maths\",\"basic_course_date\":\"2020-02-25\",\"basic_course_mark\":\"SADSAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"GA\",\"basic_course_date\":\"2020-02-19\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"G\'Sc\",\"basic_course_date\":\"2020-02-18\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"OIM\",\"basic_course_date\":\"2020-02-26\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"MH\",\"basic_course_date\":\"2020-02-25\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"}]', '[{\"name_trg\":\"Hindi\",\"basic_course_date\":\"2020-02-04\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"English\",\"basic_course_date\":\"2020-02-17\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"Maths\",\"basic_course_date\":\"2020-02-10\",\"basic_course_mark\":\"ASD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"GA\",\"basic_course_date\":\"2020-02-25\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SDA\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"G\'Sc\",\"basic_course_date\":\"2020-02-27\",\"basic_course_mark\":\"ASD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"OIM\",\"basic_course_date\":\"2020-02-24\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"},{\"name_trg\":\"MH\",\"basic_course_date\":\"2020-02-26\",\"basic_course_mark\":\"SAD\",\"basic_course_result\":\"SAD\",\"basic_course_sign\":\"SAD\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_family_info`
--

DROP TABLE IF EXISTS `candidate_family_info`;
CREATE TABLE IF NOT EXISTS `candidate_family_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `candidate_relation` varchar(255) DEFAULT NULL,
  `candidate_relation_name` varchar(255) DEFAULT NULL,
  `candidate_relation_dob` date DEFAULT NULL,
  `candidate_relation_age` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate_family_info`
--

INSERT INTO `candidate_family_info` (`id`, `candidate_id`, `candidate_relation`, `candidate_relation_name`, `candidate_relation_dob`, `candidate_relation_age`) VALUES
(55, 7, 'Mother', 'asd', '2020-02-23', '22'),
(56, 7, 'Sister', 'asd', '2020-02-03', '33'),
(64, 8, 'Sister', 'sdsa', '2020-02-14', '22'),
(63, 8, 'Brother', 'sdzfsf', '2020-02-28', '33'),
(61, 8, 'Father', 'ram', '2020-02-01', '22'),
(62, 8, 'Mother', 'sds', '2020-02-21', '33'),
(54, 7, 'Brother', 'asd', '2020-02-06', '33'),
(53, 7, 'Father', 'asda', '2020-02-20', '33');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_registration`
--

DROP TABLE IF EXISTS `candidate_registration`;
CREATE TABLE IF NOT EXISTS `candidate_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_army_no` varchar(255) DEFAULT NULL,
  `candidate_course` varchar(255) DEFAULT NULL,
  `candidate_company` varchar(255) DEFAULT NULL,
  `candidate_name` varchar(255) DEFAULT NULL,
  `candidate_section` varchar(255) DEFAULT NULL,
  `candidate_year` varchar(255) DEFAULT NULL,
  `candidate_rank` varchar(255) DEFAULT NULL,
  `candidate_platoon` varchar(255) DEFAULT NULL,
  `candidate_aro_uhq` varchar(255) DEFAULT NULL,
  `candidate_dob` date DEFAULT NULL,
  `candidate_doe` date DEFAULT NULL,
  `candidate_poe` date DEFAULT NULL,
  `candidate_address` text,
  `candidate_blood_group` varchar(255) DEFAULT NULL,
  `candidate_religion` varchar(255) DEFAULT NULL,
  `candidate_caste` varchar(255) DEFAULT NULL,
  `candidate_village` varchar(255) DEFAULT NULL,
  `candidate_post_office` varchar(255) DEFAULT NULL,
  `candidate_district` varchar(255) DEFAULT NULL,
  `candidate_state` varchar(255) DEFAULT NULL,
  `instructer_info` text,
  `edu_info` text,
  `physical_parameters` text,
  `create_date` date DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate_registration`
--

INSERT INTO `candidate_registration` (`id`, `candidate_army_no`, `candidate_course`, `candidate_company`, `candidate_name`, `candidate_section`, `candidate_year`, `candidate_rank`, `candidate_platoon`, `candidate_aro_uhq`, `candidate_dob`, `candidate_doe`, `candidate_poe`, `candidate_address`, `candidate_blood_group`, `candidate_religion`, `candidate_caste`, `candidate_village`, `candidate_post_office`, `candidate_district`, `candidate_state`, `instructer_info`, `edu_info`, `physical_parameters`, `create_date`, `create_time`) VALUES
(7, '789798', 'cxvxv', 'cdsfdsf', 'xfdgdf', 'dfgfdgd', '1999', 'dfgdfg', 'dfgdg', 'dfgdg', '2020-02-19', '2020-02-19', '2020-02-26', 'asdad', 'sdffsd', 'asda', 'asd', 'asd', 'asda', 'asd', 'asd', '{\"company_commander_rank\":\"asd\",\"company_commander_name\":\"asd\",\"company_commander_form_date\":\"2020-02-26\",\"platoon_commander_rank\":\"asd\",\"platoon_commander_name\":\"asd\",\"platoon_commander_form_date\":\"2020-02-18\",\"platoon_havaldar_first_rank\":\"asd\",\"platoon_havaldar_first_name\":\"sad\",\"platoon_havaldar_first_form_date\":\"2020-02-27\",\"platoon_havaldar_second_rank\":\"asd\",\"platoon_havaldar_second_name\":\"asd\",\"platoon_havaldar_second_form_date\":\"2020-02-28\"}', '{\"qualification_civil\":\"asd\",\"qualification_civil_date\":\"2020-02-10\",\"qualification_civil_part_order\":\"asd\",\"qualification_mr\":\"sad\",\"qualification_mr_date\":\"2020-02-12\",\"qualification_mr_part_order\":\"asd\",\"qualification_aec\":\"asd\",\"qualification_aec_date\":\"2020-02-26\",\"qualification_aec_part_order\":\"asd\",\"qualification_ttt\":\"asd\",\"qualification_ttt_date\":\"2020-02-20\",\"qualification_ttt_part_order\":\"asd\",\"qualification_attestation\":\"asd\",\"qualification_attestation_date\":\"2020-02-28\",\"qualification_attestation_part_order\":\"asd\"}', '{\"candidate_height\":\"asd\",\"candidate_permissible_wt\":\"asd\",\"candidate_actual_wt\":\"sad\",\"candidate_over_under_wt\":\"sad\",\"candidate_identification_mark_one\":\"asd\",\"candidate_identification_mark_two\":\"sad\"}', '2020-02-16', '0000-00-00 00:00:00'),
(8, '7777777777777', 'new', 'demo', 'dilip', 'demo', '2001', '1', 'test', 'test', '2020-02-01', '2020-02-02', '2020-02-03', 'test', 'b', 'hindi', 'hindu', 'sdsd', 'dfsf', 'jodhpur', 'rajasthan', '{\"company_commander_rank\":\"ASD\",\"company_commander_name\":\"SAD\",\"company_commander_form_date\":\"2020-02-21\",\"platoon_commander_rank\":\"ASD\",\"platoon_commander_name\":\"SAD\",\"platoon_commander_form_date\":\"2020-02-25\",\"platoon_havaldar_first_rank\":\"SAD\",\"platoon_havaldar_first_name\":\"SAD\",\"platoon_havaldar_first_form_date\":\"2020-02-28\",\"platoon_havaldar_second_rank\":\"SAD\",\"platoon_havaldar_second_name\":\"SAD\",\"platoon_havaldar_second_form_date\":\"2020-02-28\"}', '{\"qualification_civil\":\"asd\",\"qualification_civil_date\":\"2020-02-02\",\"qualification_civil_part_order\":\"asd\",\"qualification_mr\":\"asd\",\"qualification_mr_date\":\"2020-02-25\",\"qualification_mr_part_order\":\"asd\",\"qualification_aec\":\"sad\",\"qualification_aec_date\":\"2020-02-10\",\"qualification_aec_part_order\":\"sad\",\"qualification_ttt\":\"sad\",\"qualification_ttt_date\":\"2020-02-21\",\"qualification_ttt_part_order\":\"asdas\",\"qualification_attestation\":\"asd\",\"qualification_attestation_date\":\"2020-02-25\",\"qualification_attestation_part_order\":\"asdasd\"}', '{\"candidate_height\":\"162cm\",\"candidate_permissible_wt\":\"84kg\",\"candidate_actual_wt\":\"65kg\",\"candidate_over_under_wt\":\"sadas\",\"candidate_identification_mark_one\":\"asd\",\"candidate_identification_mark_two\":\"asda\"}', '2020-02-16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_trg_bn_cdr_ppt_test`
--

DROP TABLE IF EXISTS `candidate_trg_bn_cdr_ppt_test`;
CREATE TABLE IF NOT EXISTS `candidate_trg_bn_cdr_ppt_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `first_attempt_test` text,
  `second_attempt_test` text,
  `third_attempt_test` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate_trg_bn_cdr_ppt_test`
--

INSERT INTO `candidate_trg_bn_cdr_ppt_test` (`id`, `candidate_id`, `first_attempt_test`, `second_attempt_test`, `third_attempt_test`) VALUES
(7, 7, '[{\"name_trg\":\"2,4 KM Run\",\"trg_bn_ppt_mark\":\"asd\",\"trg_bn_ppt_grade\":\"asd\",\"trg_bn_ppt_sign\":\"asd\"},{\"name_trg\":\"Bent Knee Sit ups\",\"trg_bn_ppt_mark\":\"asd\",\"trg_bn_ppt_grade\":\"asd\",\"trg_bn_ppt_sign\":\"asd\"},{\"name_trg\":\"5 M Shuttle\",\"trg_bn_ppt_mark\":\"sad\",\"trg_bn_ppt_grade\":\"sad\",\"trg_bn_ppt_sign\":\"asd\"},{\"name_trg\":\"Toe Touch\",\"trg_bn_ppt_mark\":\"vgh\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"gh\"},{\"name_trg\":\"100 M Sprint\",\"trg_bn_ppt_mark\":\"ghgh\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"ghgh\"},{\"name_trg\":\"Chin up\",\"trg_bn_ppt_mark\":\"gh\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"gh\"}]', '[{\"name_trg\":\"2,4 KM Run\",\"trg_bn_ppt_mark\":\"gg\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"ghg\"},{\"name_trg\":\"Bent Knee Sit ups\",\"trg_bn_ppt_mark\":\"gh\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"ghgh\"},{\"name_trg\":\"5 M Shuttle\",\"trg_bn_ppt_mark\":\"gh\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"gg\"},{\"name_trg\":\"Toe Touch\",\"trg_bn_ppt_mark\":\"hgh\",\"trg_bn_ppt_grade\":\"gg\",\"trg_bn_ppt_sign\":\"hgh\"},{\"name_trg\":\"100 M Sprint\",\"trg_bn_ppt_mark\":\"gh\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"ghg\"},{\"name_trg\":\"Chin up\",\"trg_bn_ppt_mark\":\"hg\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"gh\"}]', '[{\"name_trg\":\"2,4 KM Run\",\"trg_bn_ppt_mark\":\"hgh\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"ghg\"},{\"name_trg\":\"Bent Knee Sit ups\",\"trg_bn_ppt_mark\":\"gh\",\"trg_bn_ppt_grade\":\"g\",\"trg_bn_ppt_sign\":\"g\"},{\"name_trg\":\"5 M Shuttle\",\"trg_bn_ppt_mark\":\"ghg\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"gh\"},{\"name_trg\":\"Toe Touch\",\"trg_bn_ppt_mark\":\"gh\",\"trg_bn_ppt_grade\":\"ghg\",\"trg_bn_ppt_sign\":\"hgh\"},{\"name_trg\":\"100 M Sprint\",\"trg_bn_ppt_mark\":\"g\",\"trg_bn_ppt_grade\":\"gh\",\"trg_bn_ppt_sign\":\"ghg\"},{\"name_trg\":\"Chin up\",\"trg_bn_ppt_mark\":\"hgh\",\"trg_bn_ppt_grade\":\"g\",\"trg_bn_ppt_sign\":\"ghg\"}]'),
(8, 8, '[{\"name_trg\":\"2,4 KM Run\",\"trg_bn_ppt_mark\":\"asd\",\"trg_bn_ppt_grade\":\"BH\",\"trg_bn_ppt_sign\":\"GH\"},{\"name_trg\":\"Bent Knee Sit ups\",\"trg_bn_ppt_mark\":\"GJ\",\"trg_bn_ppt_grade\":\"HJHJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"5 M Shuttle\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJHJ\",\"trg_bn_ppt_sign\":\"JH\"},{\"name_trg\":\"Toe Touch\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJH\"},{\"name_trg\":\"100 M Sprint\",\"trg_bn_ppt_mark\":\"JHJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"Chin up\",\"trg_bn_ppt_mark\":\"HJHJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJ\"}]', '[{\"name_trg\":\"2,4 KM Run\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJHJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"Bent Knee Sit ups\",\"trg_bn_ppt_mark\":\"HJH\",\"trg_bn_ppt_grade\":\"JHJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"5 M Shuttle\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJHJ\"},{\"name_trg\":\"Toe Touch\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"100 M Sprint\",\"trg_bn_ppt_mark\":\"HJH\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"Chin up\",\"trg_bn_ppt_mark\":\"H\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJHJ\"}]', '[{\"name_trg\":\"2,4 KM Run\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"Bent Knee Sit ups\",\"trg_bn_ppt_mark\":\"HJH\",\"trg_bn_ppt_grade\":\"JHJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"5 M Shuttle\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJHJ\"},{\"name_trg\":\"Toe Touch\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJ\"},{\"name_trg\":\"100 M Sprint\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJHJ\"},{\"name_trg\":\"Chin up\",\"trg_bn_ppt_mark\":\"HJ\",\"trg_bn_ppt_grade\":\"HJ\",\"trg_bn_ppt_sign\":\"HJ\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `commandent_test_bpet`
--

DROP TABLE IF EXISTS `commandent_test_bpet`;
CREATE TABLE IF NOT EXISTS `commandent_test_bpet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `first_attempt_test` text,
  `second_attempt_test` text,
  `third_attempt_test` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commandent_test_bpet`
--

INSERT INTO `commandent_test_bpet` (`id`, `candidate_id`, `first_attempt_test`, `second_attempt_test`, `third_attempt_test`) VALUES
(6, 7, '[{\"name_trg\":\"5km run\",\"commandant_test_bpet_mark\":\"ghgh\",\"commandant_test_bpet_grade\":\"gh\",\"commandant_test_bpet_sign\":\"gh\"},{\"name_trg\":\"H\\/rope\",\"commandant_test_bpet_mark\":\"ghgh\",\"commandant_test_bpet_grade\":\"gh\",\"commandant_test_bpet_sign\":\"g\"},{\"name_trg\":\"V\\/rope\",\"commandant_test_bpet_mark\":\"ggh\",\"commandant_test_bpet_grade\":\"gh\",\"commandant_test_bpet_sign\":\"gh\"},{\"name_trg\":\"9\'Ditch\",\"commandant_test_bpet_mark\":\"ghg\",\"commandant_test_bpet_grade\":\"hgh\",\"commandant_test_bpet_sign\":\"gh\"},{\"name_trg\":\"60M sprint\",\"commandant_test_bpet_mark\":\"ghgh\",\"commandant_test_bpet_grade\":\"gh\",\"commandant_test_bpet_sign\":\"gh\"},{\"name_trg\":\"Overall\",\"commandant_test_bpet_mark\":\"gh\",\"commandant_test_bpet_grade\":\"ghgh\",\"commandant_test_bpet_sign\":\"gh\"}]', '[{\"name_trg\":\"5km run\",\"commandant_test_bpet_mark\":\"ghg\",\"commandant_test_bpet_grade\":\"hgh\",\"commandant_test_bpet_sign\":\"gh\"},{\"name_trg\":\"H\\/rope\",\"commandant_test_bpet_mark\":\"gvgv\",\"commandant_test_bpet_grade\":\"g\",\"commandant_test_bpet_sign\":\"gh\"},{\"name_trg\":\"V\\/rope\",\"commandant_test_bpet_mark\":\"ghg\",\"commandant_test_bpet_grade\":\"hgh\",\"commandant_test_bpet_sign\":\"gh\"},{\"name_trg\":\"9\'Ditch\",\"commandant_test_bpet_mark\":\"ghvg\",\"commandant_test_bpet_grade\":\"vgv\",\"commandant_test_bpet_sign\":\"gv\"},{\"name_trg\":\"60M sprint\",\"commandant_test_bpet_mark\":\"g\",\"commandant_test_bpet_grade\":\"ghg\",\"commandant_test_bpet_sign\":\"gv\"},{\"name_trg\":\"Overall\",\"commandant_test_bpet_mark\":\"g\",\"commandant_test_bpet_grade\":\"gvgv\",\"commandant_test_bpet_sign\":\"gv\"}]', '[{\"name_trg\":\"5km run\",\"commandant_test_bpet_mark\":\"gv\",\"commandant_test_bpet_grade\":\"gvgv\",\"commandant_test_bpet_sign\":\"gv\"},{\"name_trg\":\"H\\/rope\",\"commandant_test_bpet_mark\":\"gvg\",\"commandant_test_bpet_grade\":\"vgv\",\"commandant_test_bpet_sign\":\"gv\"},{\"name_trg\":\"V\\/rope\",\"commandant_test_bpet_mark\":\"gvvg\",\"commandant_test_bpet_grade\":\"gv\",\"commandant_test_bpet_sign\":\"vg\"},{\"name_trg\":\"9\'Ditch\",\"commandant_test_bpet_mark\":\"vg\",\"commandant_test_bpet_grade\":\"vggv\",\"commandant_test_bpet_sign\":\"gv\"},{\"name_trg\":\"60M sprint\",\"commandant_test_bpet_mark\":\"gv\",\"commandant_test_bpet_grade\":\"gv\",\"commandant_test_bpet_sign\":\"gvgv\"},{\"name_trg\":\"Overall\",\"commandant_test_bpet_mark\":\"gv\",\"commandant_test_bpet_grade\":\"gvgv\",\"commandant_test_bpet_sign\":\"gv\"}]'),
(7, 8, '[{\"name_trg\":\"5km run\",\"commandant_test_bpet_mark\":\"BN\",\"commandant_test_bpet_grade\":\"SAD\",\"commandant_test_bpet_sign\":\"ASD\"},{\"name_trg\":\"H\\/rope\",\"commandant_test_bpet_mark\":\"ASD\",\"commandant_test_bpet_grade\":\"B\",\"commandant_test_bpet_sign\":\"B\"},{\"name_trg\":\"V\\/rope\",\"commandant_test_bpet_mark\":\"HJHJ\",\"commandant_test_bpet_grade\":\"HJ\",\"commandant_test_bpet_sign\":\"HJ\"},{\"name_trg\":\"9\'Ditch\",\"commandant_test_bpet_mark\":\"HJJH\",\"commandant_test_bpet_grade\":\"HJ\",\"commandant_test_bpet_sign\":\"HJ\"},{\"name_trg\":\"60M sprint\",\"commandant_test_bpet_mark\":\"HJHJ\",\"commandant_test_bpet_grade\":\"HJ\",\"commandant_test_bpet_sign\":\"JH\"},{\"name_trg\":\"Overall\",\"commandant_test_bpet_mark\":\"HJ\",\"commandant_test_bpet_grade\":\"HJHJ\",\"commandant_test_bpet_sign\":\"HJ\"}]', '[{\"name_trg\":\"5km run\",\"commandant_test_bpet_mark\":\"HJHJ\",\"commandant_test_bpet_grade\":\"HJ\",\"commandant_test_bpet_sign\":\"HJ\"},{\"name_trg\":\"H\\/rope\",\"commandant_test_bpet_mark\":\"HJH\",\"commandant_test_bpet_grade\":\"JHJ\",\"commandant_test_bpet_sign\":\"HJ\"},{\"name_trg\":\"V\\/rope\",\"commandant_test_bpet_mark\":\"HJHJ\",\"commandant_test_bpet_grade\":\"HJ\",\"commandant_test_bpet_sign\":\"HJ\"},{\"name_trg\":\"9\'Ditch\",\"commandant_test_bpet_mark\":\"HJ\",\"commandant_test_bpet_grade\":\"HJHJ\",\"commandant_test_bpet_sign\":\"JH\"},{\"name_trg\":\"60M sprint\",\"commandant_test_bpet_mark\":\"HJ\",\"commandant_test_bpet_grade\":\"HJ\",\"commandant_test_bpet_sign\":\"N\"},{\"name_trg\":\"Overall\",\"commandant_test_bpet_mark\":\"NB\",\"commandant_test_bpet_grade\":\"NBNB\",\"commandant_test_bpet_sign\":\"NB\"}]', '[{\"name_trg\":\"5km run\",\"commandant_test_bpet_mark\":\"NB\",\"commandant_test_bpet_grade\":\"NBN\",\"commandant_test_bpet_sign\":\"BBN\"},{\"name_trg\":\"H\\/rope\",\"commandant_test_bpet_mark\":\"BN\",\"commandant_test_bpet_grade\":\"NB\",\"commandant_test_bpet_sign\":\"NBNB\"},{\"name_trg\":\"V\\/rope\",\"commandant_test_bpet_mark\":\"BN\",\"commandant_test_bpet_grade\":\"BNBN\",\"commandant_test_bpet_sign\":\"NB\"},{\"name_trg\":\"9\'Ditch\",\"commandant_test_bpet_mark\":\"BN\",\"commandant_test_bpet_grade\":\"BN\",\"commandant_test_bpet_sign\":\"BNNB\"},{\"name_trg\":\"60M sprint\",\"commandant_test_bpet_mark\":\"BN\",\"commandant_test_bpet_grade\":\"BN\",\"commandant_test_bpet_sign\":\"BNBN\"},{\"name_trg\":\"Overall\",\"commandant_test_bpet_mark\":\"BN\",\"commandant_test_bpet_grade\":\"BN\",\"commandant_test_bpet_sign\":\"BNBN\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `commandent_test_firing`
--

DROP TABLE IF EXISTS `commandent_test_firing`;
CREATE TABLE IF NOT EXISTS `commandent_test_firing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `first_attempt_test` text,
  `second_attempt_test` text,
  `third_attempt_test` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commandent_test_firing`
--

INSERT INTO `commandent_test_firing` (`id`, `candidate_id`, `first_attempt_test`, `second_attempt_test`, `third_attempt_test`) VALUES
(6, 7, '[{\"name_trg\":\"Lnsas Lmg day\",\"commandant_test_firing_date\":\"2020-02-12\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gvgv\"},{\"name_trg\":\"Lnsas Lmg\",\"commandant_test_firing_date\":\"2020-02-26\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gvgv\",\"commandant_test_firing_sign\":\"gv\"}]', '[{\"name_trg\":\"Lnsas Lmg day\",\"commandant_test_firing_date\":\"2020-02-20\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gvgv\",\"commandant_test_firing_sign\":\"vg\"},{\"name_trg\":\"Lnsas Lmg\",\"commandant_test_firing_date\":\"2020-02-26\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gvgv\",\"commandant_test_firing_sign\":\"gv\"}]', '[{\"name_trg\":\"Lnsas Lmg day\",\"commandant_test_firing_date\":\"2020-02-11\",\"commandant_test_firing_mark\":\"vg\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gvgv\"},{\"name_trg\":\"Lnsas Lmg\",\"commandant_test_firing_date\":\"2020-02-27\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gvgv\"}]'),
(7, 8, '[{\"name_trg\":\"Lnsas Lmg day\",\"commandant_test_firing_date\":\"2020-02-04\",\"commandant_test_firing_mark\":\"BN\",\"commandant_test_firing_result\":\"NBNB\",\"commandant_test_firing_sign\":\"BN\"},{\"name_trg\":\"Lnsas Lmg\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"BNBN\",\"commandant_test_firing_result\":\"NB\",\"commandant_test_firing_sign\":\"BNBN\"}]', '[{\"name_trg\":\"Lnsas Lmg day\",\"commandant_test_firing_date\":\"2020-02-13\",\"commandant_test_firing_mark\":\"NBN\",\"commandant_test_firing_result\":\"BNB\",\"commandant_test_firing_sign\":\"NB\"},{\"name_trg\":\"Lnsas Lmg\",\"commandant_test_firing_date\":\"2020-02-22\",\"commandant_test_firing_mark\":\"NB\",\"commandant_test_firing_result\":\"BN\",\"commandant_test_firing_sign\":\"BNBN\"}]', '[{\"name_trg\":\"Lnsas Lmg day\",\"commandant_test_firing_date\":\"2020-02-03\",\"commandant_test_firing_mark\":\"BN\",\"commandant_test_firing_result\":\"NBNB\",\"commandant_test_firing_sign\":\"NB\"},{\"name_trg\":\"Lnsas Lmg\",\"commandant_test_firing_date\":\"2020-02-14\",\"commandant_test_firing_mark\":\"NBB\",\"commandant_test_firing_result\":\"NBN\",\"commandant_test_firing_sign\":\"NB\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `commandent_test_firing_another`
--

DROP TABLE IF EXISTS `commandent_test_firing_another`;
CREATE TABLE IF NOT EXISTS `commandent_test_firing_another` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `first_attempt_test` text,
  `second_attempt_test` text,
  `third_attempt_test` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commandent_test_firing_another`
--

INSERT INTO `commandent_test_firing_another` (`id`, `candidate_id`, `first_attempt_test`, `second_attempt_test`, `third_attempt_test`) VALUES
(7, 7, '[{\"name_trg\":\"Drill\",\"commandant_test_firing_date\":\"2020-02-18\",\"commandant_test_firing_mark\":\"gvg\",\"commandant_test_firing_result\":\"vgv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"FC & BC\",\"commandant_test_firing_date\":\"2020-02-18\",\"commandant_test_firing_mark\":\"vgv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"Written\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"gvg\",\"commandant_test_firing_result\":\"vgv\",\"commandant_test_firing_sign\":\"vg\"},{\"name_trg\":\"Swimming\",\"commandant_test_firing_date\":\"2020-02-18\",\"commandant_test_firing_mark\":\"gvg\",\"commandant_test_firing_result\":\"vgv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"ASLT\",\"commandant_test_firing_date\":\"2020-02-20\",\"commandant_test_firing_mark\":\"gvgv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"WT(SOET)\",\"commandant_test_firing_date\":\"2020-02-27\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"vggv\"}]', '[{\"name_trg\":\"Drill\",\"commandant_test_firing_date\":\"2020-02-27\",\"commandant_test_firing_mark\":\"vg\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"FC & BC\",\"commandant_test_firing_date\":\"2020-02-18\",\"commandant_test_firing_mark\":\"gvgv\",\"commandant_test_firing_result\":\"vg\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"Written\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gvgv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"Swimming\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gvv\"},{\"name_trg\":\"ASLT\",\"commandant_test_firing_date\":\"2020-02-26\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gvvg\"},{\"name_trg\":\"WT(SOET)\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"vg\",\"commandant_test_firing_sign\":\"gvv\"}]', '[{\"name_trg\":\"Drill\",\"commandant_test_firing_date\":\"2020-02-18\",\"commandant_test_firing_mark\":\"vg\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"FC & BC\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"vg\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"vg\"},{\"name_trg\":\"Written\",\"commandant_test_firing_date\":\"2020-02-10\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gv\",\"commandant_test_firing_sign\":\"vggv\"},{\"name_trg\":\"Swimming\",\"commandant_test_firing_date\":\"2020-02-18\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"gvgv\",\"commandant_test_firing_sign\":\"gv\"},{\"name_trg\":\"ASLT\",\"commandant_test_firing_date\":\"2020-02-12\",\"commandant_test_firing_mark\":\"gvgv\",\"commandant_test_firing_result\":\"vg\",\"commandant_test_firing_sign\":\"vg\"},{\"name_trg\":\"WT(SOET)\",\"commandant_test_firing_date\":\"2020-02-26\",\"commandant_test_firing_mark\":\"gv\",\"commandant_test_firing_result\":\"vg\",\"commandant_test_firing_sign\":\"gvgv\"}]'),
(8, 8, '[{\"name_trg\":\"Drill\",\"commandant_test_firing_date\":\"2020-02-26\",\"commandant_test_firing_mark\":\"ASD\",\"commandant_test_firing_result\":\"ASD\",\"commandant_test_firing_sign\":\"ASD\"},{\"name_trg\":\"FC & BC\",\"commandant_test_firing_date\":\"2020-02-22\",\"commandant_test_firing_mark\":\"ASD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"Written\",\"commandant_test_firing_date\":\"2020-02-20\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"Swimming\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"ASLT\",\"commandant_test_firing_date\":\"2020-02-11\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"WT(SOET)\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"}]', '[{\"name_trg\":\"Drill\",\"commandant_test_firing_date\":\"2020-02-05\",\"commandant_test_firing_mark\":\"ASDAS\",\"commandant_test_firing_result\":\"SDAD\",\"commandant_test_firing_sign\":\"ASD\"},{\"name_trg\":\"FC & BC\",\"commandant_test_firing_date\":\"2020-02-01\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"Written\",\"commandant_test_firing_date\":\"2020-02-08\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SDA\",\"commandant_test_firing_sign\":\"ASD\"},{\"name_trg\":\"Swimming\",\"commandant_test_firing_date\":\"2020-02-20\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"ASLT\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"WT(SOET)\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"}]', '[{\"name_trg\":\"Drill\",\"commandant_test_firing_date\":\"2020-02-18\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"FC & BC\",\"commandant_test_firing_date\":\"2020-02-17\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"Written\",\"commandant_test_firing_date\":\"2020-02-19\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"Swimming\",\"commandant_test_firing_date\":\"2020-02-27\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"ASLT\",\"commandant_test_firing_date\":\"2020-02-25\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"},{\"name_trg\":\"WT(SOET)\",\"commandant_test_firing_date\":\"2020-02-20\",\"commandant_test_firing_mark\":\"SAD\",\"commandant_test_firing_result\":\"SAD\",\"commandant_test_firing_sign\":\"SAD\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `map_reading_test`
--

DROP TABLE IF EXISTS `map_reading_test`;
CREATE TABLE IF NOT EXISTS `map_reading_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `first_attempt_test` text,
  `second_attempt_test` text,
  `third_attempt_test` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map_reading_test`
--

INSERT INTO `map_reading_test` (`id`, `candidate_id`, `first_attempt_test`, `second_attempt_test`, `third_attempt_test`) VALUES
(6, 7, '[{\"name_trg\":\"Practical\",\"map_reading_date\":\"2020-02-25\",\"map_reading_mark\":\"gh\",\"map_reading_result\":\"gh\",\"map_reading_sign\":\"ghgh\"},{\"name_trg\":\"Written\",\"map_reading_date\":\"2020-02-24\",\"map_reading_mark\":\"ghgh\",\"map_reading_result\":\"ghgh\",\"map_reading_sign\":\"gh\"}]', '[{\"name_trg\":\"Practical\",\"map_reading_date\":\"2020-02-25\",\"map_reading_mark\":\"gh\",\"map_reading_result\":\"gh\",\"map_reading_sign\":\"ghgh\"},{\"name_trg\":\"Written\",\"map_reading_date\":\"2020-02-24\",\"map_reading_mark\":\"ghgh\",\"map_reading_result\":\"ghgh\",\"map_reading_sign\":\"gh\"}]', '[{\"name_trg\":\"Practical\",\"map_reading_date\":\"2020-02-25\",\"map_reading_mark\":\"gh\",\"map_reading_result\":\"gh\",\"map_reading_sign\":\"ghgh\"},{\"name_trg\":\"Written\",\"map_reading_date\":\"2020-02-24\",\"map_reading_mark\":\"ghgh\",\"map_reading_result\":\"ghgh\",\"map_reading_sign\":\"gh\"}]'),
(7, 8, '[{\"name_trg\":\"Practical\",\"map_reading_date\":\"2020-02-18\",\"map_reading_mark\":\"SAD\",\"map_reading_result\":\"SAD\",\"map_reading_sign\":\"SAD\"},{\"name_trg\":\"Written\",\"map_reading_date\":\"2020-02-27\",\"map_reading_mark\":\"SAD\",\"map_reading_result\":\"SAD\",\"map_reading_sign\":\"SAD\"}]', '[{\"name_trg\":\"Practical\",\"map_reading_date\":\"2020-02-18\",\"map_reading_mark\":\"SAD\",\"map_reading_result\":\"SAD\",\"map_reading_sign\":\"SAD\"},{\"name_trg\":\"Written\",\"map_reading_date\":\"2020-02-26\",\"map_reading_mark\":\"SAD\",\"map_reading_result\":\"SAD\",\"map_reading_sign\":\"SAD\"}]', '[{\"name_trg\":\"Practical\",\"map_reading_date\":\"2020-02-17\",\"map_reading_mark\":\"SAD\",\"map_reading_result\":\"SAD\",\"map_reading_sign\":\"SAD\"},{\"name_trg\":\"Written\",\"map_reading_date\":\"2020-02-25\",\"map_reading_mark\":\"SAD\",\"map_reading_result\":\"SA\",\"map_reading_sign\":\"AS\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `trg_bn_cdr_test`
--

DROP TABLE IF EXISTS `trg_bn_cdr_test`;
CREATE TABLE IF NOT EXISTS `trg_bn_cdr_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidate_id` int(11) DEFAULT NULL,
  `first_attempt_test` text,
  `second_attempt_test` text,
  `third_attempt_test` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trg_bn_cdr_test`
--

INSERT INTO `trg_bn_cdr_test` (`id`, `candidate_id`, `first_attempt_test`, `second_attempt_test`, `third_attempt_test`) VALUES
(7, 7, '[{\"name_trg\":\"Drill\",\"trg_bn_cdr_date\":\"2020-02-18\",\"trg_bn_cdr_marks\":\"g\",\"trg_bn_cdr_grade\":\"gg\",\"trg_bn_cdr_sign\":\"gh\"},{\"name_trg\":\"Firing Day\",\"trg_bn_cdr_date\":\"2020-02-26\",\"trg_bn_cdr_marks\":\"hgh\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"gh\"},{\"name_trg\":\"Firing Night\",\"trg_bn_cdr_date\":\"2020-02-20\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"FC & BC\",\"trg_bn_cdr_date\":\"2020-02-08\",\"trg_bn_cdr_marks\":\"g\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"BYT\",\"trg_bn_cdr_date\":\"2020-02-11\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"WT(tsoet)\",\"trg_bn_cdr_date\":\"2020-02-17\",\"trg_bn_cdr_marks\":\"g\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"Written\",\"trg_bn_cdr_date\":\"2020-02-10\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"ghg\",\"trg_bn_cdr_sign\":\"gh\"},{\"name_trg\":\"Swimming\",\"trg_bn_cdr_date\":\"2020-02-03\",\"trg_bn_cdr_marks\":\"ghg\",\"trg_bn_cdr_grade\":\"hgh\",\"trg_bn_cdr_sign\":\"g\"},{\"name_trg\":\"OT\",\"trg_bn_cdr_date\":\"2020-02-26\",\"trg_bn_cdr_marks\":\"hgh\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"g\"}]', '[{\"name_trg\":\"Drill\",\"trg_bn_cdr_date\":\"2020-02-05\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"Firing Day\",\"trg_bn_cdr_date\":\"2020-02-18\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"Firing Night\",\"trg_bn_cdr_date\":\"2020-02-13\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"FC & BC\",\"trg_bn_cdr_date\":\"2020-02-19\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"ghg\",\"trg_bn_cdr_sign\":\"hgh\"},{\"name_trg\":\"BYT\",\"trg_bn_cdr_date\":\"2020-02-12\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"ghgh\",\"trg_bn_cdr_sign\":\"g\"},{\"name_trg\":\"WT(tsoet)\",\"trg_bn_cdr_date\":\"2020-02-19\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"gg\"},{\"name_trg\":\"Written\",\"trg_bn_cdr_date\":\"2020-02-26\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"ghgh\"},{\"name_trg\":\"Swimming\",\"trg_bn_cdr_date\":\"2020-02-07\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"ghg\",\"trg_bn_cdr_sign\":\"hgh\"},{\"name_trg\":\"OT\",\"trg_bn_cdr_date\":\"2020-02-28\",\"trg_bn_cdr_marks\":\"ghgh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"gh\"}]', '[{\"name_trg\":\"Drill\",\"trg_bn_cdr_date\":\"2020-02-11\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"gh\"},{\"name_trg\":\"Firing Day\",\"trg_bn_cdr_date\":\"2020-02-19\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"Firing Night\",\"trg_bn_cdr_date\":\"2020-02-18\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"ghgh\",\"trg_bn_cdr_sign\":\"gh\"},{\"name_trg\":\"FC & BC\",\"trg_bn_cdr_date\":\"2020-02-12\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"gg\"},{\"name_trg\":\"BYT\",\"trg_bn_cdr_date\":\"2020-02-12\",\"trg_bn_cdr_marks\":\"g\",\"trg_bn_cdr_grade\":\"g\",\"trg_bn_cdr_sign\":\"g\"},{\"name_trg\":\"WT(tsoet)\",\"trg_bn_cdr_date\":\"2020-02-12\",\"trg_bn_cdr_marks\":\"hgh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"ghg\"},{\"name_trg\":\"Written\",\"trg_bn_cdr_date\":\"2020-02-12\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"ghg\",\"trg_bn_cdr_sign\":\"hgh\"},{\"name_trg\":\"Swimming\",\"trg_bn_cdr_date\":\"2020-02-26\",\"trg_bn_cdr_marks\":\"gh\",\"trg_bn_cdr_grade\":\"ghg\",\"trg_bn_cdr_sign\":\"hgh\"},{\"name_trg\":\"OT\",\"trg_bn_cdr_date\":\"2020-02-13\",\"trg_bn_cdr_marks\":\"ghgh\",\"trg_bn_cdr_grade\":\"gh\",\"trg_bn_cdr_sign\":\"gh\"}]'),
(8, 8, '[{\"name_trg\":\"Drill\",\"trg_bn_cdr_date\":\"2020-02-04\",\"trg_bn_cdr_marks\":\"HJHJ\",\"trg_bn_cdr_grade\":\"HJ\",\"trg_bn_cdr_sign\":\"CXV\"},{\"name_trg\":\"Firing Day\",\"trg_bn_cdr_date\":\"2020-02-24\",\"trg_bn_cdr_marks\":\"XZCZXC\",\"trg_bn_cdr_grade\":\"ZCX\",\"trg_bn_cdr_sign\":\"ZXC\"},{\"name_trg\":\"Firing Night\",\"trg_bn_cdr_date\":\"2020-02-27\",\"trg_bn_cdr_marks\":\"ZXC\",\"trg_bn_cdr_grade\":\"XZC\",\"trg_bn_cdr_sign\":\"ZXC\"},{\"name_trg\":\"FC & BC\",\"trg_bn_cdr_date\":\"2020-02-25\",\"trg_bn_cdr_marks\":\"ZXC\",\"trg_bn_cdr_grade\":\"XZC\",\"trg_bn_cdr_sign\":\"XZC\"},{\"name_trg\":\"BYT\",\"trg_bn_cdr_date\":\"2020-02-26\",\"trg_bn_cdr_marks\":\"ZXC\",\"trg_bn_cdr_grade\":\"ZXC\",\"trg_bn_cdr_sign\":\"ZXC\"},{\"name_trg\":\"WT(tsoet)\",\"trg_bn_cdr_date\":\"2020-02-18\",\"trg_bn_cdr_marks\":\"SADAD\",\"trg_bn_cdr_grade\":\"ASD\",\"trg_bn_cdr_sign\":\"SAD\"},{\"name_trg\":\"Written\",\"trg_bn_cdr_date\":\"2020-02-17\",\"trg_bn_cdr_marks\":\"ZXCZXC\",\"trg_bn_cdr_grade\":\"ZXC\",\"trg_bn_cdr_sign\":\"SAD\"},{\"name_trg\":\"Swimming\",\"trg_bn_cdr_date\":\"2020-02-23\",\"trg_bn_cdr_marks\":\"SADSAD\",\"trg_bn_cdr_grade\":\"SAD\",\"trg_bn_cdr_sign\":\"SAD\"},{\"name_trg\":\"OT\",\"trg_bn_cdr_date\":\"2020-02-23\",\"trg_bn_cdr_marks\":\"DFDSF\",\"trg_bn_cdr_grade\":\"SDF\",\"trg_bn_cdr_sign\":\"SDF\"}]', '[{\"name_trg\":\"Drill\",\"trg_bn_cdr_date\":\"2020-02-10\",\"trg_bn_cdr_marks\":\"SDF\",\"trg_bn_cdr_grade\":\"SDF\",\"trg_bn_cdr_sign\":\"SDF\"},{\"name_trg\":\"Firing Day\",\"trg_bn_cdr_date\":\"2020-02-10\",\"trg_bn_cdr_marks\":\"SDF\",\"trg_bn_cdr_grade\":\"SDF\",\"trg_bn_cdr_sign\":\"DSF\"},{\"name_trg\":\"Firing Night\",\"trg_bn_cdr_date\":\"2020-02-12\",\"trg_bn_cdr_marks\":\"DSF\",\"trg_bn_cdr_grade\":\"DSF\",\"trg_bn_cdr_sign\":\"DSF\"},{\"name_trg\":\"FC & BC\",\"trg_bn_cdr_date\":\"2020-02-21\",\"trg_bn_cdr_marks\":\"ASD\",\"trg_bn_cdr_grade\":\"GHGH\",\"trg_bn_cdr_sign\":\"GHGH\"},{\"name_trg\":\"BYT\",\"trg_bn_cdr_date\":\"2020-02-28\",\"trg_bn_cdr_marks\":\"HGH\",\"trg_bn_cdr_grade\":\"GH\",\"trg_bn_cdr_sign\":\"GH\"},{\"name_trg\":\"WT(tsoet)\",\"trg_bn_cdr_date\":\"2020-02-27\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GH\",\"trg_bn_cdr_sign\":\"GHHG\"},{\"name_trg\":\"Written\",\"trg_bn_cdr_date\":\"2020-02-24\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GH\",\"trg_bn_cdr_sign\":\"GHG\"},{\"name_trg\":\"Swimming\",\"trg_bn_cdr_date\":\"2020-02-20\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GH\",\"trg_bn_cdr_sign\":\"GH\"},{\"name_trg\":\"OT\",\"trg_bn_cdr_date\":\"2020-02-08\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GH\",\"trg_bn_cdr_sign\":\"GH\"}]', '[{\"name_trg\":\"Drill\",\"trg_bn_cdr_date\":\"2020-02-03\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GH\",\"trg_bn_cdr_sign\":\"GHGH\"},{\"name_trg\":\"Firing Day\",\"trg_bn_cdr_date\":\"2020-02-10\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GH\",\"trg_bn_cdr_sign\":\"GHHG\"},{\"name_trg\":\"Firing Night\",\"trg_bn_cdr_date\":\"2020-02-17\",\"trg_bn_cdr_marks\":\"SDAD\",\"trg_bn_cdr_grade\":\"ASD\",\"trg_bn_cdr_sign\":\"GH\"},{\"name_trg\":\"FC & BC\",\"trg_bn_cdr_date\":\"2020-02-26\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GHG\",\"trg_bn_cdr_sign\":\"HGH\"},{\"name_trg\":\"BYT\",\"trg_bn_cdr_date\":\"2020-02-21\",\"trg_bn_cdr_marks\":\"G\",\"trg_bn_cdr_grade\":\"GHGH\",\"trg_bn_cdr_sign\":\"GH\"},{\"name_trg\":\"WT(tsoet)\",\"trg_bn_cdr_date\":\"2020-02-22\",\"trg_bn_cdr_marks\":\"GH\",\"trg_bn_cdr_grade\":\"GHGH\",\"trg_bn_cdr_sign\":\"B\"},{\"name_trg\":\"Written\",\"trg_bn_cdr_date\":\"2020-02-07\",\"trg_bn_cdr_marks\":\"BN\",\"trg_bn_cdr_grade\":\"BN\",\"trg_bn_cdr_sign\":\"BNBN\"},{\"name_trg\":\"Swimming\",\"trg_bn_cdr_date\":\"2020-02-12\",\"trg_bn_cdr_marks\":\"BN\",\"trg_bn_cdr_grade\":\"BNN\",\"trg_bn_cdr_sign\":\"BBN\"},{\"name_trg\":\"OT\",\"trg_bn_cdr_date\":\"2020-02-24\",\"trg_bn_cdr_marks\":\"BN\",\"trg_bn_cdr_grade\":\"BNNB\",\"trg_bn_cdr_sign\":\"NB\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `web_admin_login`
--

DROP TABLE IF EXISTS `web_admin_login`;
CREATE TABLE IF NOT EXISTS `web_admin_login` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `passwd` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `verify_code` varchar(200) NOT NULL,
  `status` enum('active','old') NOT NULL DEFAULT 'active',
  `phone_no` double NOT NULL,
  `profile_picture` varchar(200) NOT NULL,
  `default` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_admin_login`
--

INSERT INTO `web_admin_login` (`user_id`, `user_name`, `user_email`, `passwd`, `date`, `time`, `verify_code`, `status`, `phone_no`, `profile_picture`, `default`) VALUES
(1, 'Admin', 'admin@gmail.com', 'adcbc0209ae71f3e61d68be933046ee1a028f023', '2018-06-28', '06:06:58', '', 'active', 0, 'default_profile.jpg', 1),
(2, 'dileepsingh', 'user@gmail.com', '', '2020-02-13', '10:02:48', '', 'active', 99999999999, 'blog_5e3d78ba7ab54.png', 0),
(3, 'kkk', 'user@gmail.com', '', '2020-02-13', '10:02:22', '', 'active', 89789789789, 'blog_5e452b54e225c.png', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
