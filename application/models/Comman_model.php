<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Comman_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
    }

	public function array_from_get($fields){
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->get($field);
        }
        return $data;
    }

	public function array_from_post($fields){
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field);
        }
        return $data;
    }

    public function get_query($string,$single=false,$arry=false){
		$query = $this->db->query($string);		
		if($arry){
			if($single == TRUE) {
				$method = 'row';
			}
			else {
				$method = 'result';
			}
		}
		else{
			if($single == TRUE) {
				$method = 'row_array';
			}
			else {
				$method = 'result_array';
			}
		}
		//echo $this->db->last_query();die;
		return $query->$method(); 
	}

    public function get_by($table,$where,$order = false, $single = FALSE,$result_type=false){
        if($order){
			foreach($order as $set =>$value){				
	            $this->db->order_by($set,$value);
			}
		}
		if($where){
	        $this->db->where($where);
		}
		
        return $this->get($table, $single,$result_type);
    }

    public function get($table,$single = FALSE,$result_type=false){
      	if($single == TRUE) {//get 1 row
            $method = 'row_array';
			if($result_type==true){//get by object
	            $method = 'row';
			}
        } else {//get multi row
            $method = 'result_array';
			if($result_type==true){//get by object
	            $method = 'result';
			}
        }
        return $this->db->get($table)->$method();
    }
	

    public function update_data($table,$data,$where){
		$this->db->trans_start();
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update($table);
		$this->db->trans_complete();
		
		return true;
    }
    public function insert_data($table,$data){
		$this->db->trans_start();
		$this->db->set($data);
		$this->db->insert($table);
		$last_id = $this->db->insert_id();
		$this->db->trans_complete();
		
		return $last_id;
    }
	
}
