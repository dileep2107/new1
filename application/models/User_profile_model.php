<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class User_profile_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
    }  


	
    public function update_user_profile() {
		$user_id_ = $_POST['user_id'];
		
		
		
        $data = array(
			
			'user_name' => $_POST['user_name'],
			'user_email' => $_POST['user_email'],
			//'status' => $_POST['status'],
			'phone_no' => $_POST['phone_no'],
			'profile_picture' => $_POST['profile_picture'],
			
			'date' => date('Y-m-d'),
            'time' => date('H:m:s'),			
        );
		
		if($user_id_==0){
			$result = $this->db->insert('web_admin_login', $data);
			
		}else{
			$result = $this->db->update('web_admin_login', $data, array('user_id' => $user_id_));
			
		}
		if($result){
			return true;
		}else {
			return FALSE;
		}
	}

	 /*
     * This function is used to get public get_plans_all_list content category id 
     */
    public function get_all_user_profile_list() {
		$db_result = $this->db->get_where('web_admin_login',array('status' =>'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array();
                }
                if (array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array(
                         'user_id' => $row->user_id,
						'user_name' => $row->user_name,
						'user_email' => $row->user_email,
						'phone_no' => $row->phone_no,
						'profile_picture' => $row->profile_picture,
                    );
                    array_push($data_value, $data[$row->user_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	
	 /*
     * This function is used to get public get_plans_all_list content category id 
     */
    public function get_profile_details_by_id($user_id) {
		$db_result = $this->db->get_where('web_admin_login',array('user_id' =>$user_id,'status' =>'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array();
                }
                if (array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array(
                        'user_id' => $row->user_id,
						'user_name' => $row->user_name,
						'user_email' => $row->user_email,
						'phone_no' => $row->phone_no,
						'profile_picture' => $row->profile_picture,
						
                    );
                    array_push($data_value, $data[$row->user_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	

	public function remove_user_profile() {
		$user_id = $_POST['user_id'];
		
		$result = $this->db->delete('web_admin_login', array('user_id' => $user_id));

		return true;
	}

	public function update_user_password() { 
		$user_id = $_POST['user_id'];
		$data = array(
			'passwd' => sha1('admin' . (md5('admin' . $_POST['password']))),
		);
		$result = $this->db->update('web_admin_login', $data, array('user_id' => $user_id));
		return TRUE;
    }	
	
	
}
