<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Configure_access_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
		date_default_timezone_set('Asia/Kolkata');
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_faq() {
		
		$faq_id = (int)$_POST['faq_id'];
		$data = array(
            'faq_question' => $_POST['faq_question'],
            'faq_answer' => nl2br($_POST['faq_answer']),
            'date' => date('Y-m-d'),
        );
		if($faq_id == 0){
			$result = $this->db->insert('web_faq', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_faq', $data, array('faq_id' => $faq_id));      
			return TRUE;
		}  	$result = $this->db->insert('web_partner',array('partner_id'=>$_POST['partner_id']));     
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_refund_policy
     */
    public function get_list_of_faqs() {
		$db_result = $this->db->get_where('web_faq', array('status !=' => 'remove'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->faq_id, $data)) {
                    $data[$row->faq_id] = array();
                }
                if (array_key_exists($row->faq_id, $data)) {
                    $data[$row->faq_id] = array(
                        'faq_id' => $row->faq_id,
						'faq_question' => $row->faq_question,
						'faq_answer' => $row->faq_answer,
						'status' => $row->status,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->faq_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_details_of_faq_by_id
     */
    public function get_details_of_faq_by_id($segment) {
		$db_result = $this->db->get_where('web_faq', array('status !=' => 'remove','faq_id'=>$segment));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->faq_id, $data)) {
                    $data[$row->faq_id] = array();
                }
                if (array_key_exists($row->faq_id, $data)) {
                    $data[$row->faq_id] = array(
                        'faq_id' => $row->faq_id,
						'faq_question' => $row->faq_question,
						'faq_answer' => $row->faq_answer,
						'status' => $row->status,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->faq_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_faq_status() {
		$data = array(
            'status' => 'remove',
        );
        $result = $this->db->update('web_faq', $data, array('faq_id' => $_POST['faq_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_coupon() {
		
		$coupon_id = (int)$_POST['coupon_id'];
		$data = array(
            'coupon_code' => $_POST['coupon_code'],
            'coupon_discount' => $_POST['coupon_discount'],
            'brief_description' => $_POST['brief_description'],
			'start_date' => $_POST['start_date'],
			'end_date' => $_POST['end_date'],
			'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($coupon_id == 0){
			$result = $this->db->insert('web_coupon', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_coupon', $data, array('coupon_id' => $coupon_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get coupon list
     */
    public function get_list_of_coupon() {
		$db_result = $this->db->get_where('web_coupon', array('status !=' => 'remove'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->coupon_id, $data)) {
                    $data[$row->coupon_id] = array();
                }
                if (array_key_exists($row->coupon_id, $data)) {
                    $data[$row->coupon_id] = array(
                        'coupon_id' => $row->coupon_id,
						'coupon_code' => $row->coupon_code,
						'coupon_discount' => $row->coupon_discount,
						'brief_description' => $row->brief_description,
						'start_date' => $row->start_date,
						'end_date' => $row->end_date,
						'date'=> $row->date,
						'status'=> $row->status,
                    );
                    array_push($data_value, $data[$row->coupon_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_coupon_status() {
		$data = array(
            'status' => $_POST['coupon_status'],
        );
        $result = $this->db->update('web_coupon', $data, array('coupon_id' => $_POST['coupon_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_slider
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_slider() {
		$slider_id = (int)$_POST['slider_id'];
		$data = array(
            'heading_tag' => $_POST['heading_tag'],
            'navigation_link' => $_POST['navigation_link'],
			'slider_image' => $_POST['slider_image'],
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($slider_id == 0){
			$result = $this->db->insert('web_slider', $data);      
			if ($result) {
				return $this->db->insert_id();;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_slider', $data, array('slider_id' => $slider_id));      
			return TRUE;			
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_slider
     */
    public function get_list_of_slider() {
		$db_result = $this->db->get_where('web_slider');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
			#print_r($db_result->row());die();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array();
                }
                if (array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array(
                        'slider_id' => $row->slider_id,
						'heading_tag' => $row->heading_tag,
						'navigation_link' => $row->navigation_link,
						'slider_image' => $row->slider_image,
                        'date'=> $row->date,
                        'status'=> $row->status,
                    );
                    array_push($data_value, $data[$row->slider_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_slider
     */
    public function get_details_of_slider_by_id($slider_id) {
		$db_result = $this->db->get_where('web_slider', array('status !=' => 'remove','slider_id'=>$slider_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
			
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array();
                }
                if (array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array(
                        'slider_id' => $row->slider_id,
						'heading_tag' => $row->heading_tag,
						'navigation_link' => $row->navigation_link,
						'slider_image' => $row->slider_image,
                        'date'=> $row->date,
                        'status'=> $row->status,
                    );
                    array_push($data_value, $data[$row->slider_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_slider_status() {
		$data = array(
            'status' => $_POST['slider_status'],
        );
        $result = $this->db->update('web_slider', $data, array('slider_id' => $_POST['slider_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_banner
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_banner() {
		$banner_id = (int)$_POST['banner_id'];
		$data = array(
            'banner_position' => $_POST['banner_position'],
            'heading_tag' => $_POST['heading_tag'],
            'navigation_link' => $_POST['navigation_link'],
			'banner_image' => $_POST['banner_image'],
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($banner_id == 0){
			$result = $this->db->insert('web_banner', $data);      
			if ($result) {
				return $this->db->insert_id();;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_banner', $data, array('banner_id' => $banner_id));      
			if ($this->db->affected_rows()) {
				return $banner_id;
			} else {
				return FALSE;
			}
		}  	
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_banner
     */
    public function get_list_of_banner() {
		$db_result = $this->db->get_where('web_banner', array('status !=' => 'remove'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
			foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->banner_id, $data)) {
                    $data[$row->banner_id] = array();
                }
                if (array_key_exists($row->banner_id, $data)) {
                    $data[$row->banner_id] = array(
                        'banner_id' => $row->banner_id,
						'banner_position' => $row->banner_position,
						'heading_tag' => $row->heading_tag,
						'navigation_link' => $row->navigation_link,
						'banner_image' => $row->banner_image,
                        'date'=> $row->date,
                        'status'=> $row->status,
                    );
                    array_push($data_value, $data[$row->banner_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_details_of_banner_by_id
     */
    public function get_details_of_banner_by_id($banner_id) {
		$db_result = $this->db->get_where('web_banner', array('status !=' => 'remove','banner_id'=>$banner_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
			foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->banner_id, $data)) {
                    $data[$row->banner_id] = array();
                }
                if (array_key_exists($row->banner_id, $data)) {
                    $data[$row->banner_id] = array(
                        'banner_id' => $row->banner_id,
						'banner_position' => $row->banner_position,
						'heading_tag' => $row->heading_tag,
						'navigation_link' => $row->navigation_link,
						'banner_image' => $row->banner_image,
                        'date'=> $row->date,
                        'status'=> $row->status,
                    );
                    array_push($data_value, $data[$row->banner_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_banner_status() {
		$data = array(
            'status' => $_POST['status'],
        );
        $result = $this->db->update('web_banner', $data, array('banner_id' => $_POST['banner_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_subscribe_email
     */
    public function get_list_of_subscribe_email() {
		$db_result = $this->db->get_where('web_subscribe');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->subscribe_id, $data)) {
                    $data[$row->subscribe_id] = array();
                }
                if (array_key_exists($row->subscribe_id, $data)) {
                    $data[$row->subscribe_id] = array(
                        'subscribe_id' => $row->subscribe_id,
						'subscribe_no' => $row->subscribe_no,
						'subscribe_time' => $row->subscribe_time,
						'subscribe_date' => $row->subscribe_date,
					);
                    array_push($data_value, $data[$row->subscribe_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_subcribe_email from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_subscriber_detail() {
       
        $result = $this->db->delete('web_subscribe', array('subscribe_id' => $_POST['subscribe_id']));
       
            return TRUE;
      
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update testimonials
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_testimonials() {
		date_default_timezone_set('Asia/Kolkata');
		$testimonials_id = (int)$_POST['testimonials_id'];
		$data = array(
            'testimonials_name' => $_POST['testimonials_name'],
            'position' => $_POST['position'],
			'comments' => nl2br($_POST['comments']),
			'testimonials_image' => $_POST['testimonials_image'],
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($testimonials_id == 0){
			$result = $this->db->insert('web_testimonials', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_testimonials', $data, array('testimonials_id' => $testimonials_id));      
			
				return TRUE;
			
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_testimonials
     */
    public function get_list_of_testimonials() {
		$db_result = $this->db->get_where('web_testimonials', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->testimonials_id, $data)) {
                    $data[$row->testimonials_id] = array();
                }
                if (array_key_exists($row->testimonials_id, $data)) {
                    $data[$row->testimonials_id] = array(
                        'testimonials_id' => $row->testimonials_id,
						'testimonials_name' => $row->testimonials_name,
						'position' => $row->position,
						'comments' => $row->comments,
						'testimonials_image' => $row->testimonials_image,
                        'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->testimonials_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_news() {
		$db_result = $this->db->get_where('hr_news');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->news_id, $data)) {
                    $data[$row->news_id] = array();
                }
                if (array_key_exists($row->news_id, $data)) {
                    $data[$row->news_id] = array(
                        'news_id' => $row->news_id,
						'news_date' => $row->news_date,
						'news_type' => $row->news_type,
						'pblication_name' => $row->pblication_name,
						'headline' => $row->headline,
						'headline_url' => $row->headline_url,
                        'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->news_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_testimonials_status() {
		$data = array(
            'status' => $_POST['status'],
        );
        $result = $this->db->update('web_testimonials', $data, array('testimonials_id' => $_POST['testimonials_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_contact_us
     */
   public function get_list_of_contact_us() {
		$db_result = $this->db->get_where('web_contact_us',array('contact_status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->contact_id, $data)) {
                    $data[$row->contact_id] = array();
                }
                if (array_key_exists($row->contact_id, $data)) {
                    $data[$row->contact_id] = array(
                        'contact_id' => $row->contact_id,
						'contact_name' => $row->contact_name,
						'contact_email' => $row->contact_email,
						'contact_subject' => $row->contact_subject,
						'contact_message' => $row->contact_message,
						'date'=> $row->date,
						'contact_status' => $row->contact_status,
                    );
                    array_push($data_value, $data[$row->contact_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to mark_contact_us from the list
     * 
     * @access		public
     * @since		1.0.0
     */
 /*   public function mark_contact_us() {
		$contact_status='';
		if($_POST['contact_status']=='active'){
			$contact_status='old';
		}else{
			$contact_status='active';
		}
		$data = array(
            'contact_status' => $contact_status,
        );
        $result = $this->db->update('web_contact_us', $data, array('contact_id' => $_POST['contact_id']));
		if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}*/
	
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_slider from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_slider() {
        $result = $this->db->delete('web_slider', array('slider_id' => $_POST['slider_id']));
        return TRUE;        
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_testimonials from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_testimonials() {
        $result = $this->db->delete('web_testimonials', array('testimonials_id' => $_POST['testimonials_id']));
         return TRUE;  
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update News in MPS 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_news() {
		date_default_timezone_set('Asia/Kolkata');
		$news_id = (int)$_POST['news_id'];
		$data = array(
            'news_date' => $_POST['news_date'],
            'news_type' => $_POST['news_type'],
            'pblication_name' =>$_POST['pblication_name'],
            'headline' =>$_POST['headline'],
            'headline_url' =>$_POST['headline_url'],
			'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($news_id == 0){
			$result = $this->db->insert('hr_news', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('hr_news', $data, array('news_id' => $news_id));      
			
				return TRUE;
			
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_mps_news from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_news() {
        $result = $this->db->delete('hr_news', array('news_id' => $_POST['news_id']));
       
            return TRUE;
        
	
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get public content_by content category id 
     */
    public function get_public_content_by_content_cat_id($content_cat_id) {
		$db_result = $this->db->get_where('web_public_content', array('status' => 'active','content_id' => $content_cat_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array();
                }
                if (array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array(
                        'content_id' => $row->content_id,
						'content_cat_id' => $row->content_cat_id,
						'content' => $row->content,
						'date'=> $row->date,
						'status'=> $row->status,
                    );
                    array_push($data_value, $data[$row->content_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for update_public_content into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_public_content() {
		$content_id = $_POST['content_id'];
		$user_id = $this->session->userdata('userID');
        $data = array(
			'FK_added_by' => $user_id,
			'content_cat_id' => $_POST['content_cat_id'],
			'content' => $_POST['content'],
			'date' => date('Y-m-d'),
            'time' => date('H:m:s'),			
        );
		
		
			$result = $this->db->update('web_public_content', $data, array('content_id' => $content_id));
		
		
			return true;
		
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_module
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_image() {
		
		$data = array(
            'img_name' => 'dg',
            'image' => $_POST['image'],
            'description' => $_POST['description'],
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		$result = $this->db->insert('web_images', $data);      
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_modules
     */
    public function get_list_of_images() {
		$db_result = $this->db->get_where('web_images', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->img_id, $data)) {
                    $data[$row->img_id] = array();
                }
                if (array_key_exists($row->img_id, $data)) {
                    $data[$row->img_id] = array(
                        'img_id' => $row->img_id,
						'img_name' => $row->img_name,
						'image' => $row->image,
						'description' => $row->description,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->img_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to udate_image_detail
     * 
     * @access		public
     * @since		1.0.0
     */
    public function udate_image_detail() {
		
		$result = $this->db->update('web_images',array('img_id'=>$_POST['img_id']));      
		
			return TRUE;
		
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_image() {
       
        $result = $this->db->delete('web_images', array('img_id' => $_POST['img_id']));
       
            return TRUE;
      
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_refund_policy
     */
    public function get_list_of_faqs1() {
		$db_result = $this->db->get_where('web_faq1', array('status !=' => 'remove'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->faq1_id, $data)) {
                    $data[$row->faq1_id] = array();
                }
                if (array_key_exists($row->faq1_id, $data)) {
                    $data[$row->faq1_id] = array(
                        'faq1_id' => $row->faq1_id,
						'faq_question' => $row->faq_question,
						'faq_answer' => $row->faq_answer,
						'status' => $row->status,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->faq1_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_faq1() {
		
		$faq1_id = (int)$_POST['faq1_id'];
		$data = array(
            'faq_question' => $_POST['faq_question'],
            'faq_answer' => nl2br($_POST['faq_answer']),
            'date' => date('Y-m-d'),
        );
		if($faq1_id == 0){
			$result = $this->db->insert('web_faq1', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_faq1', $data, array('faq1_id' => $faq1_id));      
			return TRUE;
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_details_of_faq_by_id
     */
    public function get_details_of_faq1_by_id($segment) {
		$db_result = $this->db->get_where('web_faq1', array('status !=' => 'remove','faq1_id'=>$segment));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->faq1_id, $data)) {
                    $data[$row->faq1_id] = array();
                }
                if (array_key_exists($row->faq1_id, $data)) {
                    $data[$row->faq1_id] = array(
                        'faq1_id' => $row->faq1_id,
						'faq_question' => $row->faq_question,
						'faq_answer' => $row->faq_answer,
						'status' => $row->status,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->faq1_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_faq1_status() {
		$data = array(
            'status' => 'remove',
        );
        $result = $this->db->update('web_faq1', $data, array('faq1_id' => $_POST['faq1_id']));
         
            return TRUE;
         
	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_refund_policy
     */
    public function get_list_of_course() {
		$db_result = $this->db->get_where('add_course');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->course_id, $data)) {
                    $data[$row->course_id] = array();
                }
                if (array_key_exists($row->course_id, $data)) {
                    $data[$row->course_id] = array(
                        'course_id' => $row->course_id,
						'course_name' => $row->course_name,
						'course_detail' => $row->course_detail,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->course_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_course() {
		
		$course_id = (int)$_POST['course_id'];
		$data = array(
            'course_name' => $_POST['course_name'],
            'course_detail' => nl2br($_POST['course_detail']),
            'date' => date('Y-m-d'),
        );
		if($course_id == 0){
			$result = $this->db->insert('add_course', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('add_course', $data, array('course_id' => $course_id));      
			return TRUE;
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_details_of_faq_by_id
     */
    public function get_details_of_course_by_id($segment) {
		$db_result = $this->db->get_where('add_course', array('course_id'=>$segment));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->course_id, $data)) {
                    $data[$row->course_id] = array();
                }
                if (array_key_exists($row->course_id, $data)) {
                    $data[$row->course_id] = array(
                        'course_id' => $row->course_id,
						'course_name' => $row->course_name,
						'course_detail' => $row->course_detail,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->course_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_course_status() {
		$data = array(
            'status' => 'remove',
        );
        $result = $this->db->delete('add_course', array('course_id' => $_POST['course_id']));
         
            return TRUE;
         
	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_refund_policy
     */
    public function get_list_of_headings() {
		$db_result = $this->db->get_where('web_headings');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->heading_id, $data)) {
                    $data[$row->heading_id] = array();
                }
                if (array_key_exists($row->heading_id, $data)) {
                    $data[$row->heading_id] = array(
                        'heading_id' => $row->heading_id,
						'page_title' => $row->page_title,
						'page_heading' => $row->page_heading,
                    );
                    array_push($data_value, $data[$row->heading_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_heading() {
		
		$heading_id = (int)$_POST['heading_id'];
		$data = array(
            'page_title' => $_POST['page_title'],
            'page_heading' => $_POST['page_heading'],
        );
		if($heading_id == 0){
			$result = $this->db->insert('web_headings', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_headings', $data, array('heading_id' => $heading_id));      
			return TRUE;
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_details_of_faq_by_id
     */
    public function get_details_of_heading_by_id($segment) {
		$db_result = $this->db->get_where('web_headings', array('heading_id'=>$segment));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->heading_id, $data)) {
                    $data[$row->heading_id] = array();
                }
                if (array_key_exists($row->heading_id, $data)) {
                    $data[$row->heading_id] = array(
                        'heading_id' => $row->heading_id,
						'page_title' => $row->page_title,
						'page_heading' => $row->page_heading,
						'status' => $row->status,
                    );
                    array_push($data_value, $data[$row->heading_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_heading_status() {
		$data = array(
            'status' => 'remove',
        );
        $result = $this->db->delete('web_headings', array('heading_id' => $_POST['heading_id']));
         
            return TRUE;
         
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_modules
     */
    public function get_list_of_partner() {
		$db_result = $this->db->get_where('web_partner');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->partner_id, $data)) {
                    $data[$row->partner_id] = array();
                }
                if (array_key_exists($row->	partner_id, $data)) {
                    $data[$row->partner_id] = array(
                        'partner_id' => $row->partner_id,
						'partner_icon' => $row->partner_icon,
						'partner_detail' => $row->partner_detail,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->partner_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_module
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_image_partner() {
		
		$data = array(
            'partner_id' => 'partner_id',
            'partner_icon' => $_POST['partner_icon'],
			'partner_detail' => $_POST['partner_detail'],
            'date' => date('Y-m-d'),
        );
		$result = $this->db->insert('web_partner', $data);      
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	

	//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_partner_image() {
       
        $result = $this->db->delete(' web_partner', array('partner_id' => $_POST['partner_id']));
       
            return TRUE;
      
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_partner_image_detail() {
		
		
		$data = array(
			'partner_icon' => $_POST['image'],
            'partner_detail' =>$_POST['partner_detail'],
            'date' => date('Y-m-d'),
        );
		
		$result = $this->db->insert('web_partner', $data);      
	
		return TRUE;
		 
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_refund_policy
     */
    public function list_of_bookmygenrator() {
		$db_result = $this->db->get_where('web_use_generator');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->bookmygen_id, $data)) {
                    $data[$row->bookmygen_id] = array();
                }
                if (array_key_exists($row->bookmygen_id, $data)) {
                    $data[$row->bookmygen_id] = array(
                        'bookmygen_id' => $row->bookmygen_id,
						'bookmygen_heading' => $row->bookmygen_heading,
						'bookmygen_description' => $row->bookmygen_description,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->bookmygen_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_bookmygenerator() {
		
		$bookmygen_id = (int)$_POST['bookmygen_id'];
		$data = array(
			'bookmygen_id' => $_POST['bookmygen_id'],
            'bookmygen_heading' => $_POST['bookmygen_heading'],
            'bookmygen_description' => nl2br($_POST['bookmygen_description']),
            'date' => date('Y-m-d'),
        );
		if($bookmygen_id == 0){
			$result = $this->db->insert('web_use_generator', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_use_generator', $data, array('bookmygen_id' => $bookmygen_id));      
			return TRUE;
		}   
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_module
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_bookmygenerator() {
		
		$data = array(
            'bookmygen_id' => 'bookmygen_id',
            'bookmygen_heading' => $_POST['bookmygen_heading'],
			'bookmygen_description' => $_POST['bookmygen_description'],
            'date' => date('Y-m-d'),
        );
		$result = $this->db->insert('web_use_generator', $data);      
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_bookmygenerator() {
       
        $result = $this->db->delete(' web_use_generator', array('bookmygen_id' => $_POST['bookmygen_id']));
       
            return TRUE;
      
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_bookmygenerator_status() {
		$data = array(
            'status' => 'remove',
        );
        $result = $this->db->update('web_use_generator', $data, array('bookmygen_id' => $_POST['bookmygen_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_refund_policy
     */
    public function list_of_benefits() {
		$db_result = $this->db->get_where(' web_benefits');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->benefits_id, $data)) {
                    $data[$row->benefits_id] = array();
                }
                if (array_key_exists($row->benefits_id, $data)) {
                    $data[$row->benefits_id] = array(
                        'benefits_id' => $row->benefits_id,
						'benefits_user' => $row->benefits_user,
						'benefits_description' => $row->benefits_description,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->benefits_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update coupon
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_benefits() {
		
		$benefits_id = (int)$_POST['benefits_id'];
		$data = array(
			'benefits_id' => $_POST['benefits_id'],
            'benefits_user' => $_POST['benefits_user'],
            'benefits_description' => nl2br($_POST['benefits_description']),
            'date' => date('Y-m-d'),
        );
		if($benefits_id == 0){
			$result = $this->db->insert('web_benefits', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_benefits', $data, array('benefits_id' => $benefits_id));      
			return TRUE;
		}   
	}
	
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to update_module
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_benefits() {
		
		$data = array(
            'benefits_id' => 'benefits_id',
            'benefits_user' => $_POST['benefits_user'],
			'benefits_description' => $_POST['benefits_description'],
            'date' => date('Y-m-d'),
        );
		$result = $this->db->insert('web_benefits', $data);      
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_benefits() {
       
        $result = $this->db->delete(' web_benefits', array('benefits_id' => $_POST['benefits_id']));
       
            return TRUE;
      
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_benefits_status() {
		$data = array(
            'status' => 'remove',
        );
        $result = $this->db->update('web_benefits', $data, array('benefits_id' => $_POST['benefits_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_requirement() {
		$db_result = $this->db->get_where('post_requirement');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->post_id, $data)) {
                    $data[$row->post_id] = array();
                }
                if (array_key_exists($row->post_id, $data)) {
                    $data[$row->post_id] = array(
                        'post_id' => $row->post_id,
						'full_name' => $row->full_name,
						'posted_by' => $row->posted_by,
						'mobile_no' => $row->mobile_no,
						'email_id' => $row->email_id,
						'end_user' => $row->end_user,
                        'asset_owner'=> $row->asset_owner,
                        'manufacturer'=> $row->manufacturer,
                        'detail_requirement'=> $row->detail_requirement,
                    );
                    array_push($data_value, $data[$row->post_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update News in MPS 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_requirement() {
		date_default_timezone_set('Asia/Kolkata');
		$post_id = (int)$_POST['post_id'];
		$data = array(
            'full_name' => $_POST['full_name'],
            'posted_by' => $_POST['posted_by'],
            'mobile_no' =>$_POST['mobile_no'],
            'email_id' =>$_POST['email_id'],
            'end_user' =>$_POST['end_user'],
            'asset_owner' =>$_POST['asset_owner'],
            'manufacturer' =>$_POST['manufacturer'],
            'detail_requirement' =>$_POST['detail_requirement'],
			
        );
		if($post_id == 0){
			$result = $this->db->insert('post_requirement', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('post_requirement', $data, array('post_id' => $post_id));      
			
				return TRUE;
			
		}  	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_requirement() {
       
        $result = $this->db->delete(' post_requirement', array('post_id' => $_POST['post_id']));
       
            return TRUE;
      
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_saving_rental() {
		$db_result = $this->db->get_where('saving_rental');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->saving_id, $data)) {
                    $data[$row->saving_id] = array();
                }
                if (array_key_exists($row->saving_id, $data)) {
                    $data[$row->saving_id] = array(
                        'saving_id' => $row->saving_id,
						'full_name' => $row->full_name,
						'posted_by' => $row->posted_by,
						'other_posted' => $row->other_posted,
						'mobile_no' => $row->mobile_no,
						'email_id' => $row->email_id,
						'generator_rental' => $row->generator_rental,
                        'generator_lease_hiring'=> $row->generator_lease_hiring,
                        'upload_assets'=> $row->upload_assets,
                        'upload_previous_contract'=> $row->upload_previous_contract,
                        'detail_requirement'=> $row->detail_requirement,
                    );
                    array_push($data_value, $data[$row->saving_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update News in MPS 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_saving_rental() {
		date_default_timezone_set('Asia/Kolkata');
		$saving_id = (int)$_POST['saving_id'];
		$data = array(
            'full_name' => $_POST['full_name'],
            'posted_by' => $_POST['posted_by'],
            'other_posted' => $_POST['other_posted'],
            'mobile_no' =>$_POST['mobile_no'],
            'email_id' =>$_POST['email_id'],
            'generator_rental' =>$_POST['generator_rental'],
            'generator_lease_hiring' =>$_POST['generator_lease_hiring'],
            'upload_assets' =>$_POST['upload_assets'],
            'upload_previous_contract' =>$_POST['upload_previous_contract'],
            'detail_requirement' =>$_POST['detail_requirement'],
			
        );
		if($saving_id == 0){
			$result = $this->db->insert('saving_rental', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('saving_rental', $data, array('saving_id' => $saving_id));      
			
				return TRUE;
			
		}  	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_saving_rental() {
       
        $result = $this->db->delete(' saving_rental', array('saving_id' => $_POST['saving_id']));
       
            return TRUE;
      
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_profile() {
		$db_result = $this->db->get_where('register');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array();
                }
                if (array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array(
                        'user_id' => $row->user_id,
						'full_name' => $row->full_name,
						'home_address' => $row->home_address,
						'mobile_no' => $row->mobile_no,
						'profile_type' => $row->profile_type,
						'user_email' => $row->user_email,
                    );
                    array_push($data_value, $data[$row->user_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update News in MPS 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_profile() {
		date_default_timezone_set('Asia/Kolkata');
		$user_id = (int)$_POST['user_id'];
		$data = array(
            'full_name' => $_POST['full_name'],
            'home_address' => $_POST['home_address'],
            'mobile_no' => $_POST['mobile_no'],
            'profile_type' =>$_POST['profile_type'],
            'user_email' =>$_POST['user_email'],
        );
		if($user_id == 0){
			$result = $this->db->insert('register', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('register', $data, array('user_id' => $user_id));      
			
				return TRUE;
			
		}  	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_profile() {
       
        $result = $this->db->delete(' register', array('user_id' => $_POST['user_id']));
       
            return TRUE;
      
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
   /* public function get_list_of_contact_us() {
		$db_result = $this->db->get_where('contact_us');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->contact_us_id, $data)) {
                    $data[$row->contact_us_id] = array();
                }
                if (array_key_exists($row->contact_us_id, $data)) {
                    $data[$row->contact_us_id] = array(
                        'contact_us_id' => $row->contact_us_id,
						'contact_us_message' => $row->contact_us_message,
						'contact_us_subject' => $row->contact_us_subject,
						'contact_us_name' => $row->contact_us_name,
						'contact_us_email' => $row->contact_us_email,
						
                    );
                    array_push($data_value, $data[$row->contact_us_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }*/
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_contact_us from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_contact_us() {
		
         $result = $this->db->delete(' contact_us', array('contact_us_id' => $_POST['contact_us_id']));
			return TRUE;
	
	}
	
	
			//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_feedback() {
		$db_result = $this->db->get_where('feedback_us');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->feedback_id, $data)) {
                    $data[$row->feedback_id] = array();
                }
                if (array_key_exists($row->feedback_id, $data)) {
                    $data[$row->feedback_id] = array(
                        'feedback_id' => $row->feedback_id,
						'feedback_message' => $row->feedback_message,
						'feedback_name' => $row->feedback_name,
						'feedback_email' => $row->feedback_email,
						'date' => $row->date,
						
                    );
                    array_push($data_value, $data[$row->feedback_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_feedback from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_feedback() {
		
         $result = $this->db->delete(' feedback_us', array('feedback_id' => $_POST['feedback_id']));
			return TRUE;
	
	}
	
			//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_refer_friends() {
		$db_result = $this->db->get_where('refer_friend');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->refer_id, $data)) {
                    $data[$row->refer_id] = array();
                }
                if (array_key_exists($row->refer_id, $data)) {
                    $data[$row->refer_id] = array(
                        'refer_id' => $row->refer_id,
						'user_id' => $row->user_id,
						'refer_name' => $row->refer_name,
						'refer_email' => $row->refer_email,
						'refer_number' => $row->refer_number,
						'friend_number' => $row->friend_number,
						
                    );
                    array_push($data_value, $data[$row->refer_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_feedback from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_refer() {
		
         $result = $this->db->delete(' refer_friend', array('refer_id' => $_POST['refer_id']));
			return TRUE;
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_testimonials
     */
    public function get_list_of_contact_info() {
		$db_result = $this->db->get_where('contact_info');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->contact_info_id, $data)) {
                    $data[$row->contact_info_id] = array();
                }
                if (array_key_exists($row->contact_info_id, $data)) {
                    $data[$row->contact_info_id] = array(
                        'contact_info_id' => $row->contact_info_id,
                        'contact_name' => $row->contact_name,
						'address' => $row->address,
						'landline_no' => $row->landline_no,
						'mobile_no' => $row->mobile_no,
						'user_email' => $row->user_email,
						'user_email_heading' => $row->user_email_heading,
						'facebook_link' => $row->facebook_link,
						'twitter_link' => $row->twitter_link,
						'google_plus_link' => $row->google_plus_link,
						'youtube_link' => $row->youtube_link,
						'linked_in_link' => $row->linked_in_link,
						'marque' => $row->marque,
                        
                    );
                    array_push($data_value, $data[$row->contact_info_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_contact_info_status() {
		
        $result = $this->db->update('contact_info', $data, array('contact_info_id' => $_POST['contact_info_id']));
        return TRUE;
	}
	//-------------------------------------------------------------------------
    /*
     * This function is used to update admin contact_info
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_contact_info() {
		date_default_timezone_set('Asia/Kolkata');
		$contact_info_id = (int)$_POST['contact_info_id'];
		$data = array(
            'contact_name' => $_POST['contact_name'],
            'address' => nl2br($_POST['address']),
			'landline_no' => $_POST['landline_no'],
			'mobile_no' => $_POST['mobile_no'],
			'user_email' => $_POST['user_email'],
			'user_email_heading' => $_POST['user_email_heading'],
			'facebook_link' => $_POST['facebook_link'],
			'twitter_link' => $_POST['twitter_link'],
			'google_plus_link' => $_POST['google_plus_link'],
			'youtube_link' => $_POST['youtube_link'],
			'linked_in_link' => $_POST['linked_in_link'],
			'marque' => $_POST['marque'],
			
			
        );
		if($contact_info_id == 0){
			$result = $this->db->insert('contact_info', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('contact_info', $data, array('contact_info_id' => $contact_info_id));      
			return TRUE;
			
		}  	
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_services
     */
    public function get_list_of_services($service_id) {
		$db_result = $this->db->get_where('services');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'service_id' => $row->service_id,
						'cat_id' => $row->cat_id,
						'service_cat' => $row->service_cat,
						'service_heading' => $row->service_heading,
						'service_image' => $row->service_image,
						'service_description' => $row->service_description,
						
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_services
     */
    public function get_list_of_services_by_id($service_id) {
		$db_result = $this->db->get_where('services', array('service_id'=>$service_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'service_id' => $row->service_id,
						'cat_id' => $row->cat_id,
						'service_cat' => $row->service_cat,
						'service_heading' => $row->service_heading,
						'service_image' => $row->service_image,
						'service_description' => $row->service_description,
						
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
			//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_testimonials
     */
    public function get_list_of_services_view() {
		$db_result = $this->db->get_where('services');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'service_id' => $row->service_id,
                        'cat_id' => $row->cat_id,
                        'service_cat' => $row->service_cat,
						'service_heading' => $row->service_heading,
						'service_image' => $row->service_image,
						'service_description' => $row->service_description,
						
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	

	//-------------------------------------------------------------------------
    /*
     * This function is used to update_services
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_services() {
		$service_id = (int)$_POST['service_id'];
		
		$data = array(
            
            'service_cat' => $_POST['service_cat'],
           
            'service_heading' => $_POST['service_heading'],
            'service_image' => $_POST['service_image'],
            'service_description' => $_POST['service_description'],
           
        );
		if($service_id == 0){
			$result = $this->db->insert('services', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('services', $data, array('service_id' => $service_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_contact_us from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_services() {
		
         $result = $this->db->delete(' services', array('service_id' => $_POST['service_id']));
			return TRUE;
	
	}
	
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_services_detail_enquiry() {
		$db_result = $this->db->get_where('services_detail_enquiry');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_detail_id, $data)) {
                    $data[$row->service_detail_id] = array();
                }
                if (array_key_exists($row->service_detail_id, $data)) {
                    $data[$row->service_detail_id] = array(
                        'service_detail_id' => $row->service_detail_id,
						'service_detail_message' => $row->service_detail_message,
						'service_detail_name' => $row->service_detail_name,
						'service_detail_contact' => $row->service_detail_contact,
						'service_detail_email' => $row->service_detail_email,
						
                    );
                    array_push($data_value, $data[$row->service_detail_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
		
		//-------------------------------------------------------------------------
    /*
     * This function is used to update News in MPS 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_services_detail_enquiry() {
		date_default_timezone_set('Asia/Kolkata');
		$service_detail_id = (int)$_POST['service_detail_id'];
		$data = array(
            'service_detail_message' => $_POST['service_detail_message'],
            'service_detail_name' => $_POST['service_detail_name'],
            'service_detail_contact' =>$_POST['service_detail_contact'],
            'service_detail_email' =>$_POST['service_detail_email'],
           
        );
		if($service_detail_id == 0){
			$result = $this->db->insert('services_detail_enquiry', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('services_detail_enquiry', $data, array('service_detail_id' => $service_detail_id));      
			
				return TRUE;
			
		}  	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
	 
    public function remove_services_detail_enquiry() {
       
        $result = $this->db->delete(' services_detail_enquiry', array('service_detail_id' => $_POST['service_detail_id']));
       
            return TRUE;
      
    }
	
	 /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_product_detail_enquiry() {
		$db_result = $this->db->get_where('product_detail_enquiry');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->product_detail_id, $data)) {
                    $data[$row->product_detail_id] = array();
                }
                if (array_key_exists($row->product_detail_id, $data)) {
                    $data[$row->product_detail_id] = array(
                        'product_detail_id' => $row->product_detail_id,
						'product_detail_message' => $row->product_detail_message,
						'product_detail_name' => $row->product_detail_name,
						'product_detail_contact' => $row->product_detail_contact,
						'product_detail_email' => $row->product_detail_email,
						
                    );
                    array_push($data_value, $data[$row->product_detail_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
		
		//-------------------------------------------------------------------------
    /*
     * This function is used to update News in MPS 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_product_detail_enquiry() {
		date_default_timezone_set('Asia/Kolkata');
		$product_detail_id = (int)$_POST['product_detail_id'];
		$data = array(
            'product_detail_message' => $_POST['product_detail_message'],
            'product_detail_name' => $_POST['product_detail_name'],
            'product_detail_contact' =>$_POST['product_detail_contact'],
            'product_detail_email' =>$_POST['product_detail_email'],
           
        );
		if($product_detail_id == 0){
			$result = $this->db->insert('product_detail_enquiry', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('product_detail_enquiry', $data, array('product_detail_id' => $product_detail_id));      
			
				return TRUE;
			
		}  	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_product_detail_enquiry() {
       
        $result = $this->db->delete(' product_detail_enquiry', array('product_detail_id' => $_POST['product_detail_id']));
       
            return TRUE;
      
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_aboutus
     */
    public function get_list_of_aboutus() {
		$db_result = $this->db->get_where('about_us');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array();
                }
                if (array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array(
                        'aboutus_id' => $row->aboutus_id,
						'brief_description' => $row->brief_description,
						'full_description' => $row->full_description,
						'aboutus_image' => $row->aboutus_image,
						'our_mission_image' => $row->our_mission_image,
						'mission_heading' => $row->mission_heading,
						'mission_description' => $row->mission_description,
						'our_history_image' => $row->our_history_image,
						'history_heading' => $row->history_heading,
						'history_description' => $row->history_description,
						'our_vision_image' => $row->our_vision_image,
						'vision_heading' => $row->vision_heading,
						'vision_description' => $row->vision_description,
						
                    );
                    array_push($data_value, $data[$row->aboutus_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update aboutus
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_aboutus() {
		date_default_timezone_set('Asia/Kolkata');
		$aboutus_id = (int)$_POST['aboutus_id'];
		$data = array(
            'brief_description' => nl2br($_POST['brief_description']),
            'full_description' =>  nl2br($_POST['full_description']),
			'aboutus_image' =>  $_POST['aboutus_image'],
			'our_mission_image' => $_POST['our_mission_image'],
			'mission_heading' => $_POST['mission_heading'],
			'mission_description' =>  nl2br($_POST['mission_description']),
			'our_history_image' => $_POST['our_history_image'],
			'history_heading' => $_POST['history_heading'],
			'history_description' =>  nl2br($_POST['history_description']),
			'our_vision_image' => $_POST['our_vision_image'],
			'vision_heading' => $_POST['vision_heading'],
			'vision_description' =>  nl2br($_POST['vision_description']),
           
        );
		if($aboutus_id == 0){
			$result = $this->db->insert('about_us', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('about_us', $data, array('aboutus_id' => $aboutus_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_career_jobs
     */
    public function get_list_of_career_jobs() {
		$db_result = $this->db->get_where('career');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->career_id, $data)) {
                    $data[$row->career_id] = array();
                }
                if (array_key_exists($row->career_id, $data)) {
                    $data[$row->career_id] = array(
                        'career_id' => $row->career_id,
						'education' => $row->education,
						'job_title' => $row->job_title,
						'experience' => $row->experience,
						'salary' => $row->salary,
						'location' => $row->location,
						'note1' => $row->note1,
						'note2' => $row->note2,
						'note3' => $row->note3,
						'note4' => $row->note4,
						'note5' => $row->note5,
						'note6' => $row->note6,
						
                    );
                    array_push($data_value, $data[$row->career_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to update career
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_career() {
		date_default_timezone_set('Asia/Kolkata');
		$career_id = (int)$_POST['career_id'];
		$data = array(
            'education' => $_POST['education'],
            'job_title' => $_POST['job_title'],
            'experience' => $_POST['experience'],
            'salary' => $_POST['salary'],
            'location' => $_POST['location'],
            'note1' => $_POST['note1'],
            'note2' => $_POST['note2'],
            'note3' => $_POST['note3'],
            'note4' => $_POST['note4'],
            'note5' => $_POST['note5'],
            'note6' => $_POST['note6'],
			
        );
		if($career_id == 0){
			$result = $this->db->insert('career', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('career', $data, array('career_id' => $career_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	

		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_career_job() {
       
        $result = $this->db->delete(' career', array('career_id' => $_POST['career_id']));
       
            return TRUE;
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_team() {
		$db_result = $this->db->get_where('web_team');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->team_id, $data)) {
                    $data[$row->team_id] = array();
                }
                if (array_key_exists($row->team_id, $data)) {
                    $data[$row->team_id] = array(
                        'team_id' => $row->team_id,
						'team_name' => $row->team_name,
						'team_position' => $row->team_position,
						'team_description' => $row->team_description,
						'team_img' => $row->team_img,
						
                    );
                    array_push($data_value, $data[$row->team_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update testimonials
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_team() {
		date_default_timezone_set('Asia/Kolkata');
		$team_id = (int)$_POST['team_id'];
		$data = array(
            'team_name' => $_POST['team_name'],
            'team_position' => $_POST['team_position'],
			'team_description' => nl2br($_POST['team_description']),
			'team_img' => $_POST['team_img'],
           
        );
		if($team_id == 0){
			$result = $this->db->insert('web_team', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('web_team', $data, array('team_id' => $team_id));      
			
				return TRUE;
			
		}  	
	}
		//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_team_status() {
		
        $result = $this->db->update('web_team', $data, array('team_id' => $_POST['team_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to remove_testimonials from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_team() {
        $result = $this->db->delete('web_team', array('team_id' => $_POST['team_id']));
         return TRUE;  
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update testimonials
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_listing() {
		date_default_timezone_set('Asia/Kolkata');
		$listing_id = (int)$_POST['listing_id'];
		$data = array(
            //'user_id' => $_POST['user_id'],
            'user_name' => $_POST['user_name'],
            'listing_name' => $_POST['listing_name'],
            'listing_mblo' => $_POST['listing_mblo'],
            'listing_email' => $_POST['listing_email'],
            'listing_type' => $_POST['listing_type'],
            'listing_profile' => $_POST['listing_profile'],
            'listing_services' => $_POST['listing_services'],
			'listing_image' => ($_POST['listing_image']),
			'listing_detail' => nl2br($_POST['listing_detail']),
			'status' => $_POST['status'],
			
			
           
        );
		if($listing_id == 0){
			$result = $this->db->insert('listing', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('listing', $data, array('listing_id' => $listing_id));      
			
				return TRUE;
			
		}  	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_refund_policy
     */
    public function get_list_of_listing_view() {
		$db_result = $this->db->get_where('listing');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->listing_id, $data)) {
                    $data[$row->listing_id] = array();
                }
                if (array_key_exists($row->listing_id, $data)) {
                    $data[$row->listing_id] = array(
                        'listing_id' => $row->listing_id,
                        'user_id' => $row->user_id,
						'user_name' => $row->user_name,
						'listing_name' => $row->listing_name,
						'listing_mblo' => $row->listing_mblo,
						'listing_email' => $row->listing_email,
						'listing_type' => $row->listing_type,
						'listing_profile' => $row->listing_profile,
						'listing_services' => $row->listing_services,
						'listing_image' => $row->listing_image,
						'listing_detail' => $row->listing_detail,
						'status' => $row->status,
						//'status' => 'old',
						
                    );
                    array_push($data_value, $data[$row->listing_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to remove_testimonials from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_listing() {
        $result = $this->db->delete('listing', array('listing_id' => $_POST['listing_id']));
         return TRUE;  
	}
		//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_services
     */
    public function get_list_of_listing_by_id($listing_id) {
		$db_result = $this->db->get_where('listing', array('listing_id'=>$listing_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->listing_id, $data)) {
                    $data[$row->listing_id] = array();
                }
                if (array_key_exists($row->listing_id, $data)) {
                    $data[$row->listing_id] = array(
                        'listing_id' => $row->listing_id,
						'user_id' => $row->user_id,
						'user_name' => $row->user_name,
						'listing_name' => $row->listing_name,
						'listing_mblo' => $row->listing_mblo,
						'listing_email' => $row->listing_email,
						'listing_type' => $row->listing_type,
						'listing_profile' => $row->listing_profile,
						'listing_services' => $row->listing_services,
						'listing_image' => $row->listing_image,
						'listing_detail' => $row->listing_detail,
						'status' => $row->status,
						//'status' => 'old',
						
                    );
                    array_push($data_value, $data[$row->listing_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_approve_status() {
		$data = array(
            'status' => 'active',
        );
        $result = $this->db->update('listing', $data, array('listing_id' => $_POST['listing_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
		//-------------------------------------------------------------------------
    /*
     * This function is used to update_category_status from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_reject_status() {
		$data = array(
            'status' => 'old',
        );
        $result = $this->db->update('listing', $data, array('listing_id' => $_POST['listing_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_listing_detail_enquiry() {
		$db_result = $this->db->get_where('listing_detail_enquiry');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->listing_detail_id, $data)) {
                    $data[$row->listing_detail_id] = array();
                }
                if (array_key_exists($row->listing_detail_id, $data)) {
                    $data[$row->listing_detail_id] = array(
                        'listing_detail_id' => $row->listing_detail_id,
						'listing_detail_name' => $row->listing_detail_name,
						'listing_detail_contact' => $row->listing_detail_contact,
						'listing_detail_email' => $row->listing_detail_email,
						'listing_detail_message' => $row->listing_detail_message,
						
                    );
                    array_push($data_value, $data[$row->listing_detail_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_listing_detail_enquiry() {
       
        $result = $this->db->delete(' listing_detail_enquiry', array('listing_detail_id' => $_POST['listing_detail_id']));
       
            return TRUE;
      
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_offer_detail_enquiry() {
		$db_result = $this->db->get_where('offer_detail_enquiry');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->offer_id, $data)) {
                    $data[$row->offer_id] = array();
                }
                if (array_key_exists($row->offer_id, $data)) {
                    $data[$row->offer_id] = array(
                        'offer_id' => $row->offer_id,
						'offer_name' => $row->offer_name,
						'offer_contact' => $row->offer_contact,
						'offer_email' => $row->offer_email,
						'offer_message' => $row->offer_message,
						'offer_cat_name' => $row->offer_cat_name,
						
                    );
                    array_push($data_value, $data[$row->offer_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_offer_detail_enquiry from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_offer_detail_enquiry() {
		
         $result = $this->db->delete(' offer_detail_enquiry', array('offer_id' => $_POST['offer_id']));
			return TRUE;
	
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_register_user() {
		$db_result = $this->db->get_where('register');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array();
                }
                if (array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array(
                        'user_id' => $row->user_id,
						'full_name' => $row->full_name,
						'home_address' => $row->home_address,
						'mobile_no' => $row->mobile_no,
						'profile_type' => $row->profile_type,
						'user_email' => $row->user_email,
						
                    );
                    array_push($data_value, $data[$row->user_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_register_user from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_register_user() {
		
         $result = $this->db->delete(' register', array('user_id' => $_POST['user_id']));
			return TRUE;
	
	}
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list News in MPS
     */
    public function get_list_of_opportunities_enquiries() {
		$db_result = $this->db->get_where('admin_contact_enquiry');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->enquiries_id, $data)) {
                    $data[$row->enquiries_id] = array();
                }
                if (array_key_exists($row->enquiries_id, $data)) {
                    $data[$row->enquiries_id] = array(
                        'enquiries_id' => $row->enquiries_id,
						
						'name' => $row->name,
						'email' => $row->email,
						'number' => $row->number,
						'message' => $row->message,
						'date' => date('Y-m-d'),
			            'time' => date('H:m:s'),
						
                    );
                    array_push($data_value, $data[$row->enquiries_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_feedback from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_opportunities_enquiry() {
		
         $result = $this->db->delete(' admin_contact_enquiry', array('enquiries_id' => $_POST['enquiries_id']));
			return TRUE;
	
	}
	public function update_gallery() {
		date_default_timezone_set('Asia/Kolkata');
		$gallery_id = (int)$_POST['gallery_id'];
		$data = array(
            'gallery_des' => $_POST['gallery_des'],
			'gallery_img' => $_POST['gallery_img'],
            'created_date' => date('Y-m-d'),
			'created_time' => date('H:m:s'),
        );
		if($gallery_id == 0){
			$result = $this->db->insert('gallery_press', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('gallery_press', $data, array('gallery_id' => $gallery_id));      
			
			return TRUE;
			
		}  	
	}
	public function remove_gallery() {
		
        $result = $this->db->delete('gallery_press', array('gallery_id' => $_POST['gallery_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	}
	
	public function remove_public_gallery() {
		
        $result = $this->db->delete('public_gallery', array('gallery_id' => $_POST['gallery_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_gallerys
     */
    public function get_list_of_gallerys() {
		$db_result = $this->db->get_where('gallery_press');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->gallery_id, $data)) {
                    $data[$row->gallery_id] = array();
                }
                if (array_key_exists($row->gallery_id, $data)) {
                    $data[$row->gallery_id] = array(
                        'gallery_id' => $row->gallery_id,
						'gallery_des' => $row->gallery_des,
						'gallery_img' => $row->gallery_img,
                        'created_date'=> $row->created_date,
                    );
                    array_push($data_value, $data[$row->gallery_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_public_gallerys
     */
    public function get_list_of_public_gallerys() {
		$db_result = $this->db->get_where('public_gallery');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->gallery_id, $data)) {
                    $data[$row->gallery_id] = array();
                }
                if (array_key_exists($row->gallery_id, $data)) {
                    $data[$row->gallery_id] = array(
                        'gallery_id' => $row->gallery_id,
						'gallery_des' => $row->gallery_des,
						'gallery_img' => $row->gallery_img,
                        'created_date'=> $row->created_date,
                    );
                    array_push($data_value, $data[$row->gallery_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_benefits
     */
	public function update_public_gallery() {
		date_default_timezone_set('Asia/Kolkata');
		$gallery_id = (int)$_POST['gallery_id'];
		$data = array(
            'gallery_des' => $_POST['gallery_des'],
			'gallery_img' => $_POST['gallery_img'],
            'created_date' => date('Y-m-d'),
			'created_time' => date('H:m:s'),
        );
		if($gallery_id == 0){
			$result = $this->db->insert('public_gallery', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('public_gallery', $data, array('gallery_id' => $gallery_id));      
			
			return TRUE;
		}  	
	}
	
}