<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Page_validation_model extends CI_Model {
	
    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
		date_default_timezone_set('Asia/Kolkata');
    }
	
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_submenu 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function is_permitted() {
		$user_id = $this->session->userdata('userID');
		$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$current_path = explode(base_url(),$current_url)[1];
		//var_dump($current_path);
		$current_path = explode('?',$current_path)[0];
		//var_dump($current_path);
		$user_access = $this->get_user_access_detail_given_user($user_id);
		$status = 0;
		if($user_access){
			foreach($user_access as $value){
				if (strpos($current_path,$value['link']) !== false) {
					$status=1;
				}
				/*if($value['link']==$current_path){
					$status=1;
				}*/
			}
			if($status==1){
				$status = 0;
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get the user access of current user
     * 
     * @access		public
     * @since		1.0.0
     */
    public function get_user_access_detail_given_user($user_id) {
		
		$this->db->from('admin_emp_access');
		$this->db->where(array('emp_id'=>$user_id));
		$db_result = $this->db->get();
		if ($db_result && $db_result->num_rows() > 0) {
			$data = array();
			$data_value = array();
			foreach ($db_result->result() as $row) {
				$module_name = '';
				$submodule_name = '';
				/*fetching the module name*/
				$this->db->where(array('module_id' => $row->module_id));	
				$db_result1 = $this->db->get('admin_module');
				if($db_result1 && $db_result1->num_rows()==1){
					foreach($db_result1->result() as $row1){
						$module_name = $row1->module_name;
					}
				}
				/*fetching the sub module name*/
				$this->db->where(array('submodule_id' => $row->submodule_id));	
				$db_result2 = $this->db->get('admin_submodule');
				if($db_result2 && $db_result2->num_rows()==1){
					foreach($db_result2->result() as $row2){
						$submodule_name = $row2->submodule_name;
						$link = $row2->link;
					}
				}
				if (!array_key_exists($row->index_id, $data)) {
					$data[$row->index_id] = array();
				}
				if (array_key_exists($row->index_id, $data)) {
					$data[$row->index_id] = array(
						'index_id' => $row->index_id,
						'user_id' => $row->emp_id,
						'module_id' => $row->module_id,
						'module_name' => $module_name,
						'submodule_id' => $row->submodule_id,
						'submodule_name' => $submodule_name,
						'link' => $link,
						'date' => $row->date,
					);
					array_push($data_value, $data[$row->index_id]);
				}
			}
			return $data_value;
		}else {
			 return FALSE;
		}
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get the user access of current user
     * 
     * @access		public
     * @since		1.0.0
     */
    public function get_user_access_module_of_given_user($user_id) {
		
		$this->db->from('admin_emp_access');
		$this->db->where(array('emp_id'=>$user_id));
		$this->db->group_by('module_id');
		$db_result = $this->db->get();
		if ($db_result && $db_result->num_rows() > 0) {
			$data = array();
			$data_value = array();
			foreach ($db_result->result() as $row) {
				$module_name = '';
				$submodule_name = '';
				/*fetching the menu name*/
				$this->db->where(array('module_id' => $row->module_id));	
				$db_result1 = $this->db->get('admin_module');
				if($db_result1 && $db_result1->num_rows()==1){
					foreach($db_result1->result() as $row1){
						$module_name = $row1->module_name;
					}
				}
				/*fetching the sub menu name*/
				$this->db->where(array('submodule_id' => $row->submodule_id));	
				$db_result2 = $this->db->get('admin_submodule');
				if($db_result2 && $db_result2->num_rows()==1){
					foreach($db_result2->result() as $row2){
						$submodule_name = $row2->submodule_name;
						$link = $row2->link;
					}
				}
				if (!array_key_exists($row->index_id, $data)) {
					$data[$row->index_id] = array();
				}
				if (array_key_exists($row->index_id, $data)) {
					$data[$row->index_id] = array(
						'index_id' => $row->index_id,
						'user_id' => $row->emp_id,
						'module_id' => $row->module_id,
						'module_name' => $module_name,
						'submodule_id' => $row->submodule_id,
						'submodule_name' => $submodule_name,
						'link' => $link,
						'date' => $row->date,
					);
					array_push($data_value, $data[$row->index_id]);
				}
			}
			return $data_value;
		}else {
			 return FALSE;
		}
	}
	
	
}
