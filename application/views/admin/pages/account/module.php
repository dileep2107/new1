

<style>
.error,.required{
	color:red;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Module Table</h3>
			 <a  href="javascript:void(0);" data-toggle="modal" data-target="#browseNewModule" class="btn btn-default pull-right">Add New Module</a>
            </div>
            <!-- /.box-header -->
			  <div class="box-body">
			  <div id="headerMsg" ></div>
				<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
			<tr>
				<th class="text-center">S. No.</th>
				<th class="text-center">Module Name</th>
				<th class="text-center">Date</th>
				<th class="text-center">Action</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 1;
			if ($module_list == 0) {
				echo 'No record found into database';
			} else {
				
				$content = '';
				foreach ($module_list as $value) {
					$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
					$content .= '<td class="text-center">' . $value['module_name'] . '</td>';
					$content .= '<td class="text-center">' . date('d F Y',strtotime($value['date'])) . '</td>';
					$content .= '<td class="text-center"><a href="#" class="edit_module" data-toggle="modal" data-target="#browseNewModule" name=' . $value['module_id'] . ' value=""><span class="label label-success">Edit</span></a>';
					$content .= '&nbsp;&nbsp;<a href="#" class="remove_module"  name=' . $value['module_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
					$i++;
				}
				echo $content;
			}
			?>
			
			</tbody>
		
			</table>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!---------------------------- Modal for Browse Module-------------------------->
<div class="modal fade" id="browseNewModule" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Module</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well" id="menu_form" method="post" enctype="multipart/form-data">
						<input class="form-control" id="menu_id" name="menu_id" value=0 type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="menu_name">Module Name <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="menu_name" name="menu_name" placeholder="Module Name" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>



<script>
$('document').ready(function(){

	//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit ads
     */
    $('body').on('click', '.edit_module', function () {		
        $('#headerMsg').empty();
		$('#menu_form label.error').empty();
        $('#menu_form input.error').removeClass('error');
        $('#menu_form select.error').removeClass('error');
		
        var menu_id = $(this).attr('name');
        var menu_name = $(this).closest('tr').find('td:eq(1)').text();		
        
		$('#menu_id').val(menu_id);
        $('#menu_name').val(menu_name);
	});
	
	//------------------------------------------------------------------------
    /*
     * This script is used to empty the model  when click on add new city
     */
    $('body').on('click', '.addNewModule', function () {
		$('#headerMsg').empty();
		$('#menu_form label.error').empty();
        $('#menu_form input.error').removeClass('error');
        $('#menu_form select.error').removeClass('error');
		$("#menu_id").val(0);
		$("#menu_name").val('');
		
	});

	
	//-----------------------------------------------------------------------
    /* 
     * validation of add city
     */
	$('#menu_form').validate({
		ignore: [],
        rules: {
            menu_name: {
                required: true,
            },
        },
		 messages: {
			menu_name: {
                required: "Module Name is required.",
            },
		},
		submitHandler: function (form) {
			$.blockUI();
			$('#menu_form').find('button[type="submit"]').prop('disabled',true);
			var menu_id = $('#menu_id').val();
            var menu_name = $('#menu_name').val();
			
            $.post(APP_URL + 'admin/account/update_module', {
                menu_id: menu_id,
                menu_name: menu_name,
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status == 200) {
                    var message = response.message;
					if(menu_id!=0){
						message = "Module has been updated successfully!";
						$('.edit_module[name=' + menu_id + ']').closest("tr").find("td:eq(1)").text(menu_name);
					}
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'>Refresh!</a></div>");
                 
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
						window.location.href = APP_URL+'admin/account/module';
					});
	
	
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'> <button class='close' type='button' data-dismiss='alert'>x</button>  <strong>" + response.message + "</strong></div>");
                       
                      
                }
				$("#menu_id").val(0);
				$("#menu_name").val('');
				$('#browseNewModule').modal('hide');
				$.unblockUI();
				$('#menu_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	//---------------------------------------------------------------------
    /**
     * This script is used to remove ads from the list
     */
	$('body').on('click', '.remove_module', function () {
      /*  if (!confirm("Do you want to delete")) {
            return false;
        }*/
		$.blockUI();
        var menu_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'admin/account/remove_module', {menu_id: menu_id}, function (response) {
            $('#headerMsg').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");
               
                $('.remove_module[name=' + menu_id + ']').closest("tr").remove();
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					window.location.href = APP_URL+'admin/account/module';
				});
            }
            else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'> <button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");
                  $("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});     
                       
            }
        }, 'json');
		$.unblockUI();
        return false;
    });
});
 </script>