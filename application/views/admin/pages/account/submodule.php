

<style>
.error,.required{
	color:red;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Submodule Table</h3>
			
            <a  href="javascript:void(0);" data-toggle="modal" data-target="#browseNewSubModule" class="btn btn-default pull-right addNewSubModule">Add New Sub Module</a>
            </div>
			<div id="headerMsg"></div>
			<?php #print_r($emp_list);?>
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover dataTables-example" >
			<thead>
			<tr>
				<th class="text-center">S. No.</th>
				<th class="text-center">Module Name</th>
				<th class="text-center">Sub Module Name</th>
				<th class="text-center">Link</th>
				<th class="text-center">Date</th>
				<th class="text-center">Action</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 1;
			if ($submodule_list == 0) {
				echo 'No record found into database';
			} else {
				
				$content = '';
				foreach ($submodule_list as $value) {
					$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
					$module_name = '';
					foreach($module_list as $module){
						if($module['module_id']==$value['FK_module_id']){
							$module_name = $module['module_name'];
						}
					}
					$content .= '<td class="text-center" name="'.$value['FK_module_id'].'">' . $module_name . '</td>';
					$content .= '<td class="text-center">' . $value['submodule_name'] . '</td>';
					$content .= '<td class="text-center">' . $value['link'] . '</td>';
					$content .= '<td class="text-center">' . date('d F Y',strtotime($value['date'])) . '</td>';
					$content .= '<td class="text-center"><a href="#" class="edit_submodule" data-toggle="modal" data-target="#browseNewSubModule" name=' . $value['submodule_id'] . ' value=""><span class="label label-success">Edit</span></a>';
					$content .= '&nbsp;&nbsp;<a href="#" class="remove_submodule"  name=' . $value['submodule_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
					$i++;
				}
				echo $content;
			}
			?>
			
			</tbody>
			
			</table>
				</div>

			</div>
		</div>
	</div>
	</div>
	
</div>

<!---------------------------- Modal for Browse city-------------------------->
<div class="modal fade" id="browseNewSubModule" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Submodule</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well form-horizontal" id="submenu_form" method="post" enctype="multipart/form-data">
						<input class="form-control" id="submodule_id" name="submodule_id" value=0 type="hidden">
						<div class="form-group col-md-12">
							<label class="control-label col-md-3" for="module_name">Module Name <span class="required">*</span></label>
							<div class="col-md-9">
								<select class="form-control" id="module_name" name="module_name">
									<option value="">Select Module</option>
									<?php 
										if($module_list){
											$content = ''; 
											foreach($module_list as $menu){
												$content .= '<option name="'.$menu['module_id'].'" value="'.$menu['module_name'].'">'.$menu['module_name'].'</option>'; 
											}
											echo $content;
										}
									?>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12">
							<label class="control-label col-md-3" for="submodule_name">Sub Module Name <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="submodule_name" name="submodule_name" placeholder="Sub Module Name" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12">
							<label class="control-label col-md-3" for="link">Link <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="link" name="link" placeholder="Link" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
							<button type="reset" class="btn btn-primary" style="display:none;">Reset</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>



<script>

$('document').ready(function(){

	//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit ads
     */
    $('body').on('click', '.edit_submodule', function () {		
        $('#headerMsg').empty();
		$('#submenu_form label.error').empty();
        $('#submenu_form input.error').removeClass('error');
        $('#submenu_form select.error').removeClass('error');
		
        var submodule_id = $(this).attr('name');
        var module_id = $(this).closest('tr').find('td:eq(1)').attr('name');		
        var submodule_name = $(this).closest('tr').find('td:eq(2)').text();		
        var link = $(this).closest('tr').find('td:eq(3)').text();		
        
		$('#submodule_id').val(submodule_id);
        $('#module_name option[name="'+module_id+'"]').attr('selected','selected');
		$('#submodule_name').val(submodule_name);
		$('#link').val(link);
	});
	
	//------------------------------------------------------------------------
    /*
     * This script is used to empty the model  when click on add new city
     */
    $('body').on('click', '.addNewSubModule', function () {
		$('#headerMsg').empty();
		$('#submenu_form label.error').empty();
        $('#submenu_form input.error').removeClass('error');
        $('#submenu_form select.error').removeClass('error');
		
		$("#submodule_id").val(0);
		$('#module_name option').removeAttr('selected');
		$("#submodule_name").val('');
		$("#link").val('');
		
	});

	
	//-----------------------------------------------------------------------
    /* 
     * validation of add city
     */
	$('#submenu_form').validate({
		ignore: [],
        rules: {
            module_name: {
                required: true,
            },
			submodule_name: {
                required: true,
            },
			link: {
                required: true,
            },
        },
		 messages: {
			module_name: {
                required: "Module Name is required.",
            },
			submodule_name: {
                required: "Sub Module Name is required.",
            },
			link: {
                required: "Link is required.",
            },
		},
		submitHandler: function (form) {
			$.blockUI();
			$('#submenu_form').find('button[type="submit"]').prop('disabled',true);
			var submodule_id = $('#submodule_id').val();
			var module_id = $('#module_name option:selected').attr('name');
            var module_name = $('#module_name').val();
            var submodule_name = $('#submodule_name').val();
            var link = $('#link').val();
			
            $.post(APP_URL + 'admin/account/update_submodule', {
                submodule_id: submodule_id,
                module_id: module_id,
                submodule_name: submodule_name,
                link: link,
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status == 200) {
                    var message = response.message;
					if(submodule_id!=0){
						message = "Sub Menu has been updated successfully!";
						$('.edit_submodule[name=' + submodule_id + ']').closest("tr").find("td:eq(1)").text(module_name);
						$('.edit_submodule[name=' + submodule_id + ']').closest("tr").find("td:eq(1)").attr('name',module_id);
						$('.edit_submodule[name=' + submodule_id + ']').closest("tr").find("td:eq(2)").text(submodule_name);
						$('.edit_submodule[name=' + submodule_id + ']').closest("tr").find("td:eq(3)").text(link);
					}
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'>Refresh!</a></div>");
                    $("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
						window.location.href = APP_URL+'admin/account/submodule';
					});
				}
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'> <button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");
                    $("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				$("#submodule_id").val(0);
				$("#module_name option").removeAttr('selected');
				$("#submodule_name").val('');
				$("#link").val('');
				//$('#submenu_form').find('button[type="reset"]').trigger('click');
				$('#browseNewSubModule').modal('hide');
				$.unblockUI();
				$('#submenu_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	//---------------------------------------------------------------------
    /**
     * This script is used to remove ads from the list
     */
	$('body').on('click', '.remove_submodule', function () {
     /*   if (!confirm("Do you want to delete")) {
            return false;
        }*/
		$.blockUI();
        var submodule_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'admin/account/remove_submodule', {submodule_id: submodule_id}, function (response) {
            $('#headerMsg').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#headerMsg').html("<div class='alert alert-success fade in'> <button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");
        
                $('.remove_submodule[name=' + submodule_id + ']').closest("tr").remove();
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					window.location.href= ''+APP_URL+'admin/account/submodule';
				});
            }
            else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");
                $("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
            }
        }, 'json');
		$.unblockUI();
        return false;
    });
});


</script>
