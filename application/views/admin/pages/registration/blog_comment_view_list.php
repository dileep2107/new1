<?php
	
	
		$blog_id='';
		if(isset($_GET['blog_id'])){
			$blog_id = $_GET['blog_id'];
		}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Blog Comment View List</h3>
			  
			  <a style="float: right;" href="<?php echo base_url();?>admin/blog/blog_comment_view?blog_id=<?php echo $blog_id;?>" class="btn btn-default pull-right">Add New Comment</a>
			</div>
            <!-- /.box-header -->
			  <div class="box-body">
			  <div id="headerMsg" ></div>
				<table align="left" class="table table-hover">
					<thead>
						<tr>
							<th class="text-center">S. No.</th>
							<th class="text-center">Comment</th>
							<th class="text-center">Name</th>
							<th class="text-center">Image</th>
							<th class="text-center">Date</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
		
					<?php
			$i = 1;
			if ($comment_list == 0) {
				echo 'No record found into database';
			} else {
				
				$content = '';
				foreach ($comment_list as $value) {
					$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
					$content .= '<td class="text-center">' . $value['user_comment'] . '</td>';
					$content .= '<td class="text-center">' . $value['user_name'] . '</td>';
					$content .= '<td class="text-center"><img src="'.base_url().'uploads/'.$value['usercommimage'].'" class="" style="width:100px;"></td>';
					$content .= '<td class="text-center">' . $value['date'] . '</td>';
					$content .= '<td class="text-center"><a href="'.base_url().'admin/blog/blog_comment_view?blog_id='.$blog_id.'& comment_id='.$value['comment_id'].'" class="edit_blog" name='.' value=""><span class="label label-success">Edit</span></a>';
					$content .= '&nbsp;&nbsp;<a href="#" class="remove_blog"  name='.$value['comment_id'].' value=""><span class="label label-danger">Remove </span></a></td></tr>';
					$i++;
				}
				echo $content;
			}
			?>
					</tbody>
				</table>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
$('document').ready(function(){
	$('body').on('click', '.remove_blog', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
		//$.blockUI();
         var comment_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'admin/blog/remove_comment_view_list', {comment_id: comment_id}, function (response) {
            $('#headerMsg').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");

                $('.remove_blog[name=' + comment_id + ']').closest("tr").remove();
			//window.location.href= ''+APP_URL+'admin/blog/blog_comment_view_list';
				  window.location.reload();
            }
            else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
            }
        }, 'json');
		//$.unblockUI();
        return false;
    });

});
</script>
