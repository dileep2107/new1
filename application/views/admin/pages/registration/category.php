<style>

.error,.required{
	color:red;
}
</style>

<style>
#headerMsg{
	margin:20px 0px;
}
.dataTables-example th{
	text-align:center;
}
.display_none{
	display:none;
}
</style>

<div class="content-wrapper animated fadeInRight">
<div class="content"> 
	<div class="row">
		<div class="col-lg-12">
		  <div class="box ">
		<div class="ibox float-e-margins">
		<div class="box-header with-border">
              <h3 class="box-title">Blog Category</h3>
			  <button class="btn btn-default pull-right" data-toggle="modal" data-target="#browseNewCategory">Add New Category</button>

            
           </div>
			
			<div class="ibox-content">
			
			<div id="headerMsg"></div>
			
			<div class="table-responsive">
			<table id="blog_cat_table" class="table table-striped table-bordered table-hover " >
			<thead>
			<tr>
				<th class="text-center">S. No.</th>
				<th class="text-center">Category Name</th>
				<th class="text-center">Category Status</th>
				<th class="text-center">Date</th>
				<th class="text-center">Action</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 1;
			if ($category_list == 0) {
				echo 'No record found into database';
			} else {
				
				$content = '';
				foreach ($category_list as $value) {
					$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
					$content .= '<td class="text-center">' . $value['category_name'] . '</td>';
					$content .= '<td class="text-center text-capitalize" name="'.$value['status'].'" >' . $value['status'] . '</td>';
					$content .= '<td class="text-center">' . date('d F Y',strtotime($value['date'])) . '</td>';
					$content .= '<td class="text-center"><a href="#" class="edit_category" data-toggle="modal" data-target="#browseNewCategory" name=' . $value['category_id'] . ' value=""><span class="label label-success">Edit</span></a>';
					$content .= '&nbsp;&nbsp;<a href="#" class="change_status"  name=' . $value['category_id'] . ' data-toggle="modal" data-target="#browseChangeStatus"><span class="label label-danger">Change Status</span></a></td></tr>';
					$i++;
				}
				echo $content;
			}
			?>
			
			</tbody>
			
			</table>
				</div>

			</div>
		</div>
	</div>
	</div>
	</div>
	
</div>
</div>

<!---------------------------- Modal for Browse Category-------------------------->
<div class="modal fade" id="browseNewCategory" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3> Category</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well form-inline" id="category_form" method="post" enctype="multipart/form-data">
					<div id="headMsg"></div>
						<input class="form-control" id="category_id" name="category_id" value=0 type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="category_name"> Category Name <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="category_name" name="category_name" placeholder="Category Name" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Change Status-------------------------->
<div class="modal fade" id="browseChangeStatus" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Change  Category Status</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well" id="category_form2" method="post" enctype="multipart/form-data">
					<div id="headMsg"></div>
						<input class="form-control" id="category_id2" name="category_id2" value=0 type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="category_status">I Category Status<span class="required">*</span></label>
							<div class="col-md-9">
								<select class="form-control" id="category_status" name="category_status">
									<option value="">Select Status</option>
									<option value="active">Active</option>
									<option value="inactive">Inactive</option>
									<option value="remove">Remove</option>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>



<!-- Page-Level Scripts -->
<script>
 $(document).ready(function(){
    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit category
     */
    $('body').on('click', '.edit_category', function () {		
        $('#headerMsg').empty();
		$('#category_form label.error').empty();
        $('#category_form input.error').removeClass('error');
        $('#category_form select.error').removeClass('error');
		
        var category_id = $(this).attr('name');
        var category_name = $(this).closest('tr').find('td:eq(1)').text();		
        
		$('#category_id').val(category_id);
        $('#category_name').val(category_name);
	});
	
	//------------------------------------------------------------------------
    /*
     * This script is used to empty the model  when click on add new city
     */
    $('body').on('click', '.addNewCategory', function () {
		$('#headerMsg').empty();
		$('#category_form label.error').empty();
        $('#category_form input.error').removeClass('error');
        $('#category_form select.error').removeClass('error');
		$("#category_id").val(0);
		$("#category_name").val('');
		
	});

	
	//-----------------------------------------------------------------------
    /* 
     * validation of category_form
     */
	$('#category_form').validate({
		ignore: [],
        rules: {
            category_name: {
                required: true,
            },
        },
		 messages: {
			category_name: {
                required: "Category Name is required.",
            },
		},
		submitHandler: function (form) {
			
			
			var category_id = $('#category_id').val();
            var category_name = $('#category_name').val();
			
            $.post(APP_URL + 'admin/blog/update_category', {
                category_id: category_id,
                category_name: category_name,
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status == 200) {
                    var message = response.message;
					if(category_id!=0){
						message = "Category has been updated successfully!";
						$('.edit_category[name=' + category_id + ']').closest("tr").find("td:eq(1)").text(category_name);
					}
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
						window.location.href = APP_URL+'admin/blog/category';
					});
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				$("#category_id").val(0);
				$("#category_name").val('');
				$('#browseNewCategory').modal('hide');
				
				
			}, 'json');
		return false;
		},
	});
	
	
	//-----------------------------------------------------------------------
    /* 
     * validation of category_form2
     */
	$('#category_form2').validate({
		ignore: [],
        rules: {
            category_status: {
                required: true,
            },
        },
		 messages: {
			category_status: {
                required: "Category Status is required.",
            },
		},
		submitHandler: function (form) {
			var category_id = $('#category_id2').val();
            var category_status = $('#category_status').val();
			
				change_status_execute();
			
		},
	});
	
	function change_status_execute(){
		var category_id = $('#category_id2').val();
		var category_status = $('#category_status').val();
		
		
		$.post(APP_URL + 'admin/blog/update_category_status', {
			category_id: category_id,
			category_status: category_status,
		},
		function (response) {
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {
				var message = response.message;
				$('.change_status[name=' + category_id + ']').closest("tr").find("td:eq(2)").text(category_status);
				$('.change_status[name=' + category_id + ']').closest("tr").find("td:eq(2)").attr('name',category_status);
				
				if(category_status=='remove'){
					$('.change_status[name=' + category_id + ']').closest("tr").remove();
					message = 'Category has been removed successfully';
				}
				$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					window.location.href = APP_URL+'admin/blog/category';
				});
			}
			else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
			$("#category_id2").val(0);
			$("#category_status").val('');
			$('#browseChangeStatus').modal('hide');
		
			
			
		}, 'json');
		return false;
	}
	
	
	//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit category
     */
    $('body').on('click', '.change_status', function () {		
        $('#headerMsg').empty();
		$('#category_form2 label.error').empty();
        $('#category_form2 input.error').removeClass('error');
        $('#category_form2 select.error').removeClass('error');
		
        var category_id = $(this).attr('name');
        var category_status = $(this).closest('tr').find('td:eq(2)').attr('name');		
        
		$('#category_id2').val(category_id);
        $('#category_status').val(category_status);
	});

});

 </script>
