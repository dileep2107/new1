

<style>
.error,.required{
	color:red;
}
</style>

			
<style>
#headerMsg{
	margin:20px 0px;
}
.dataTables-example th{
	text-align:center;
}
.display_none{
	display:none;
}
</style>

<div class="content-wrapper animated fadeInRight">
<div class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="box ">
		<div class="ibox float-e-margins">
			<div class="box-header with-border">
				  <h3 class="box-title">Candidate List</h3>
				  <a style="float: right;" href="<?php echo base_url();?>admin/registration"  class="btn btn-default pull-right">Add candidate</a>
			</div>
		
			<div class="ibox-content">
		    <div id="headerMsg"></div>
			<div class="row">
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_army_no" value="<?php echo $ano; ?>" placeholder="Search by Army no" > </div>
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_rank" value="<?php echo $rank; ?>" placeholder="Search by Rank"> </div>
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_name" value="<?php echo $name; ?>" placeholder="Search by Name"> </div>
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_course" value="<?php echo $course; ?>" placeholder="Search by Course"> </div>
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_section" value="<?php echo $section; ?>" placeholder="Search by Section"> </div>
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_platoon" value="<?php echo $platoon; ?>" placeholder="Search by Platoon"> </div>
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_campany" value="<?php echo $campany; ?>" placeholder="Search by Company"> </div>
			 <div class="col-md-2 form-group"><input type="text" class="form-control" id="s_arq" value="<?php echo $arq; ?>" placeholder="Search by ARQ/UHQ"> </div>
			 <div class="col-md-2 form-group"><button class="btn btn-primary search_filter"> Search </button></div>
			</div>
			<div class="table-responsive">
			<table id="blog_view_table" class="table table-striped table-bordered table-hover dataTables-example" >
			<thead>
			<tr>
				<th class="text-center">S. No.</th>
				<th class="text-center">Name</th>
				<th class="text-center">DOB</th>
				<th class="text-center">Company</th>
				<th class="text-center">course</th>
				<th class="text-center">section</th>
				<th class="text-center">Rank</th>
				<th class="text-center">Army no</th>
				<th class="text-center">Action</th>
			</tr>
			</thead>
			<tbody style="text-align: center;">
			   <?php 
                 if($candidate_list){
					 $i = 1;
					 foreach($candidate_list as $val){ ?>
						 <tr class="table-active">
						  <td scope="row"><?php echo $i++; ?></td>
						  <td><?php echo $val->candidate_name; ?></td>
						  <td><?php echo $val->candidate_dob; ?></td>
						  <td><?php echo $val->candidate_company; ?></td>
						  <td><?php echo $val->candidate_course; ?></td>
						  <td><?php echo $val->candidate_section; ?></td>
						  <td><?php echo $val->candidate_rank; ?></td>
						  <td><?php echo $val->candidate_army_no; ?></td>
						  <td>
						  <a href="<?php echo base_url(); ?>admin/registration/edit_candidate?id=<?php echo $val->id; ?>"> <span style="cursor: pointer;" class="label label-primary">Edit</span></a>
						 <a href="<?php echo base_url(); ?>admin/registration/print_view?id=<?php echo $val->id; ?>"> <span style="cursor: pointer;" class="label label-success">Print</span></a>
						  <span style="cursor: pointer;" class="label label-danger delete_user" candi="<?php echo $val->id; ?>">Delete</span>
						  </td>
						</tr>
				<?php	 }
					 
				 }else{ ?>
					 <p> No record found </p>
				<?php }
				?>
			</tbody>
			   
				
			</table>
				</div>

			</div>
		</div>
	</div>
	</div>
	</div>
	
</div>
</div>

<script>
$('document').ready(function(){

	 $('body').on('click', '.search_filter', function () {	
	   var search = '';
	  var s_army_no = $('#s_army_no').val();
	  var s_rank = $('#s_rank').val();
	  var s_name = $('#s_name').val();
	  var s_course = $('#s_course').val();
	  var s_section = $('#s_section').val();
	  var s_platoon = $('#s_platoon').val();
	  var s_campany = $('#s_campany').val();
	  var s_arq = $('#s_arq').val();
	  
	   if(s_army_no){
		    search += 'ano='+s_army_no;
	   }
	   if(s_rank){
		   search += '&rank='+s_rank;  
		}
	   if(s_name){
		  search += '&name='+s_name;   
	   }
	    if(s_course){
		  search += '&course='+s_course;   
	   }
	    if(s_section){
		  search += '&section='+s_section;   
	   }
	    if(s_platoon){
		  search += '&platoon='+s_platoon;   
	   }
	    if(s_arq){
		  search += '&arq='+s_arq;   
	   }
	    if(s_campany){
		  search += '&campany='+s_campany;   
	   }
	  window.location.href = APP_URL +'admin/registration/candidate_list?'+search;
	 });
	 
	 
    $('body').on('click', '.delete_user', function () {		
       var candidate_id =  $(this).attr('candi');
	   console.log(candidate_id);
	   $.post(APP_URL + 'admin/registration/delete_candidate', {
			candidate_id: candidate_id,
		},
		function (response) {
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {
				var message = response.message;
				
				$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					window.location.reload();
				});
			} else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
			
		}, 'json');
		return false;
	});

	
	//-----------------------------------------------------------------------
    /* 
     * validation of category_form2
     */
	$('#category_form2').validate({
		ignore: [],
        rules: {
            category_status: {
                required: true,
            },
        },
		 messages: {
			category_status: {
                required: "Blog Status is required.",
            },
		},
		submitHandler: function (form) {
			var category_id = $('#category_id2').val();
            var category_status = $('#category_status').val();
			
				change_status_execute();
			
		},
	});
	
	function change_status_execute(){
		var category_id = $('#category_id2').val();
		var category_status = $('#category_status').val();
		$.blockUI();
		$('#category_form2').find('button[type="submit"]').prop('disabled',true);
		$.post(APP_URL + 'admin/blog/update_blog_status', {
			blog_id: category_id,
			blog_status: category_status,
		},
		function (response) {
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {
				var message = response.message;
				$('.change_status[name=' + category_id + ']').closest("tr").find("td:eq(4)").text(category_status);
				$('.change_status[name=' + category_id + ']').closest("tr").find("td:eq(4)").attr('name',category_status);
				
				if(category_status=='remove'){
					$('.change_status[name=' + category_id + ']').closest("tr").remove();
					message = 'Category has been removed successfully';
				}
				$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					window.location.href = APP_URL+'admin/blog/blog_view';
				});
			} else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
			$("#category_id2").val(0);
			$("#category_status").val('');
			$('#browseChangeStatus').modal('hide');
			$.unblockUI();
			$('#category_form2').find('button[type="submit"]').prop('disabled',false);
			
		}, 'json');
		return false;
	}
	
	
	
});
</script>

