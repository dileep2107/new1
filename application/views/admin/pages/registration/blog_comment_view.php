


<style>
.error,.required{
	color:red;
}
</style>
<?php
	if($comment_details){
		$comment_id = $comment_details[0]['comment_id'];
		$user_id = $comment_details[0]['user_id'];
		$blog_id = $comment_details[0]['blog_id'];
		$user_name = $comment_details[0]['user_name'];
		$usercommimage = $comment_details[0]['usercommimage'];
		$user_comment = $comment_details[0]['user_comment'];
	
	}else{
		$comment_id = 0;
		$user_id = 0;
		$user_name = '';
		$usercommimage = '';
		$user_comment = '';
		
	}
	
		$blog_id='';
		if(isset($_GET['blog_id'])){
			$blog_id = $_GET['blog_id'];
		}
?>
			
<style>
#headerMsg{
	margin:20px 0px;
}
.dataTables-example th{
	text-align:center;
}
.display_none{
	display:none;
}
</style>
<!-- add comm Scripts -->
<div class="content-wrapper animated fadeInRight">
<div class="content"> 
	<div class="row">
	   <div class="col-lg-12">
	     <div class="box ">
			<div class="ibox float-e-margins">
				<div class="box-header with-border">
					  <h3 class="box-title">Add Blog Comment</h3>
					  <a style="float: right;" href="<?php echo base_url();?>admin/blog/blog_comment_view_list?blog_id=<?php echo $blog_id;?>"  class="btn btn-default pull-right">Blog Comment List</a>
				</div>
				
				<div class="ibox-content">
					<form class="form-horizontal" id="blog_form">
						<div id="headerMsg"></div>
						<input hidden type="text" name="comment_id" id="comment_id" value="<?php echo $comment_id;?>">
						<input hidden type="text" name="blog_id" id="blog_id" value="<?php echo $blog_id;?>">
						
						
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="user_comment">User Comment <span class="required">*</span></label>
							<div class="col-lg-6">
								<textarea rows="3"  type="text"  id="user_comment" name="user_comment" placeholder="User Comment" class="form-control user_comment" ><?php echo $user_comment;?></textarea> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="user_name">User Name <span class="required">*</span></label>
							<div class="col-lg-6">
								<input type="text"  id="user_name" name="user_name" placeholder="User Name" class="form-control" value="<?php echo $user_name;?>"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="slider_image">User Image <span class="required">*</span></label>
							<div class="col-lg-8">
								<a id="slider_image_btn" name="slider_image_btn" class="btn btn-success slider_image_btn" data-toggle="modal" data-target="#browseImage" style="width: 167px;">Upload Image </a>
                                <input class="form-control slider_image" id="slider_image" name="slider_image" type="hidden" value="<?php echo $usercommimage;?>">
								<div class="image-preview inline_block">
								<?php 
									if($usercommimage!=''){
										echo '<img src="'.base_url().'uploads/'.$usercommimage.'" class="" style="width:200px;">';
									}else{
										echo '<img src="" class="display_none" style="width:200px;">';
									}
								?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-offset-3 col-lg-6">
								<button class="btn btn-sm btn-primary" type="submit">Submit</button>
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		</div>
		
	</div>
	</div>
</div>
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
			<div class="modal-body">
				<div id="head1_msg"></div>
				<input type="hidden" value="blog_comment" name="image_cat" class="image_cat">
				<input type="hidden" value="" name="sub_folder_name" class="sub_folder_name">
                <div class="row demo-columns">
					<div class="col-md-8 col-md-offset-2">
						<p>Image type should be GIF,JPG,PNG</p>
						<p>Image should be 2 MB or smaller</p>
						<!-- D&D Zone-->
						<div id="drag-and-drop-zone" class="uploader">
							<div>Drag &amp; Drop Images Here</div>
							<div class="or">-or-</div>
							<div class="browser">
								<label>
									<span>Click to open the file Browser</span>
									<input type="file" name="files[]" multiple="multiple" title='Click to add Files'>
								</label>
							</div>
						</div>
						<!-- /D&D Zone -->
						<!-- Debug box -->
						<div class="panel panel-default display_none">
							<div class="panel-heading">
								<h3 class="panel-title">Debug</h3>
							</div>
							<div class="panel-body demo-panel-debug">
								<ul id="demo-debug">
								</ul>
							</div>
						</div>
						<!-- /Debug box -->
					</div>
					<div class="clearfix"></div>
					<!-- / Left column -->
					<div class="col-md-8 col-md-offset-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Uploads</h3>
							</div>
							<div class="panel-body demo-panel-files minHeight" id='demo-files'>
								<span class="demo-note">No Files have been selected/droped yet...</span>
							</div>
						</div>
					</div>
					<!-- / Right column -->
				</div>
			</div><!-- / modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
	   
        </div>
    </div>
</div>

<!-- add comm Scripts -->

<!-- Page-Level Scripts -->
<script>
        $('document').ready(function(){
			

	//-----------------------------------------------------------------------
    /* 
     * validation of blog_form
     */
	$('#blog_form').validate({
        ignore: [],
		rules: {
           
			comasdsament_id: {
                required: true,
            },
			user_comment: {
                required: true,
            },
			user_name: {
                required: true,
            },
			
			slider_image: {
                required: true,
            },
		},
		 messages: {
			
			comasdsament_id: {
                required: "Comment is required.",
            },
			user_comment: {
                required: "Comment is required.",
            },
			user_name: {
                required: "Name is required.",
            },
			slider_image: {
                required: "Image is required.",
            },
		},
		submitHandler: function (form) {
			
			
			var comment_id = $('#comment_id').val();
			var blog_id = $('#blog_id').val();
         
            var user_id = 0;
            
			var user_comment = $('#user_comment').val();
			var user_name = $('#user_name').val();
			var slider_image = $('#slider_image').val();
			
            $.post(APP_URL + 'admin/blog/update_Blog_comment', {
                comment_id: comment_id,
                blog_id: blog_id,
                user_id: user_id,
                user_comment: user_comment,
                user_name: user_name,
                slider_image: slider_image,
            },
			function (response) {  console.log(response);
				$("html, body").animate({scrollTop: 0}, "slow");
				
                $('#headerMsg').empty();
				console.log(response.message);
				
				if (response.status ==200) {
					$.unblockUI();
                    var message = response.message;
					if(comment_id!=0){
						message = "Comment has been updated successfully!";						
					}
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
						window.location.href= ''+APP_URL+'admin/blog/blog_comment_view_list?blog_id='+blog_id;
					});
				
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
					
                }
				
				
				
			}, 'json');
		return false;
		},
	});
	
	//---------------------------------------------------------------------
    
					
	/*uploading drag and drop image*/
	$('#drag-and-drop-zone').dmUploader({
        url: APP_URL+'admin/blog/upload_image',
        dataType: 'json',
		extraData: {
		  image_cat:$('.image_cat').val(),
		  sub_folder_name:$('.sub_folder_name').val(),
		},
		maxFiles :'1',
		fileName : 'myFile',
		//maxFileSize :'1',
        allowedTypes: 'image/*',
        /*extFilter: 'jpg;png;gif',*/
        onInit: function(){
          $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
        },
        onBeforeUpload: function(id){
          
		  $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

          $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function(id, file){
			$('.demo-panel-files').empty();
			$.danidemo.addFile('#demo-files', id, file);
        },
        onComplete: function(){
          
		  $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
        },
        onUploadProgress: function(id, percent){
          var percentStr = percent + '%';

          $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
			var status= data.status;
			var filename_ = data.filename;
			if(status == 200){
				$('.image-preview').find('img').removeClass('display_none');
				$('.image-preview').find('img').attr('src',APP_URL+'uploads/'+filename_);
				$('#slider_image').val(filename_);
				$('#slider_image-error').css("display","none");
				$('#browseImage').modal('hide');
			}else{
                $('#browseImage').modal('hide');
              
			}
		  $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

          $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

          $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

          $.danidemo.updateFileProgress(id, '100%');
        },
        onUploadError: function(id, message){
			alert(message);
          $.danidemo.updateFileStatus(id, 'error', message);

          $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
        },
        onFileTypeError: function(file){
			alert('File \'' + file.name + '\' cannot be added: must be an image');
			$.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
        },
        onFileSizeError: function(file){
			alert('File \'' + file.name + '\' cannot be added: size excess limit');
			$.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
        },
        /*onFileExtError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
        },*/
        onFallbackMode: function(message){
			alert('info', 'Browser not supported(do something else here!): ' + message);
          $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
        },
		onFilesMaxError: function(file){
			alert(' cannot be added to queue due to upload limits.');
		  $.danidemo.addLog(file.name + ' cannot be added to queue due to upload limits.');
		}
      });
		
			
			
 });	

    </script>
	      