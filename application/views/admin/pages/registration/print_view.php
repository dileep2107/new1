
<style>
    .error,
    .required {
        color: red;
    }

    .form-control {
        padding: 0px;
    }

    .row {
        margin: 0px;
    }

    .rel_data {
        margin-bottom: 11px;
        margin-left: -22px;
    }

    .removepara {
        cursor: pointer;
    }

    .content-wrapper {
        margin-left: 0px;
    }
	.main-footer{
		margin-left:0px;
	}
</style>

<div class="content-wrapper animated fadeInRight">
    <div class="content">
	
        <div id="headerMsg"> </div>
        <div class="row" style="background: white;">
		<div style="width:100%;    text-align: center;margin-bottom: 35px;"> 
		<span onclick="print()" style="padding: 11px;background: chartreuse;cursor: pointer;">Print preview </span>
		
		</div>
            <div class="row">
                <input type="hidden" name="candidate_id" id="candidate_id" value="<?php echo $candidate_id; ?>">
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Army No </label></div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_army_no; ?></div>
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Course </label> </div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_course; ?></div>
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Company </label> </div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_company; ?> </div>
            </div>
            </br>
            <div class="row">
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Name </label></div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_name; ?></div>
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Section </label> </div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_section; ?></div>
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Year </label> </div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_year; ?></div>
            </div>
            </br>
            <div class="row">
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Rank </label></div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_rank; ?></div>
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> Platoon </label> </div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_platoon; ?></div>
                <div class="col-md-1" style="width: 8.33333333%;float: left;"> <label> ARO/UHQ </label> </div>
                <div class="col-md-3" style="width: 25%;float: left;"><?php echo $candidate_info[0]->candidate_aro_uhq; ?></div>
            </div>
            </br>
         
		 <div style="width:100%">

                <div style="width:50%;float: left;">
                    <h4 style="padding-left: 20px;">Bio Data </h4>
                    <div style="width:50%;float: left;">
                        <table border="1" style="width:100%;height: 161px;">
                            <tr class="table-active">
                                <th><label>DOB:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_dob; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>DOE:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_doe; ?> </td>
                            </tr>
                            <tr class="table-active">
                                <th><label>POE:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_poe; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>ADDRESS:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_address; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>BLOOD GROUP:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_blood_group; ?></td>
                            </tr>
                        </table>
                    </div>
                    <div style="width:50%;float: right;">
                        <table border="1" style="width:100%;height: 161px;">
                            <tr class="table-active">
                                <th><label>Religion:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_religion; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>Caste:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_caste; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>Village:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_village; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>Post office:<label></th>
                                <td><?php echo $candidate_info[0]->candidate_post_office; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>District:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_district; ?></td>
                            </tr>
                            <tr class="table-active">
                                <th><label>State:</label></th>
                                <td><?php echo $candidate_info[0]->candidate_state; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="width:50%;float: right;min-height: 196px;">
                    <h4> Instructer</h4>
                    <table border="1" style="width:100%;height: 161px;">
                        <tr>
                            <th>APPT</th>
                            <th>Rank</th>
                            <th>Name</th>
                            <th>From Date</th>
                        </tr>
                        <tr>
                            <td><label>Company commander</label></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_rank ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_name ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_form_date ; ?></td>
                        </tr>
                        <tr>
                            <td><label>Platoon commander</label></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_rank ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_name ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_form_date ; ?></td>
                        </tr>
                        <tr>
                            <td><label>Platoon havaldar 1</label></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_rank ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_name ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_form_date ; ?></td>
                        </tr>
                        <tr>
                            <td><label>Platoon havaldar 2</label></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_rank ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_name ; ?></td>
                            <td><?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_form_date ; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
			
			 <div style="width:100%;">
                <div style="width:50%;height: 333px;float: left;">
                    <h4> Faimly details </h4>
                    <table border="1" style="width:100%">
                        <tr>
                            <th>Relation</th>
                            <th>Name</th>
                            <th>DOB</th>
                            <th>Age</th>
                        </tr>
                        <?php 
					 if($candidate_family_info){
						 foreach($candidate_family_info as $val){ ?>

                        <tr>
                            <td><label><?php echo $val->candidate_relation; ?></label></td>
                            <td><?php echo $val->candidate_relation_name; ?></td>
                            <td><?php echo $val->candidate_relation_dob; ?></td>
                            <td><?php echo $val->candidate_relation_age; ?></td>
                        </tr>

                        <?php	 }
					 }
					
					?>


                    </table>
                </div>
                <div style="width:50%;height: 333px;float: right;">
                    <h4> Education qualification</h4>
                    <table border="1" style="width:100%">
                        <tr>
                            <th>APPT</th>
                            <th>Rank</th>
                            <th>Name</th>
                            <th>From Date</th>
                        </tr>
                        <tr>
                            <td><label>Company commander</label></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_rank ; ?>" id="company_commander_rank" name="company_commander_rank" placeholder="Rank"></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_name ; ?>" id="company_commander_name" name="company_commander_name" placeholder="Name"></td>
                            <td><input class="form-control date" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_form_date ; ?>" id="company_commander_form_date" name="company_commander_form_date" placeholder="From Date"></td>
                        </tr>
                        <tr>
                            <td><label>Platoon commander</label></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_rank ; ?>" id="platoon_commander_rank" name="platoon_commander_rank" placeholder="Rank"></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_name ; ?>" id="platoon_commander_name" name="platoon_commander_name" placeholder="Name"></td>
                            <td><input class="form-control date" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_form_date ; ?>" id="platoon_commander_form_date" name="platoon_commander_form_date" placeholder="From Date"></td>
                        </tr>
                        <tr>
                            <td><label>Platoon havaldar 1</label></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_rank ; ?>" id="platoon_havaldar_first_rank" name="platoon_havaldar_first_rank" placeholder="Rank"></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_name ; ?>" id="platoon_havaldar_first_name" name="platoon_havaldar_first_name" placeholder="Name"></td>
                            <td><input class="form-control date" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_form_date ; ?>" id="platoon_havaldar_first_form_date" name="platoon_havaldar_first_form_date" placeholder="From Date"></td>
                        </tr>
                        <tr>
                            <td><label>Platoon havaldar 2</label></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_rank ; ?>" id="platoon_havaldar_second_rank" name="platoon_havaldar_second_rank" placeholder="Rank"></td>
                            <td><input class="form-control" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_name ; ?>" id="platoon_havaldar_second_name" name="platoon_havaldar_second_name" placeholder="Name"></td>
                            <td><input class="form-control date" type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_form_date ; ?>" id="platoon_havaldar_second_form_date" name="platoon_havaldar_second_form_date" placeholder="From Date"></td>
                        </tr>
                    </table>
                </div>
            </div>
			
			<div style="width:100%;display: block;">
                <h4 class="box-title" style="margin-left: 11px;"> Physical Parameters </h4>
                <div style="width:50%;float: left;">

                    <table style="width:100%" border="1">
                        <tr>
                            <th><label>Height:</label></th>
                            <td><?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_height; ?></td>
                        </tr>
                        <tr>
                            <th><label>Permissible Wt:</label></th>
                            <td><?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_permissible_wt; ?></td>
                        </tr>
                        <tr>
                            <th><label>Actual Wt:</label></th>
                            <td><?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_actual_wt; ?></td>
                        </tr>
                        <tr>
                            <th><label>Over Wt / Under Wt:</label></th>
                            <td><?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_over_under_wt; ?></td>
                        </tr>
                    </table>
                </div>
                <div style="width:50%;float: right;">
                    <table style="width:100%;height: 100%;" border="1">
                        <tr style="height: 51px;">
                            <th><label>Identification Mark - 1:</label></th>
                            <td><?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_identification_mark_one; ?></td>
                        </tr>
                        <tr style="height: 51px;">
                            <th><label>Identification Mark - 2:</label></th>
                            <td><?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_identification_mark_two; ?></td>
                        </tr>

                    </table>
                </div>
            </div>
			
			<div style="width:100%; display:inline-block">
			<h4 style="margin-left: 11px;"> TRG BN CDR PPT TEST </h4>
                <div style="width:40%; float: left;">
                    <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Event</label></th>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($candidate_cdr_ppt_test){
									  $i = 1;
									$trg_arr = json_decode($candidate_cdr_ppt_test[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->trg_bn_ppt_mark; ?></td>
                            <td><?php echo $val->trg_bn_ppt_grade; ?></td>
                            <td><?php echo $val->trg_bn_ppt_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 2nd Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
									  
									  if($candidate_cdr_ppt_test){
										  $i = 1;
										$trg_arr = json_decode($candidate_cdr_ppt_test[0]->second_attempt_test);
										 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->trg_bn_ppt_mark; ?></td>
                            <td><?php echo $val->trg_bn_ppt_grade; ?></td>
                            <td><?php echo $val->trg_bn_ppt_sign; ?></td>
                        </tr>
                        <?php	 
									  }
									   }
									  ?>

                    </table>
                </div>
                <div style="width:30%; float: right;">
                    <h5 style="margin-left: 11px;"> 3rd Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
									  
									  if($candidate_cdr_ppt_test){
										  $i = 1;
										$trg_arr = json_decode($candidate_cdr_ppt_test[0]->third_attempt_test);
										 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->trg_bn_ppt_mark; ?></td>
                            <td><?php echo $val->trg_bn_ppt_grade; ?></td>
                            <td><?php echo $val->trg_bn_ppt_sign; ?></td>
                        </tr>
                        <?php	 
									  }
									   }
									  ?>

                    </table>
                </div>
            </div>
			
			
			<div style="width:100%;display:inline-block">
			<h4 class="box-title"> TRG BN CDR TEST </h4>
                <div style="width:40%; float: left;">
                    <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Event</label></th>
                            <th><label>Date</label></th>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($trg_bn_cdr_test){
									  $i = 1;
									$trg_arr = json_decode($trg_bn_cdr_test[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->trg_bn_cdr_date; ?></td>
                            <td><?php echo $val->trg_bn_cdr_marks; ?></td>
                            <td><?php echo $val->trg_bn_cdr_grade; ?></td>
                            <td><?php echo $val->trg_bn_cdr_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 2nd Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Date</label></th>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($trg_bn_cdr_test){
									  $i = 1;
									$trg_arr = json_decode($trg_bn_cdr_test[0]->second_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->trg_bn_cdr_date; ?></td>
                            <td><?php echo $val->trg_bn_cdr_marks; ?></td>
                            <td><?php echo $val->trg_bn_cdr_grade; ?></td>
                            <td><?php echo $val->trg_bn_cdr_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 3rd Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Date</label></th>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($trg_bn_cdr_test){
									  $i = 1;
									$trg_arr = json_decode($trg_bn_cdr_test[0]->third_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->trg_bn_cdr_date; ?></td>
                            <td><?php echo $val->trg_bn_cdr_marks; ?></td>
                            <td><?php echo $val->trg_bn_cdr_grade; ?></td>
                            <td><?php echo $val->trg_bn_cdr_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
            </div>

			<div style="width:100%;display:inline-block">
				<h4 class="box-title"> COMMANDANT TEST BPET </h4>
                <div style="width:40%; float: left;">
                    <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Event</label></th>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_bpet){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_bpet[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->commandant_test_bpet_mark; ?></td>
                            <td><?php echo $val->commandant_test_bpet_grade; ?></td>
                            <td><?php echo $val->commandant_test_bpet_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 2nd Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_bpet){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_bpet[0]->second_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->commandant_test_bpet_mark; ?></td>
                            <td><?php echo $val->commandant_test_bpet_grade; ?></td>
                            <td><?php echo $val->commandant_test_bpet_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 3rd Attempt </h5>
                    <table border="1" style="width:100%;">
                        <tr>
                            <th><label>Marks</label></th>
                            <th><label>Grade</label></th>
                            <th><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_bpet){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_bpet[0]->third_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                           <td><?php echo $val->commandant_test_bpet_mark; ?></td>
                            <td><?php echo $val->commandant_test_bpet_grade; ?></td>
                            <td><?php echo $val->commandant_test_bpet_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
            </div>
            <div style="width:100%;display:inline-block">
					<h4 class="box-title"> COMMANDANT TEST FIRING </h4>
                <div style="width:40%; float: left;">
                    <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                    <table border="1" style="width:100%; text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Event</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_firing){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_firing[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->commandant_test_firing_date; ?></td>
                            <td><?php echo $val->commandant_test_firing_mark; ?></td>
                            <td><?php echo $val->commandant_test_firing_result; ?></td>
                            <td><?php echo $val->commandant_test_firing_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 2nd Attempt </h5>
                   <table border="1" style="width:100%;text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Event</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_firing){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_firing[0]->second_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->commandant_test_firing_date; ?></td>
                            <td><?php echo $val->commandant_test_firing_mark; ?></td>
                            <td><?php echo $val->commandant_test_firing_result; ?></td>
                            <td><?php echo $val->commandant_test_firing_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 3rd Attempt </h5>
                     <table border="1" style="width:100%;text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Event</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_firing){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_firing[0]->third_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->commandant_test_firing_date; ?></td>
                            <td><?php echo $val->commandant_test_firing_mark; ?></td>
                            <td><?php echo $val->commandant_test_firing_result; ?></td>
                            <td><?php echo $val->commandant_test_firing_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
            </div>
             <div style="width:100%;display:inline-block">
					<h4 class="box-title"> COMMANDANT TEST FIRING </h4>
                <div style="width:40%; float: left;">
                    <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                    <table border="1" style="width:100%; text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Event</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_firing_another){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_firing_another[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->commandant_test_firing_date; ?></td>
                            <td><?php echo $val->commandant_test_firing_mark; ?></td>
                            <td><?php echo $val->commandant_test_firing_result; ?></td>
                            <td><?php echo $val->commandant_test_firing_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 2nd Attempt </h5>
                   <table border="1" style="width:100%;text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Event</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_firing_another){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_firing_another[0]->second_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->commandant_test_firing_date; ?></td>
                            <td><?php echo $val->commandant_test_firing_mark; ?></td>
                            <td><?php echo $val->commandant_test_firing_result; ?></td>
                            <td><?php echo $val->commandant_test_firing_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 3rd Attempt </h5>
                     <table border="1" style="width:100%;text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Event</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($commandent_test_firing_another){
									  $i = 1;
									$trg_arr = json_decode($commandent_test_firing_another[0]->third_attempt_test);
									 foreach($trg_arr as $val){ ?>
                        <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
                            <td><?php echo $val->name_trg; ?></td>
                            <td><?php echo $val->commandant_test_firing_date; ?></td>
                            <td><?php echo $val->commandant_test_firing_mark; ?></td>
                            <td><?php echo $val->commandant_test_firing_result; ?></td>
                            <td><?php echo $val->commandant_test_firing_sign; ?></td>
                        </tr>
                        <?php	 
								  }
								   }
								  ?>

                    </table>
                </div>
            </div>
			
			<div style="width:100%;display:inline-block">
					<h4 class="box-title"> BASIC FOUNDATION COURSE</h4>
                <div style="width:40%; float: left;">
                    <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                    <table border="1" style="width:100%; text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Course</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($basic_foundation_course){
									  $i = 1;
									$trg_arr = json_decode($basic_foundation_course[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
										<tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
											<td><?php echo $val->name_trg; ?></td>
											<td><?php echo $val->basic_course_date; ?></td>
											<td><?php echo $val->basic_course_mark; ?></td>
											<td><?php echo $val->basic_course_result; ?></td>
											<td><?php echo $val->basic_course_sign; ?></td>
										</tr>
										<?php	 
												  }
												   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 2nd Attempt </h5>
                   <table border="1" style="width:100%; text-align: center;">
                        <tr>
                           <th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($basic_foundation_course){
									  $i = 1;
									$trg_arr = json_decode($basic_foundation_course[0]->second_attempt_test);
									 foreach($trg_arr as $val){ ?>
										<tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
											<td><?php echo $val->basic_course_date; ?></td>
											<td><?php echo $val->basic_course_mark; ?></td>
											<td><?php echo $val->basic_course_result; ?></td>
											<td><?php echo $val->basic_course_sign; ?></td>
										</tr>
										<?php	 
												  }
												   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 3rd Attempt </h5>
                     <table border="1" style="width:100%; text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($basic_foundation_course){
									  $i = 1;
									$trg_arr = json_decode($basic_foundation_course[0]->third_attempt_test);
									 foreach($trg_arr as $val){ ?>
										<tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
											<td><?php echo $val->basic_course_date; ?></td>
											<td><?php echo $val->basic_course_mark; ?></td>
											<td><?php echo $val->basic_course_result; ?></td>
											<td><?php echo $val->basic_course_sign; ?></td>
										</tr>
										<?php	 
												  }
												   }
								  ?>

                    </table>
                </div>
            </div>
			
			<div style="width:100%;display:inline-block">
					<h4 class="box-title"> MAP Reading Test </h4>
                <div style="width:40%; float: left;">
                    <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                    <table border="1" style="width:100%; text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Event</label></th>
							<th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($map_reading_test){
									  $i = 1;
									$trg_arr = json_decode($map_reading_test[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
										<tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
											<td><?php echo $val->name_trg; ?></td>
											<td><?php echo $val->map_reading_date; ?></td>
											<td><?php echo $val->map_reading_mark; ?></td>
											<td><?php echo $val->map_reading_result; ?></td>
											<td><?php echo $val->map_reading_sign; ?></td>
										</tr>
										<?php	 
												  }
												   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 2nd Attempt </h5>
                   <table border="1" style="width:100%; text-align: center;">
                        <tr>
                           <th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($map_reading_test){
									  $i = 1;
									$trg_arr = json_decode($map_reading_test[0]->second_attempt_test);
									 foreach($trg_arr as $val){ ?>
										<tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
											<td><?php echo $val->map_reading_date; ?></td>
											<td><?php echo $val->map_reading_mark; ?></td>
											<td><?php echo $val->map_reading_result; ?></td>
											<td><?php echo $val->map_reading_sign; ?></td>
										</tr>
										<?php	 
												  }
												   }
								  ?>

                    </table>
                </div>
                <div style="width:30%; float: left;">
                    <h5 style="margin-left: 11px;"> 3rd Attempt </h5>
                     <table border="1" style="width:100%; text-align: center;">
                        <tr>
                            <th style="text-align: center;"><label>Date</label></th>
                            <th style="text-align: center;"><label>Marks</label></th>
                            <th style="text-align: center;"><label>Result</label></th>
                            <th style="text-align: center;"><label>Signature</label></th>
                        </tr>
                        <?php
								  
								  if($map_reading_test){
									  $i = 1;
									$trg_arr = json_decode($map_reading_test[0]->third_attempt_test);
									 foreach($trg_arr as $val){ ?>
										<tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
											<td><?php echo $val->map_reading_date; ?></td>
											<td><?php echo $val->map_reading_mark; ?></td>
											<td><?php echo $val->map_reading_result; ?></td>
											<td><?php echo $val->map_reading_sign; ?></td>
										</tr>
										<?php	 
												  }
												   }
								  ?>

                    </table>
                </div>
            </div>
			
			
        </div>
    </div>
</div>

<script>
    $('document').ready(function() {

        function print(){
			 window.print();
		}

    });
</script>