<link href="<?php echo base_url();?>assets/css/jquery-ui.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>

  <script>
$(function() {
        var date = new Date();
        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: 'c-75:c+75',
        });
		
    });
	
 
 </script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<style>
.error,.required{
	color:red;
}
.form-control{
	padding:0px;
}
.row{
	margin: 0px;
}
.rel_data{
	margin-bottom: 11px;
    margin-left: -22px;
}
.removepara{
	cursor: pointer;
}
.content-wrapper{
	margin-left:0px;
}

</style>

<div class="content-wrapper animated fadeInRight">
<div class="content"> 
 <div id="headerMsg"> </div>
	<div class="row">
	   <div class="col-lg-12" style="padding: 0px;">
	     <div class="box " style="padding: 11px;">
			<div class="ibox float-e-margins">
				<div class="box-header with-border">
					  <h3 class="box-title"><label style="font-size: 18px;    font-weight: bold;">Registration </label></h3>
						<a style="float: right;" href="<?php echo base_url(); ?>admin/registration/candidate_list"  class="btn btn-default pull-right">Candidate list</a>
				           <a style="float: right;margin-right: 45px;" href="<?php echo base_url(); ?>admin/configure"  class="btn btn-success">Deshboard</a>
						   
				</div>
				
				<div class="ibox-content">
				<form id="registeredit_form" method="post" role="form">
				 <h4 class="box-title">Persional information</h4>
					<div class="row">
					<input type="hidden" name="candidate_id" id="candidate_id" value="<?php echo $candidate_id; ?>">
					 <div class="col-md-1" > <label> Army No </label></div>
					 <div class="col-md-3" ><input type="text" class="form-control" name="candidate_army_no" id="candidate_army_no" value="<?php echo $candidate_info[0]->candidate_army_no; ?>"  placeholder="Enter Army No"> </div>
					 <div class="col-md-1" > <label>  Course  </label> </div>
					 <div class="col-md-3" ><input type="text" class="form-control" name="candidate_course" id="candidate_course" value="<?php echo $candidate_info[0]->candidate_course; ?>"  placeholder="Enter Course"> </div>
					  <div class="col-md-1" > <label>  Company  </label> </div>
					 <div class="col-md-3" ><input type="text" class="form-control" name="candidate_company" id="candidate_company" value="<?php echo $candidate_info[0]->candidate_company; ?>"  placeholder="Enter Company"> </div>
					</div>
					</br>
					<div class="row">
					 <div class="col-md-1"> <label> Name </label></div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_name" id="candidate_name" value="<?php echo $candidate_info[0]->candidate_name; ?>"  placeholder="Enter candidate name"> </div>
					 <div class="col-md-1"> <label>  Section  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_section" id="candidate_section" value="<?php echo $candidate_info[0]->candidate_section; ?>"  placeholder="Enter candidate section"> </div>
					  <div class="col-md-1"> <label>  Year  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_year" id="candidate_year" value="<?php echo $candidate_info[0]->candidate_year; ?>"  placeholder="Enter candidate year"> </div>
					</div>
					</br>
						<div class="row">
					 <div class="col-md-1"> <label> Rank </label></div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_rank" id="candidate_rank" value="<?php echo $candidate_info[0]->candidate_rank; ?>" placeholder="Enter candidate rank"> </div>
					 <div class="col-md-1"> <label>  Platoon  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_platoon" id="candidate_platoon" value="<?php echo $candidate_info[0]->candidate_platoon; ?>" placeholder="Enter candidate platoon"> </div>
					  <div class="col-md-1"> <label>  ARO/UHQ  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_aro_uhq" id="candidate_aro_uhq" value="<?php echo $candidate_info[0]->candidate_aro_uhq; ?>"  placeholder="Enter ARO/UHQ "> </div>
					</div>
					</br>
					<div class="row">
					<div class="col-md-6"> 
					<div class="row" style="margin-left: -31px;">
                       <h4 style="padding-left: 20px;">Bio Data </h4>					
					<div class="col-md-6"> 
					<table class="table table-hover" style="width:100%" >
						  <tr  class="table-active">
							<th><label>DOB:</label></th>
							<td><input type="text" autocomplete="off" class="form-control date" name="candidate_dob" value="<?php echo $candidate_info[0]->candidate_dob; ?>" id="candidate_dob"  placeholder="Enter DOB"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>DOE:</label></th>
							<td><input type="text" autocomplete="off" class="form-control date" name="candidate_doe" value="<?php echo $candidate_info[0]->candidate_doe; ?>" id="candidate_doe"  placeholder="Enter DOE"> </td>
						  </tr>
						 <tr  class="table-active">
							<th><label>POE:</label></th>
							<td><input type="text" autocomplete="off" class="form-control date" name="candidate_poe" value="<?php echo $candidate_info[0]->candidate_poe; ?>" id="candidate_poe"  placeholder="Enter POE"> </td>
						  </tr>
						   <tr  class="table-active">
							<th><label>ADDRESS:</label></th>
							<td><input type="text" class="form-control" id="candidate_address" name="candidate_address" value="<?php echo $candidate_info[0]->candidate_address; ?>"  placeholder="Enter ADDRESS"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>BLOOD GROUP:</label></th>
							<td><input type="text" class="form-control" name="candidate_blood_group" value="<?php echo $candidate_info[0]->candidate_blood_group; ?>" id="candidate_blood_group"  placeholder="Enter BLOOD GROUP"> </td>
						  </tr>
						</table>
						
					</div>
					<div class="col-md-6" style="padding:0px"> 
					<table class="table table-hover" style="width:100%">
					  <tr  class="table-active">
							<th><label>Religion:</label></th>
							<td><input type="text" class="form-control" name="candidate_religion" value="<?php echo $candidate_info[0]->candidate_religion; ?>" id="candidate_religion"  placeholder="Enter Religion"> </td>
						  </tr>
					<tr  class="table-active">
							<th><label>Caste:</label></th>
							<td><input type="text" class="form-control" name="candidate_caste" value="<?php echo $candidate_info[0]->candidate_caste; ?>" id="candidate_caste"  placeholder="Enter Caste"> </td>
						  </tr>
					  <tr  class="table-active">
							<th><label>Village:</label></th>
							<td><input type="text" class="form-control" name="candidate_village" value="<?php echo $candidate_info[0]->candidate_village; ?>" id="candidate_village"  placeholder="Enter Village"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>Post office:<label></th>
							<td><input type="text" class="form-control" name="candidate_post_office" value="<?php echo $candidate_info[0]->candidate_post_office; ?>" id="candidate_post_office"  placeholder="Enter Post office"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>District:</label></th>
							<td><input type="text" class="form-control" name="candidate_district" value="<?php echo $candidate_info[0]->candidate_district; ?>" id="candidate_district"  placeholder="Enter District"> </td>
						  </tr>
						   <tr  class="table-active">
							<th><label>State:</label></th>
							<td><input type="text" class="form-control" id="candidate_state" value="<?php echo $candidate_info[0]->candidate_state; ?>" name="candidate_state"  placeholder="Enter State"> </td>
						  </tr>
					</table>
					</div>
					
					</div>
					<div class="row">
					<h4> Faimly details </h4>
					<hr>
					<div class="row rel_data" >
						   <div class="col-md-3"><label>Relation</label></div>
						   <div class="col-md-4"><label>Name</label></div>
						   <div class="col-md-3"><label>DOB</label></div>
						   <div class="col-md-2"><label>Age</label></div>
					</div> 
					<div class="TextBoxContainer2">	
					<?php 
					 if($candidate_family_info){
						 foreach($candidate_family_info as $val){ ?>
							
						<div class="row rel_data remove-added-para" style="margin-bottom: 11px;">
						   <div class="col-md-3"><input style="width: 100%;" type="text"  id="candidate_relation" name="candidate_relation"  placeholder="Relation" value="<?php echo $val->candidate_relation; ?>"></div>
						   <div class="col-md-4"><input type="text" class="" id="candidate_relation_name" name="candidate_relation_name"  placeholder="Name" value="<?php echo $val->candidate_relation_name; ?>" /></div>
						   <div class="col-md-3" style="padding: 0px;"><input style="width: 100%;" type="date" class="" id="candidate_relation_dob" value="<?php echo $val->candidate_relation_dob; ?>" name="candidate_relation_dob" placeholder="DOB"></div>
						   <div class="col-md-2"><input type="text" class="" id="candidate_relation_age" name="candidate_relation_age"  placeholder="Age" value="<?php echo $val->candidate_relation_age; ?>" style="width: 100%;"></div>
					    </div> 
							 
					<?php	 }
					 }
					
					?>
					 
										
						  
						  </div>
                         <div class="col-md-12" style="margin-left: -22px;">
						  <input id="addProSpeci" class="btn btn-success" type="button" value="&#43" />
					    </div> 						
						</div>
					</div>
						<div class="col-md-6">
						<h4> Instructer</h4>
						<hr>
						 <div class="row">
						  <table class="table" style="width:100%">
							  <tr>
								<th>APPT</th>
								<th>Rank</th>
								<th>Name</th>
								<th>From Date</th>
							  </tr>
							  <tr>
								<td><label>Company commander</label></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_rank ; ?>" id="company_commander_rank" name="company_commander_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_name ; ?>" id="company_commander_name" name="company_commander_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->company_commander_form_date ; ?>" id="company_commander_form_date" name="company_commander_form_date" placeholder="From Date"></td>
							  </tr>
							   <tr>
								<td><label>Platoon commander</label></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_rank ; ?>" id="platoon_commander_rank" name="platoon_commander_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_name ; ?>" id="platoon_commander_name" name="platoon_commander_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_commander_form_date ; ?>" id="platoon_commander_form_date" name="platoon_commander_form_date" placeholder="From Date"></td>
							  </tr>
							   <tr>
								<td><label>Platoon havaldar 1</label></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_rank ; ?>" id="platoon_havaldar_first_rank" name="platoon_havaldar_first_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_name ; ?>" id="platoon_havaldar_first_name" name="platoon_havaldar_first_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_first_form_date ; ?>" id="platoon_havaldar_first_form_date" name="platoon_havaldar_first_form_date" placeholder="From Date"></td>
							  </tr>
							   <tr>
								<td><label>Platoon havaldar 2</label></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_rank ; ?>" id="platoon_havaldar_second_rank" name="platoon_havaldar_second_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_name ; ?>" id="platoon_havaldar_second_name" name="platoon_havaldar_second_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="<?php echo  json_decode($candidate_info[0]->instructer_info)->platoon_havaldar_second_form_date ; ?>" id="platoon_havaldar_second_form_date" name="platoon_havaldar_second_form_date" placeholder="From Date"></td>
							  </tr>
							</table>
						 </div>
						 <div class="row">
						 <h4> Education qualification</h4>
						<hr>
					<div class="col-md-12" style="margin-bottom: 11px;">
						   <div class="col-md-2"><label>Civil</label></div>
						   <div class="col-md-4"><input class="form-control"  type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_civil;  ?>" id="qualification_civil" name="qualification_civil" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date"  type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_civil_date;  ?>" name="qualification_civil_date" id="qualification_civil_date"  placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" type="text" id="qualification_civil_part_order"  name="qualification_civil_part_order" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_civil_part_order;  ?>" placeholder="Part-II order" style="width: 100%;"></div>
					</div>  
					<div class="col-md-12" style="margin-bottom: 11px;">
						   <div class="col-md-2"><label>MR</label></div>
						   <div class="col-md-4"><input class="form-control" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_mr;  ?>" id="qualification_mr" name="qualification_mr" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_mr_date;  ?>"  id="qualification_mr_date" name="qualification_mr_date" placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" type="text"  id="qualification_mr_part_order" name="qualification_mr_part_order" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_mr_part_order;  ?>" placeholder="Part-II order" style="width: 100%;"></div>
					</div>
                    <div class="col-md-12" style="margin-bottom: 11px;">					
						   <div class="col-md-2"><label>AEC</label></div>
						   <div class="col-md-4"><input class="form-control" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_aec;  ?>" name="qualification_aec" id="qualification_aec" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_aec_date;  ?>" id="qualification_aec_date" name="qualification_aec_date" placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" type="text"  name="qualification_aec_part_order" id="qualification_aec_part_order" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_aec_part_order;  ?>" placeholder="Part-II order" style="width: 100%;"></div>
						</div>
                       <div class="col-md-12" style="margin-bottom: 11px;">					
						   <div class="col-md-2"><label>TTT</label></div>
						   <div class="col-md-4"><input class="form-control" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_ttt;  ?>" name="qualification_ttt" id="qualification_ttt" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_ttt_date;  ?>" name="qualification_ttt_date" id="qualification_ttt_date" placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" type="text"  name="qualification_ttt_part_order" id="qualification_ttt_part_order" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_ttt_part_order;  ?>" placeholder="Part-II order" style="width: 100%;"></div>
						</div>	
                        <div class="col-md-12">					
						   <div class="col-md-2"><label>Attestation</label></div>
						   <div class="col-md-4"><input  class="form-control" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_attestation;  ?>" name="qualification_attestation" id="qualification_attestation"  placeholder="qualification"></div>
						   <div class="col-md-3"><input   class="form-control date" type="text" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_attestation_date;  ?>" name="qualification_attestation_date" id="qualification_attestation_date" placeholder="date"></div>
						   <div class="col-md-3"><input  class="form-control" type="text"  name="qualification_attestation_part_order" id="qualification_attestation_part_order" value="<?php echo json_decode($candidate_info[0]->edu_info)->qualification_attestation_part_order;  ?>" placeholder="Part-II order" style="width: 100%;"></div>
						</div>						
						</div>
						 </div>
					</div>
					</br>
					<hr>
					<div class="row">
                    <h4 class="box-title" style="margin-left: 11px;"> Physical Parameters </h4>					
					<div class="col-md-6">
					<table style="width:100%" class="table">
						  <tr>
							<th><label>Height:</label></th>
							<td><input type="text" class="form-control" name="candidate_height" id="candidate_height" placeholder="Height" value="<?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_height; ?>"></td>
						  </tr>
						  <tr>
							<th><label>Permissible Wt:</label></th>
							<td><input type="text" class="form-control" name="candidate_permissible_wt" id="candidate_permissible_wt" placeholder="Permissible Wt" value="<?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_permissible_wt; ?>"></td>
						  </tr>
						  <tr>
							<th><label>Actual Wt:</label></th>
							<td><input type="text" class="form-control" name="candidate_actual_wt" id="candidate_actual_wt" placeholder="Actual Wt" value="<?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_actual_wt; ?>"></td>
						  </tr>
						  <tr>
							<th><label>Over Wt / Under Wt:</label></th>
							<td><input type="text" class="form-control" id="candidate_over_under_wt" name="candidate_over_under_wt" placeholder="Over Wt / Under Wt" value="<?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_over_under_wt; ?>"></td>
						  </tr>
					</table>
					</div>
					<div class="col-md-6">
					  <table style="width:100%" class="table">
						  <tr>
							<th><label>Identification Mark - 1:</label></th>
							<td><input type="text" class="form-control" id="candidate_identification_mark_one" name="candidate_identification_mark_one" placeholder="Identification Mark - 1" value="<?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_identification_mark_one; ?>" ></td>
						  </tr>
						  <tr>
							<th><label>Identification Mark - 2:</label></th>
							<td><input type="text" class="form-control"  name="candidate_identification_mark_two" id="candidate_identification_mark_two" placeholder="Identification Mark - 2" value="<?php echo json_decode($candidate_info[0]->physical_parameters)->candidate_identification_mark_two; ?>"></td>
						  </tr>
						 
					</table>
					</div>
					</div>
					<hr>
					<div class="row">
					<h4 style="margin-left: 11px;"> TRG BN CDR PPT TEST </h4>
					  <div class="col-md-6" style="padding:0px">
					  <h5> 1st Attempt </h5>
                       <table class="table " style="width:100%; border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <?php if($candidate_cdr_ppt_test){ ?>
									  
								  <tr class="trg_one" name_trg="2,4 KM Run">
									<td ><label >2,4 KM Run</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[0]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_2_4_km_mark" name="trg_bn_ppt_2_4_km_mark"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[0]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_2_4_km_grade" name="trg_bn_ppt_2_4_km_grade" required ></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[0]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_2_4_km_sign" name="trg_bn_ppt_2_4_km_sign" required ></td>
								  </tr>
								   <tr  class="trg_one" name_trg="Bent Knee Sit ups">
									<td><label>Bent Knee Sit ups</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[1]->trg_bn_ppt_mark; ?>"  id="trg_bn_ppt_bent_knee_sit_up_mark" name="trg_bn_ppt_bent_knee_sit_up_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[1]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_bent_knee_sit_up_grade" name="trg_bn_ppt_bent_knee_sit_up_grade" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[1]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_bent_knee_sit_up_sign" name="trg_bn_ppt_bent_knee_sit_up_sign" required></td>
								  </tr>
								   <tr  class="trg_one" name_trg="5 M Shuttle">
									<td><label>5 M Shuttle</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[2]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_5m_shuttle_mark" name="trg_bn_ppt_5m_shuttle_mark"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[2]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_5m_shuttle_grade" name="trg_bn_ppt_5m_shuttle_grade"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[2]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_5m_shuttle_sign" name="trg_bn_ppt_5m_shuttle_sign" required></td>
								  </tr >
								   <tr  class="trg_one" name_trg="Toe Touch">
									<td><label>Toe Touch</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[3]->trg_bn_ppt_mark; ?>"  id="trg_bn_ppt_toe_touch_mark" name="trg_bn_ppt_toe_touch_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[3]->trg_bn_ppt_grade; ?>"  id="trg_bn_ppt_toe_touch_grade" name="trg_bn_ppt_toe_touch_grade" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[3]->trg_bn_ppt_sign; ?>"  id="trg_bn_ppt_toe_touch_sign" name="trg_bn_ppt_toe_touch_sign" required></td>
								  </tr>
								   <tr  class="trg_one" name_trg="100 M Sprint">
									<td><label>100 M Sprint</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[4]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_100m_sprint_mark" name="trg_bn_ppt_100m_sprint_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[4]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_100m_sprint_grade" name="trg_bn_ppt_100m_sprint_grade" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[4]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_100m_sprint_sign" name="trg_bn_ppt_100m_sprint_sign" required ></td>
								  </tr>
								   <tr  class="trg_one" name_trg="Chin up">
									<td><label>Chin up</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[5]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_chinup_mark"  name="trg_bn_ppt_chinup_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[5]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_chinup_grade" name="trg_bn_ppt_chinup_grade"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->first_attempt_test)[5]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_chinup_sign" name="trg_bn_ppt_chinup_sign" required ></td>
								  </tr>
								  <tr  >
									<td><label>Over All</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_overallmark" name="trg_bn_ppt_overallmark" ></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_overallgrade" name="trg_bn_ppt_overallgrade" ></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_overallsign" name="trg_bn_ppt_overallsign"  ></td>
								  </tr>
								  
								 <?php } ?>
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="trg_2nd" name_trg="2,4 KM Run">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[0]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_2_4_km_mark_2nd" name="trg_bn_ppt_2_4_km_mark_2nd"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[0]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_2_4_km_grade_2nd" name="trg_bn_ppt_2_4_km_grade_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[0]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_2_4_km_sign_2nd" name="trg_bn_ppt_2_4_km_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="Bent Knee Sit ups">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[1]->trg_bn_ppt_mark; ?>"  id="trg_bn_ppt_bent_knee_sit_up_mark_2nd" name="trg_bn_ppt_bent_knee_sit_up_mark_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[1]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_bent_knee_sit_up_grade_2nd" name="trg_bn_ppt_bent_knee_sit_up_grade_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[1]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_bent_knee_sit_up_sign_2nd" name="trg_bn_ppt_bent_knee_sit_up_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="5 M Shuttle">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[2]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_5m_shuttle_mark_2nd" name="trg_bn_ppt_5m_shuttle_mark_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[2]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_5m_shuttle_grade_2nd"  name="trg_bn_ppt_5m_shuttle_grade_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[2]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_5m_shuttle_sign_2nd" name="trg_bn_ppt_5m_shuttle_sign_2nd" required></td>
								  </tr >
								   <tr  class="trg_2nd" name_trg="Toe Touch">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[3]->trg_bn_ppt_mark; ?>"  id="trg_bn_ppt_toe_touch_mark_2nd" name="trg_bn_ppt_toe_touch_mark_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[3]->trg_bn_ppt_grade; ?>"  id="trg_bn_ppt_toe_touch_grade_2nd" name="trg_bn_ppt_toe_touch_grade_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[3]->trg_bn_ppt_sign; ?>"  id="trg_bn_ppt_toe_touch_sign_2nd" name="trg_bn_ppt_toe_touch_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="100 M Sprint">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[4]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_100m_sprint_mark_2nd" name="trg_bn_ppt_100m_sprint_mark_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[4]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_100m_sprint_grade_2nd" name="trg_bn_ppt_100m_sprint_grade_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[4]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_100m_sprint_sign_2nd" name="trg_bn_ppt_100m_sprint_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="Chin up">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[5]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_chinup_mark_2nd"  name="trg_bn_ppt_chinup_mark_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[5]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_chinup_grade_2nd" name="trg_bn_ppt_chinup_grade_2nd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->second_attempt_test)[5]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_chinup_sign_2nd" name="trg_bn_ppt_chinup_sign_2nd" required></td>
								  </tr>
								  <tr  >
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_mark"  ></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_grade" ></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_sign" ></td>
								  </tr>
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								  <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="trg_3rd" name_trg="2,4 KM Run">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[0]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_2_4_km_mark_3rd"  name="trg_bn_ppt_2_4_km_mark_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[0]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_2_4_km_grade_3rd" name="trg_bn_ppt_2_4_km_grade_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[0]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_2_4_km_sign_3rd" name="trg_bn_ppt_2_4_km_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="Bent Knee Sit ups">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[1]->trg_bn_ppt_mark; ?>"  id="trg_bn_ppt_bent_knee_sit_up_mark_3rd" name="trg_bn_ppt_bent_knee_sit_up_mark_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[1]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_bent_knee_sit_up_grade_3rd" name="trg_bn_ppt_bent_knee_sit_up_grade_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[1]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_bent_knee_sit_up_sign_3rd" name="trg_bn_ppt_bent_knee_sit_up_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="5 M Shuttle">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[2]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_5m_shuttle_mark_3rd"  name="trg_bn_ppt_5m_shuttle_mark_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[2]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_5m_shuttle_grade_3rd"  name="trg_bn_ppt_5m_shuttle_grade_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[2]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_5m_shuttle_sign_3rd" name="trg_bn_ppt_5m_shuttle_sign_3rd" required></td>
								  </tr >
								   <tr  class="trg_3rd" name_trg="Toe Touch">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[3]->trg_bn_ppt_mark; ?>"  id="trg_bn_ppt_toe_touch_mark_3rd" name="trg_bn_ppt_toe_touch_mark_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[3]->trg_bn_ppt_grade; ?>"  id="trg_bn_ppt_toe_touch_grade_3rd" name="trg_bn_ppt_toe_touch_grade_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[3]->trg_bn_ppt_sign; ?>"  id="trg_bn_ppt_toe_touch_sign_3rd" name="trg_bn_ppt_toe_touch_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="100 M Sprint">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[4]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_100m_sprint_mark_3rd" name="trg_bn_ppt_100m_sprint_mark_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[4]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_100m_sprint_grade_3rd" name="trg_bn_ppt_100m_sprint_grade_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[4]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_100m_sprint_sign_3rd" name="trg_bn_ppt_100m_sprint_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="Chin up">
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[5]->trg_bn_ppt_mark; ?>" id="trg_bn_ppt_chinup_mark_3rd"  name="trg_bn_ppt_chinup_mark_3rd" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[5]->trg_bn_ppt_grade; ?>" id="trg_bn_ppt_chinup_grade_3rd"  name="trg_bn_ppt_chinup_grade_3rd"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="<?php echo json_decode($candidate_cdr_ppt_test[0]->third_attempt_test)[5]->trg_bn_ppt_sign; ?>" id="trg_bn_ppt_chinup_sign_3rd" name="trg_bn_ppt_chinup_sign_3rd" required></td>
								  </tr>
								  <tr  >
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_mark"  ></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_grade" ></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_sign" ></td>
								  </tr>
								  
								</table>
					  </div>
					</div>
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> TRG BN CDR TEST </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <?php
								  
								  if($trg_bn_cdr_test){
									  $i = 1;
									$trg_arr = json_decode($trg_bn_cdr_test[0]->first_attempt_test);
									 foreach($trg_arr as $val){ ?>
										 <tr class="trg_bn" name_trg="<?php echo $val->name_trg; ?>">
											<td><label><?php echo $val->name_trg; ?></label></td>
											<td><input type="text" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_date<?php echo $i++; ?>" name="trg_bn_cdr_date<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_date; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_marks<?php echo $i++; ?>"  name="trg_bn_cdr_marks<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_marks; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_grade<?php echo $i++; ?>" name="trg_bn_cdr_grade<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_grade; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_sign<?php echo $i++; ?>" name="trg_bn_cdr_sign<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_sign; ?>" required></td>
										  </tr > 
								<?php	 
								  }
								   }
								  ?>
								 
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
								  
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								    <?php
								  
								  if($trg_bn_cdr_test){
									  $i = 1;
									$trg_arr = json_decode($trg_bn_cdr_test[0]->second_attempt_test);
									 foreach($trg_arr as $val){ ?>
										  <tr class="trg_bn_2nd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_date<?php echo $i++; ?>" name="trg_bn_cdr_date<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_date; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_marks<?php echo $i++; ?>"  name="trg_bn_cdr_marks<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_marks; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_grade<?php echo $i++; ?>" name="trg_bn_cdr_grade<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_grade; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_sign<?php echo $i++; ?>" name="trg_bn_cdr_sign<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_sign; ?>" required></td>
										  </tr > 
								<?php	 
								  }
								   }
								  ?>
								  
								   
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								  <tr>
								    <th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								      <?php
								  
								  if($trg_bn_cdr_test){
									  $i = 1;
									$trg_arr3 = json_decode($trg_bn_cdr_test[0]->third_attempt_test);
									 foreach($trg_arr3 as $val){ ?>
										  <tr class="trg_bn_3rd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_date<?php echo $i++; ?>" name="trg_bn_cdr_date<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_date; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_marks<?php echo $i++; ?>"  name="trg_bn_cdr_marks<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_marks; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_grade<?php echo $i++; ?>" name="trg_bn_cdr_grade<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_grade; ?>" required></td>
											<td><input type="text" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_sign<?php echo $i++; ?>" name="trg_bn_cdr_sign<?php echo $i++; ?>" value="<?php echo $val->trg_bn_cdr_sign; ?>" required></td>
										  </tr > 
								<?php	 
								  }
								   }
								  ?>
								
								  
								</table>
					  </div>
					</div>
					
					<div class="row" style="margin-left: 1px;">
					<h4 class="box-title"> COMMANDANT TEST BPET </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								        <?php
								  
								  if($commandent_test_bpet){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_bpet[0]->first_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="commandent_test_bpet" name_trg="<?php echo $val->name_trg; ?>">
											<td><label><?php echo $val->name_trg; ?></label></td>
											<td><input type="text" class="form-control commandant_test_bpet_mark" value="<?php echo $val->commandant_test_bpet_mark; ?>" id="commandant_test_bpet_mark<?php echo $i++; ?>" name="commandant_test_bpet_5km_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_bpet_grade" value="<?php echo $val->commandant_test_bpet_grade; ?>" id="commandant_test_bpet_grade<?php echo $i++; ?>" name="commandant_test_bpet_5km_grade<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_bpet_sign" value="<?php echo $val->commandant_test_bpet_sign; ?>" id="commandant_test_bpet_sign<?php echo $i++; ?>" name="commandant_test_bpet_5km_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								
								 
								   
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								       <?php
								  
								  if($commandent_test_bpet){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_bpet[0]->second_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="commandent_test_bpet_2nd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control commandant_test_bpet_mark" value="<?php echo $val->commandant_test_bpet_mark; ?>" id="commandant_test_bpet_mark<?php echo $i++; ?>" name="commandant_test_bpet_5km_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_bpet_grade" value="<?php echo $val->commandant_test_bpet_grade; ?>" id="commandant_test_bpet_grade<?php echo $i++; ?>" name="commandant_test_bpet_5km_grade<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_bpet_sign" value="<?php echo $val->commandant_test_bpet_sign; ?>" id="commandant_test_bpet_sign<?php echo $i++; ?>" name="commandant_test_bpet_5km_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								  
								
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								        <?php
								  
								  if($commandent_test_bpet){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_bpet[0]->third_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="commandent_test_bpet_3rd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control commandant_test_bpet_mark" value="<?php echo $val->commandant_test_bpet_mark; ?>" id="commandant_test_bpet_mark<?php echo $i++; ?>" name="commandant_test_bpet_5km_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_bpet_grade" value="<?php echo $val->commandant_test_bpet_grade; ?>" id="commandant_test_bpet_grade<?php echo $i++; ?>" name="commandant_test_bpet_5km_grade<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_bpet_sign" value="<?php echo $val->commandant_test_bpet_sign; ?>" id="commandant_test_bpet_sign<?php echo $i++; ?>" name="commandant_test_bpet_5km_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								 
								
								  
								</table>
					  </div>
					</div>
					
					
					
					<div class="row" style="margin-left: 1px;">
					<h4 class="box-title"> COMMANDANT TEST FIRING </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;" >
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  	        <?php
								  
								  if($commandent_test_firing){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_firing[0]->first_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="test_firing" name_trg="<?php echo $val->name_trg; ?>">
											<td><label><?php echo $val->name_trg; ?></label></td>
											<td><input type="text" class="form-control commandant_test_firing_date" value="<?php echo $val->commandant_test_firing_date; ?>" id="commandant_test_firing_date<?php echo $i++; ?>" name="commandant_test_firing_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_mark" value="<?php echo $val->commandant_test_firing_mark; ?>" id="commandant_test_firing_mark<?php echo $i++; ?>"  name="commandant_test_firing_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_result" value="<?php echo $val->commandant_test_firing_result; ?>" id="commandant_test_firing_result<?php echo $i++; ?>"  name="commandant_test_firing_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control commandant_test_firing_sign" value="<?php echo $val->commandant_test_firing_sign; ?>" id="commandant_test_firing_sign<?php echo $i++; ?>"  name="commandant_test_firing_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								  
								   
								   
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   	        <?php
								  
								  if($commandent_test_firing){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_firing[0]->second_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="test_firing_2nd" name_trg="<?php echo $val->name_trg; ?>">
											<td ><input type="text" class="form-control commandant_test_firing_date" value="<?php echo $val->commandant_test_firing_date; ?>" id="commandant_test_firing_date<?php echo $i++; ?>" name="commandant_test_firing_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_mark" value="<?php echo $val->commandant_test_firing_mark; ?>" id="commandant_test_firing_mark<?php echo $i++; ?>"  name="commandant_test_firing_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_result" value="<?php echo $val->commandant_test_firing_result; ?>" id="commandant_test_firing_result<?php echo $i++; ?>"  name="commandant_test_firing_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control commandant_test_firing_sign" value="<?php echo $val->commandant_test_firing_sign; ?>" id="commandant_test_firing_sign<?php echo $i++; ?>"  name="commandant_test_firing_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								  
								
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   	        <?php
								  
								  if($commandent_test_firing){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_firing[0]->third_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="test_firing_3rd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control commandant_test_firing_date" value="<?php echo $val->commandant_test_firing_date; ?>" id="commandant_test_firing_date<?php echo $i++; ?>" name="commandant_test_firing_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_mark" value="<?php echo $val->commandant_test_firing_mark; ?>" id="commandant_test_firing_mark<?php echo $i++; ?>"  name="commandant_test_firing_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_result" value="<?php echo $val->commandant_test_firing_result; ?>" id="commandant_test_firing_result<?php echo $i++; ?>"  name="commandant_test_firing_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control commandant_test_firing_sign" value="<?php echo $val->commandant_test_firing_sign; ?>" id="commandant_test_firing_sign<?php echo $i++; ?>"  name="commandant_test_firing_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								 
								 
								  
								</table>
					  </div>
					</div>
					
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> COMMANDANT TEST FIRING </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								<?php
								  
								  if($commandent_test_firing_another){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_firing_another[0]->first_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="test_firing_other" name_trg="<?php echo $val->name_trg; ?>">
											<td><label><?php echo $val->name_trg; ?></label></td>
											<td><input type="text" class="form-control commandant_test_firing_date" value="<?php echo $val->commandant_test_firing_date; ?>" id="commandant_test_firing_date1<?php echo $i++; ?>" name="commandant_test_firing_date1<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_mark" value="<?php echo $val->commandant_test_firing_mark; ?>" id="commandant_test_firing_mark1<?php echo $i++; ?>"  name="commandant_test_firing_mark1<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_result" value="<?php echo $val->commandant_test_firing_result; ?>" id="commandant_test_firing_result1<?php echo $i++; ?>"  name="commandant_test_firing_result1<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control commandant_test_firing_sign" value="<?php echo $val->commandant_test_firing_sign; ?>" id="commandant_test_firing_sign1<?php echo $i++; ?>"  name="commandant_test_firing_sign1<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								  
								 
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <?php
								  
								  if($commandent_test_firing_another){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_firing_another[0]->second_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="test_firing_other_2nd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control commandant_test_firing_date" value="<?php echo $val->commandant_test_firing_date; ?>" id="commandant_test_firing_date2<?php echo $i++; ?>" name="commandant_test_firing_date2<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_mark" value="<?php echo $val->commandant_test_firing_mark; ?>" id="commandant_test_firing_mark2<?php echo $i++; ?>"  name="commandant_test_firing_mark2<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_result" value="<?php echo $val->commandant_test_firing_result; ?>" id="commandant_test_firing_result2<?php echo $i++; ?>"  name="commandant_test_firing_result2<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control commandant_test_firing_sign" value="<?php echo $val->commandant_test_firing_sign; ?>" id="commandant_test_firing_sign2<?php echo $i++; ?>"  name="commandant_test_firing_sign2<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <?php
								  
								  if($commandent_test_firing_another){
									  $i = 1;
									$trg_arr3 = json_decode($commandent_test_firing_another[0]->third_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="test_firing_other_3rd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control commandant_test_firing_date" value="<?php echo $val->commandant_test_firing_date; ?>" id="commandant_test_firing_date3<?php echo $i++; ?>" name="commandant_test_firing_date3<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_mark" value="<?php echo $val->commandant_test_firing_mark; ?>" id="commandant_test_firing_mark3<?php echo $i++; ?>"  name="commandant_test_firing_mark3<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control commandant_test_firing_result" value="<?php echo $val->commandant_test_firing_result; ?>" id="commandant_test_firing_result3<?php echo $i++; ?>"  name="commandant_test_firing_result3<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control commandant_test_firing_sign" value="<?php echo $val->commandant_test_firing_sign; ?>" id="commandant_test_firing_sign3<?php echo $i++; ?>"  name="commandant_test_firing_sign3<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								
								  
								</table>
					  </div>
					</div>
					
					
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> BASIC FOUNDATION COURSE </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Course</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <?php
								  
								  if($basic_foundation_course){
									  $i = 1;
									$trg_arr3 = json_decode($basic_foundation_course[0]->first_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="basic_course" name_trg="<?php echo $val->name_trg; ?>">
											<td><label> <?php echo $val->name_trg; ?> </label></td>
											<td><input type="text" class="form-control basic_course_date date" value="<?php echo $val->basic_course_date; ?>" id="basic_course_date<?php echo $i++; ?>" name="basic_course_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control basic_course_mark" value="<?php echo $val->basic_course_mark; ?>" id="basic_course_mark<?php echo $i++; ?>"  name="basic_course_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control basic_course_result" value="<?php echo $val->basic_course_result; ?>" id="basic_course_result<?php echo $i++; ?>"  name="basic_course_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control basic_course_sign" value="<?php echo $val->basic_course_sign; ?>" id="basic_course_sign<?php echo $i++; ?>"  name="basic_course_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								  
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <?php
								  
								  if($basic_foundation_course){
									  $i = 11;
									$trg_arr3 = json_decode($basic_foundation_course[0]->second_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="basic_course_2nd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control basic_course_date date" value="<?php echo $val->basic_course_date; ?>" id="basic_course_date<?php echo $i++; ?>" name="basic_course_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control basic_course_mark" value="<?php echo $val->basic_course_mark; ?>" id="basic_course_mark<?php echo $i++; ?>"  name="basic_course_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control basic_course_result" value="<?php echo $val->basic_course_result; ?>" id="basic_course_result<?php echo $i++; ?>"  name="basic_course_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control basic_course_sign" value="<?php echo $val->basic_course_sign; ?>" id="basic_course_sign<?php echo $i++; ?>"  name="basic_course_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								  
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								     <?php
								  
								  if($basic_foundation_course){
									  $i = 31;
									$trg_arr3 = json_decode($basic_foundation_course[0]->third_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="basic_course_3rd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control basic_course_date date" value="<?php echo $val->basic_course_date; ?>" id="basic_course_date<?php echo $i++; ?>" name="basic_course_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control basic_course_mark" value="<?php echo $val->basic_course_mark; ?>" id="basic_course_mark<?php echo $i++; ?>"  name="basic_course_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control basic_course_result" value="<?php echo $val->basic_course_result; ?>" id="basic_course_result<?php echo $i++; ?>"  name="basic_course_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control basic_course_sign" value="<?php echo $val->basic_course_sign; ?>" id="basic_course_sign<?php echo $i++; ?>"  name="basic_course_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								  
								</table>
					  </div>
					</div>
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> MAP Reading Test </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								     <?php
								  
								  if($map_reading_test){
									  $i = 11;
									$trg_arr3 = json_decode($map_reading_test[0]->first_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="map_reading" name_trg="<?php echo $val->name_trg; ?>">
											<td><label><?php echo $val->name_trg; ?></label></td>
											<td><input type="text" class="form-control map_reading_date date" value="<?php echo $val->map_reading_date; ?>" id="map_reading_date<?php echo $i++; ?>" name="map_reading_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control map_reading_mark" value="<?php echo $val->map_reading_mark; ?>" id="map_reading_mark<?php echo $i++; ?>"  name="map_reading_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control map_reading_result" value="<?php echo $val->map_reading_result; ?>" id="map_reading_result<?php echo $i++; ?>"  name="map_reading_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control map_reading_sign" value="<?php echo $val->map_reading_sign; ?>" id="map_reading_sign<?php echo $i++; ?>"  name="map_reading_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								
								   
								   
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								     <?php
								  
								  if($map_reading_test){
									  $i = 22;
									$trg_arr3 = json_decode($map_reading_test[0]->second_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="map_reading_2nd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control map_reading_date date" value="<?php echo $val->map_reading_date; ?>" id="map_reading_date<?php echo $i++; ?>" name="map_reading_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control map_reading_mark" value="<?php echo $val->map_reading_mark; ?>" id="map_reading_mark<?php echo $i++; ?>"  name="map_reading_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control map_reading_result" value="<?php echo $val->map_reading_result; ?>" id="map_reading_result<?php echo $i++; ?>"  name="map_reading_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control map_reading_sign" value="<?php echo $val->map_reading_sign; ?>" id="map_reading_sign<?php echo $i++; ?>"  name="map_reading_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								 
								
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <?php
								  
								  if($map_reading_test){
									  $i = 33;
									$trg_arr3 = json_decode($map_reading_test[0]->third_attempt_test);
									 foreach($trg_arr3 as $val){  ?>
										  <tr class="map_reading_3rd" name_trg="<?php echo $val->name_trg; ?>">
											<td><input type="text" class="form-control map_reading_date date" value="<?php echo $val->map_reading_date; ?>" id="map_reading_date<?php echo $i++; ?>" name="map_reading_date<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control map_reading_mark" value="<?php echo $val->map_reading_mark; ?>" id="map_reading_mark<?php echo $i++; ?>"  name="map_reading_mark<?php echo $i++; ?>" required></td>
											<td><input type="text" class="form-control map_reading_result" value="<?php echo $val->map_reading_result; ?>" id="map_reading_result<?php echo $i++; ?>"  name="map_reading_result<?php echo $i++; ?>"  required></td>
											<td><input type="text" class="form-control map_reading_sign" value="<?php echo $val->map_reading_sign; ?>" id="map_reading_sign<?php echo $i++; ?>"  name="map_reading_sign<?php echo $i++; ?>" required></td>
										 </tr>
								<?php	 
								  }
								   }
								  ?>
								
								  
								</table>
					  </div>
					</div>
					
					
					
				</div>
				<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
				</form>
			</div>
		</div>
		</div>
		
	</div>
	</div>
</div>

<script>	
$('document').ready(function(){
           
		
	
	   
	
	$("body").on("click",'#addProSpeci', function () {
		console.log('TextBoxContainer2');
		var div = $("<div class='remove-added-para'>");
		div.html(GetDynamicTextBoxforSpeci());
		$(".TextBoxContainer2").append(GetDynamicTextBoxforSpeci());
		
		/* $('.TextBoxContainer2').find(".inputdate").datepicker({
               changeMonth: true,
            changeYear: true,
			dateFormat: 'yy-mm-dd',
            yearRange: 'c-75:c+75',
        }); */
	});
	
	$("body").on("click", ".removepara", function () {
		$(this).parent().parent().remove();
	});
	 function GetDynamicTextBoxforSpeci() {
		var content = '<div class="row remove-added-para" style="margin-left: -22px;">\n\
		                <div class="col-md-3" style="padding: 0px;"><span class="removepara" style="float: left;font-size: 25px;color: red;">&times; </span><input required style="float: right;width: 83%;" type="text" class="" placeholder="Relation" name="candidate_relation" id="candidate_relation" value=""></div>\n\
						<div class="col-md-4"><input required type="text"  class="" placeholder="Name" id="candidate_relation_name" name="candidate_relation_name" ></div>\n\
						<div class="col-md-3" style="padding: 0px;"><input style="width:100%" type="date" required class=" inputdate" placeholder="DOB" id="candidate_relation_dob" name="candidate_relation_dob" ></div>\n\
                        <div class="col-md-2"><input type="text" required class="" placeholder="AGE" name="candidate_relation_age" id="candidate_relation_age" style="width: 100%;padding: 0px;float: left;"></div>\n\
                        </div>';
		return 	content;			
	}
 
	$('#registeredit_form').validate({
		ignore: [],
         rules: {
             candidate_army_no: {
                required: true,
            },
			candidate_course: {
                required: true,
            },
			candidate_company: {
                required: true,
            },
			candidate_name: {
                required: true,
            },
			candidate_section: {
                required: true,
            },
			candidate_year: {
                required: true,
            },
			candidate_rank: {
                required: true,
            },
			candidate_platoon: {
                required: true,
            },
			candidate_aro_uhq: {
                required: true,
            },
			candidate_dob: {
                required: true,
            },
			candidate_doe: {
                required: true,
            },
			candidate_poe: {
                required: true,
            },
			candidate_blood_group: {
                required: true,
            },
			candidate_religion: {
                required: true,
            },
			candidate_caste: {
                required: true,
            },
			candidate_address: {
                required: true,
            },
			candidate_village: {
                required: true,
            },
			candidate_post_office: {
                required: true,
            },
			candidate_district: {
                required: true,
            },
			candidate_state: {
                required: true,
            },
			company_commander_rank: {
                required: true,
            },
			company_commander_name: {
                required: true,
            },
			company_commander_form_date: {
                required: true,
            },
			platoon_commander_rank: {
                required: true,
            },
			platoon_commander_name: {
                required: true,
            },
			platoon_commander_form_date: {
                required: true,
            },
			platoon_havaldar_first_rank: {
                required: true,
            },
			platoon_havaldar_first_name: {
                required: true,
            },
			platoon_havaldar_first_form_date: {
                required: true,
            },
			platoon_havaldar_second_rank: {
                required: true,
            },
			platoon_havaldar_second_name: {
                required: true,
            },
			platoon_havaldar_second_form_date: {
                required: true,
            },
			qualification_civil: {
                required: true,
            },
			qualification_mr: {
                required: true,
            },
			qualification_aec: {
                required: true,
            },
			qualification_ttt: {
                required: true,
            },
			qualification_attestation: {
                required: true,
            },
			qualification_civil_date: {
                required: true,
            },
			qualification_civil_part_order: {
                required: true,
            },
			qualification_mr_date: {
                required: true,
            },
			qualification_mr_part_order: {
                required: true,
            },
			qualification_aec_date: {
                required: true,
            },
			qualification_aec_part_order: {
                required: true,
            },
			qualification_ttt_date: {
                required: true,
            },
			qualification_ttt_part_order: {
                required: true,
            },
			qualification_attestation_date: {
                required: true,
            },
			qualification_attestation_part_order: {
                required: true,
            },
			candidate_height: {
                required: true,
            },
			candidate_permissible: {
                required: true,
            },
			candidate_actual_wt: {
                required: true,
            },
			candidate_over_under_wt: {
                required: true,
            },
			candidate_over_under_wt: {
                required: true,
            },
			candidate_identification_mark_one: {
                required: true,
            },
			candidate_identification_mark_two: {
                required: true,
            },
			qualification_civil_date: {
                required: true,
            },
			trg_bn_ppt_2_4_km_mark: {
                required: true,
            },
			trg_bn_ppt_2_4_km_grade: {
                required: true,
            },
			trg_bn_ppt_2_4_km_sign: {
                required: true,
            },
            trg_bn_ppt_bent_knee_sit_up_mark: {
                required: true,
            },
			trg_bn_ppt_bent_knee_sit_up_grade: {
                required: true,
            },
			trg_bn_ppt_bent_knee_sit_up_sign: {
                required: true,
            },
			trg_bn_ppt_5m_shuttle_mark: {
                required: true,
            },
			trg_bn_ppt_5m_shuttle_grade: {
                required: true,
            },
			trg_bn_ppt_5m_shuttle_sign: {
                required: true,
            },
			qualification_civil_date: {
                required: true,
            },
		}, 
		 messages: {
			blog_cat: {
                required: "Category is required.",
            },
			blog_title: {
                required: "Chapter Name is required.",
            },
			content: {
                required: "History/Description is required.",
            },
			slider_image: {
                required: "Blog Image is required.",
            },
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			var candidate_id = $('#candidate_id').val();
			var trg_arr = [];
			  $('.trg_one').each(function(key,val){
              
			   trg_arr.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_ppt_mark : $(this).find('td').find('.trg_bn_ppt_mark').val(),
					trg_bn_ppt_grade : $(this).find('td').find('.trg_bn_ppt_grade').val(),
					trg_bn_ppt_sign : $(this).find('td').find('.trg_bn_ppt_sign').val(),
				}); 
            });
			
			var trg_2nd = [];
			  $('.trg_2nd').each(function(key,val){
              
			   trg_2nd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_ppt_mark : $(this).find('td').find('.trg_bn_ppt_mark').val(),
					trg_bn_ppt_grade : $(this).find('td').find('.trg_bn_ppt_grade').val(),
					trg_bn_ppt_sign : $(this).find('td').find('.trg_bn_ppt_sign').val(),
				}); 
            });
			
			var trg_3rd = [];
			  $('.trg_3rd').each(function(key,val){
              
			   trg_3rd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_ppt_mark : $(this).find('td').find('.trg_bn_ppt_mark').val(),
					trg_bn_ppt_grade : $(this).find('td').find('.trg_bn_ppt_grade').val(),
					trg_bn_ppt_sign : $(this).find('td').find('.trg_bn_ppt_sign').val(),
				}); 
            });
			
			var trg_bn = [];
			  $('.trg_bn').each(function(key,val){
              
			   trg_bn.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_cdr_date : $(this).find('td').find('.trg_bn_cdr_date').val(),
					trg_bn_cdr_marks : $(this).find('td').find('.trg_bn_cdr_marks').val(),
					trg_bn_cdr_grade : $(this).find('td').find('.trg_bn_cdr_grade').val(),
					trg_bn_cdr_sign : $(this).find('td').find('.trg_bn_cdr_sign').val(),
				}); 
            });
			var trg_bn_2nd = [];
			  $('.trg_bn_2nd').each(function(key,val){
              
			   trg_bn_2nd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_cdr_date : $(this).find('td').find('.trg_bn_cdr_date').val(),
					trg_bn_cdr_marks : $(this).find('td').find('.trg_bn_cdr_marks').val(),
					trg_bn_cdr_grade : $(this).find('td').find('.trg_bn_cdr_grade').val(),
					trg_bn_cdr_sign : $(this).find('td').find('.trg_bn_cdr_sign').val(),
				}); 
            });
			
			var trg_bn_3rd = [];
			  $('.trg_bn_3rd').each(function(key,val){
              
			   trg_bn_3rd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_cdr_date : $(this).find('td').find('.trg_bn_cdr_date').val(),
					trg_bn_cdr_marks : $(this).find('td').find('.trg_bn_cdr_marks').val(),
					trg_bn_cdr_grade : $(this).find('td').find('.trg_bn_cdr_grade').val(),
					trg_bn_cdr_sign : $(this).find('td').find('.trg_bn_cdr_sign').val(),
				}); 
            });
			
			
			var commandent_test_bpet = [];
			  $('.commandent_test_bpet').each(function(key,val){
              
			   commandent_test_bpet.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_bpet_mark : $(this).find('td').find('.commandant_test_bpet_mark').val(),
					commandant_test_bpet_grade : $(this).find('td').find('.commandant_test_bpet_grade').val(),
					commandant_test_bpet_sign : $(this).find('td').find('.commandant_test_bpet_sign').val(),
				}); 
            });
			var commandent_test_bpet_2nd = [];
			  $('.commandent_test_bpet_2nd').each(function(key,val){
              
			   commandent_test_bpet_2nd.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_bpet_mark : $(this).find('td').find('.commandant_test_bpet_mark').val(),
					commandant_test_bpet_grade : $(this).find('td').find('.commandant_test_bpet_grade').val(),
					commandant_test_bpet_sign : $(this).find('td').find('.commandant_test_bpet_sign').val(),
				}); 
            });
			var commandent_test_bpet_3rd = [];
			  $('.commandent_test_bpet_3rd').each(function(key,val){
              
			   commandent_test_bpet_3rd.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_bpet_mark : $(this).find('td').find('.commandant_test_bpet_mark').val(),
					commandant_test_bpet_grade : $(this).find('td').find('.commandant_test_bpet_grade').val(),
					commandant_test_bpet_sign : $(this).find('td').find('.commandant_test_bpet_sign').val(),
				}); 
            });
			
			var test_firing = [];
			  $('.test_firing').each(function(key,val){
              
			   test_firing.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_2nd = [];
			  $('.test_firing_2nd').each(function(key,val){
              
			   test_firing_2nd.push({
				     name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_3rd = [];
			  $('.test_firing_3rd').each(function(key,val){
              
			   test_firing_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			
			
			
			var test_firing_other = [];
			  $('.test_firing_other').each(function(key,val){
              
			   test_firing_other.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_other_2nd = [];
			  $('.test_firing_other_2nd').each(function(key,val){
              
			   test_firing_other_2nd.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_other_3rd = [];
			  $('.test_firing_other_3rd').each(function(key,val){
              
			   test_firing_other_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			
			var basic_course = [];
			  $('.basic_course').each(function(key,val){
              
			   basic_course.push({
				      name_trg : $(this).attr('name_trg'),
					basic_course_date : $(this).find('td').find('.basic_course_date').val(),
					basic_course_mark : $(this).find('td').find('.basic_course_mark').val(),
					basic_course_result : $(this).find('td').find('.basic_course_result').val(),
					basic_course_sign : $(this).find('td').find('.basic_course_sign').val(),
				}); 
            });
			var basic_course_2nd = [];
			  $('.basic_course_2nd').each(function(key,val){
              
			   basic_course_2nd.push({
				      name_trg : $(this).attr('name_trg'),
					basic_course_date : $(this).find('td').find('.basic_course_date').val(),
					basic_course_mark : $(this).find('td').find('.basic_course_mark').val(),
					basic_course_result : $(this).find('td').find('.basic_course_result').val(),
					basic_course_sign : $(this).find('td').find('.basic_course_sign').val(),
				}); 
            });
			var basic_course_3rd = [];
			  $('.basic_course_3rd').each(function(key,val){
              
			   basic_course_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					basic_course_date : $(this).find('td').find('.basic_course_date').val(),
					basic_course_mark : $(this).find('td').find('.basic_course_mark').val(),
					basic_course_result : $(this).find('td').find('.basic_course_result').val(),
					basic_course_sign : $(this).find('td').find('.basic_course_sign').val(),
				}); 
            });
			
			
			var map_reading = [];
			  $('.map_reading').each(function(key,val){
               
			   map_reading.push({
				      name_trg : $(this).attr('name_trg'),
					map_reading_date : $(this).find('td').find('.map_reading_date').val(),
					map_reading_mark : $(this).find('td').find('.map_reading_mark').val(),
					map_reading_result : $(this).find('td').find('.map_reading_result').val(),
					map_reading_sign : $(this).find('td').find('.map_reading_sign').val(),
				}); 
            });
			var map_reading_2nd = [];
			  $('.map_reading_2nd').each(function(key,val){
              
			   map_reading_2nd.push({
				      name_trg : $(this).attr('name_trg'),
					map_reading_date : $(this).find('td').find('.map_reading_date').val(),
					map_reading_mark : $(this).find('td').find('.map_reading_mark').val(),
					map_reading_result : $(this).find('td').find('.map_reading_result').val(),
					map_reading_sign : $(this).find('td').find('.map_reading_sign').val(),
				}); 
            });
			
			var map_reading_3rd = [];
			  $('.map_reading_3rd').each(function(key,val){
              
			   map_reading_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					map_reading_date : $(this).find('td').find('.map_reading_date').val(),
					map_reading_mark : $(this).find('td').find('.map_reading_mark').val(),
					map_reading_result : $(this).find('td').find('.map_reading_result').val(),
					map_reading_sign : $(this).find('td').find('.map_reading_sign').val(),
				}); 
            });
			
			var physical_parameters = [];
			   physical_parameters.push({
			       candidate_height: $('#candidate_height').val(),
                   candidate_permissible_wt: $('#candidate_permissible_wt').val(),
                   candidate_actual_wt: $('#candidate_actual_wt').val(),
                   candidate_over_under_wt: $('#candidate_over_under_wt').val(),
                   candidate_identification_mark_one: $('#candidate_identification_mark_one').val(),
                   candidate_identification_mark_two: $('#candidate_identification_mark_two').val(),
                 });
			
			var instructer_info = [];
			   instructer_info.push({
			       company_commander_rank: $('#company_commander_rank').val(),
                   company_commander_name: $('#company_commander_name').val(),
                   company_commander_form_date: $('#company_commander_form_date').val(),
                   platoon_commander_rank: $('#platoon_commander_rank').val(),
                   platoon_commander_name: $('#platoon_commander_name').val(),
                   platoon_commander_form_date: $('#platoon_commander_form_date').val(),
                   platoon_havaldar_first_rank: $('#platoon_havaldar_first_rank').val(),
                   platoon_havaldar_first_name: $('#platoon_havaldar_first_name').val(),
                   platoon_havaldar_first_form_date: $('#platoon_havaldar_first_form_date').val(),
                   platoon_havaldar_second_rank: $('#platoon_havaldar_second_rank').val(),
                   platoon_havaldar_second_name: $('#platoon_havaldar_second_name').val(),
                   platoon_havaldar_second_form_date: $('#platoon_havaldar_second_form_date').val(),
		        });
			 var edu_info = [];
			   edu_info.push({
			       qualification_civil: $('#qualification_civil').val(),
                   qualification_civil_date: $('#qualification_civil_date').val(),
                   qualification_civil_part_order: $('#qualification_civil_part_order').val(),
                   qualification_mr: $('#qualification_mr').val(),
                   qualification_mr_date: $('#qualification_mr_date').val(),
                   qualification_mr_part_order: $('#qualification_mr_part_order').val(),
                   qualification_aec: $('#qualification_aec').val(),
                   qualification_aec_date: $('#qualification_aec_date').val(),
                   qualification_aec_part_order: $('#qualification_aec_part_order').val(),
                   qualification_ttt: $('#qualification_ttt').val(),
                   qualification_ttt_date: $('#qualification_ttt_date').val(),
                   qualification_ttt_part_order: $('#qualification_ttt_part_order').val(),
                   qualification_attestation: $('#qualification_attestation').val(),
                   qualification_attestation_date: $('#qualification_attestation_date').val(),
                   qualification_attestation_part_order: $('#qualification_attestation_part_order').val(),
                }); 
			
			var family_data = [];
			$('.TextBoxContainer2 .remove-added-para').each(function(key,val){
				 //  console.log($(this).find('#candidate_relation').val());
				  // console.log($(this).find('#candidate_relation_name').val());
				 //  console.log($(this).find('#candidate_relation_age').val());
				family_data.push({
					candidate_relation : $(this).find('#candidate_relation').val(),
					candidate_relation_name : $(this).find('#candidate_relation_name').val(),
					candidate_relation_dob : $(this).find('#candidate_relation_dob').val(),
					candidate_relation_age : $(this).find('#candidate_relation_age').val(),
				});  
			});
			$.post(APP_URL + 'admin/registration/update_candidate', {
                candidate_id: candidate_id,
                trg_arr: trg_arr,
                trg_2nd: trg_2nd,
                trg_3rd: trg_3rd,
                trg_bn: trg_bn,
                trg_bn_2nd: trg_bn_2nd,
                trg_bn_3rd: trg_bn_3rd,
                test_firing: test_firing,
                test_firing_2nd: test_firing_2nd,
                test_firing_3rd: test_firing_3rd,
                test_firing_other: test_firing_other,
                test_firing_other_2nd: test_firing_other_2nd,
                test_firing_other_3rd: test_firing_other_3rd,
                basic_course: basic_course,
                basic_course_2nd: basic_course_2nd,
                basic_course_3rd: basic_course_3rd,
                map_reading: map_reading,
                map_reading_2nd: map_reading_2nd,
                map_reading_3rd: map_reading_3rd,
                commandent_test_bpet: commandent_test_bpet,
                commandent_test_bpet_2nd: commandent_test_bpet_2nd,
                commandent_test_bpet_3rd: commandent_test_bpet_3rd,
                physical_parameters: physical_parameters,
                edu_info: edu_info,
                family_data: family_data,
                instructer_info: instructer_info,
                candidate_army_no: $('#candidate_army_no').val(),
                candidate_course: $('#candidate_course').val(),
                candidate_company: $('#candidate_company').val(),
                candidate_name: $('#candidate_name').val(),
                candidate_section: $('#candidate_section').val(),
                candidate_year: $('#candidate_year').val(),
                candidate_rank: $('#candidate_rank').val(),
                candidate_platoon: $('#candidate_platoon').val(),
                candidate_aro_uhq: $('#candidate_aro_uhq').val(),
                candidate_dob: $('#candidate_dob').val(),
                candidate_doe: $('#candidate_doe').val(),
                candidate_poe: $('#candidate_poe').val(),
                candidate_address: $('#candidate_address').val(),
                candidate_blood_group: $('#candidate_blood_group').val(),
                candidate_religion: $('#candidate_religion').val(),
                candidate_caste: $('#candidate_caste').val(),
                candidate_village: $('#candidate_village').val(),
                candidate_post_office: $('#candidate_post_office').val(),
                candidate_district: $('#candidate_district').val(),
                candidate_state: $('#candidate_state').val(),
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.href = APP_URL+'admin/registration/print_view?id='+candidate_id;
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				
			}, 'json');
		return false;
		},
	});
	
	

	
	
	
});
</script>	
