<link href="<?php echo base_url();?>assets/css/jquery-ui.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>

  <script>
$(function() {
        var date = new Date();
        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: 'c-75:c+75',
        });
		
    });
	
 
 </script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<style>
.error,.required{
	color:red;
}
.form-control{
	padding:0px;
}
.row{
	margin: 0px;
}
.rel_data{
	margin-bottom: 11px;
    margin-left: -22px;
}
.removepara{
	cursor: pointer;
}
.content-wrapper{
	margin-left:0px;
}
.main-footer{
	margin-left:0px;
}
</style>

<div class="content-wrapper animated fadeInRight">
<div class="content"> 
 <div id="headerMsg"> </div>
	<div class="row">
	   <div class="col-lg-12" style="padding: 0px;">
	     <div class="box " style="padding: 11px;">
			<div class="ibox float-e-margins">
				<div class="box-header with-border">
					  <h3 class="box-title"><label style="font-size: 18px;    font-weight: bold;">Registration </label></h3>
				 		   <a style="float: right;" href="<?php echo base_url(); ?>admin/registration/candidate_list"  class="btn btn-default pull-right">Candidate list</a>
				           <a style="float: right;margin-right: 45px;" href="<?php echo base_url(); ?>admin/configure"  class="btn btn-success">Deshboard</a>
						
				</div>
				
				<div class="ibox-content">
				<form id="register_form" method="post" role="form">
				 <h4 class="box-title">Persional information</h4>
					<div class="row">
					 <div class="col-md-1" > <label> Army No </label></div>
					 <div class="col-md-3" ><input type="text" class="form-control"  name="candidate_army_no" id="candidate_army_no" value=""  placeholder="Enter Army No"> </div>
					 <div class="col-md-1" > <label>  Course  </label> </div>
					 <div class="col-md-3" ><input type="text" class="form-control" name="candidate_course" id="candidate_course" value=""  placeholder="Enter Course"> </div>
					  <div class="col-md-1" > <label>  Company  </label> </div>
					 <div class="col-md-3" ><input type="text" class="form-control" name="candidate_company" id="candidate_company" value=""  placeholder="Enter Company"> </div>
					</div>
					</br>
					<div class="row">
					 <div class="col-md-1"> <label> Name </label></div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_name" id="candidate_name" value=""  placeholder="Enter candidate name"> </div>
					 <div class="col-md-1"> <label>  Section  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_section" id="candidate_section" value=""  placeholder="Enter candidate section"> </div>
					  <div class="col-md-1"> <label>  Year  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_year" id="candidate_year" value=""  placeholder="Enter candidate year"> </div>
					</div>
					</br>
						<div class="row">
					 <div class="col-md-1"> <label> Rank </label></div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_rank" id="candidate_rank" value="" placeholder="Enter candidate rank"> </div>
					 <div class="col-md-1"> <label>  Platoon  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_platoon" id="candidate_platoon" value="" placeholder="Enter candidate platoon"> </div>
					  <div class="col-md-1"> <label>  ARO/UHQ  </label> </div>
					 <div class="col-md-3"><input type="text" class="form-control" name="candidate_aro_uhq" id="candidate_aro_uhq" value=""  placeholder="Enter ARO/UHQ "> </div>
					</div>
					</br>
					<div class="row">
					<div class="col-md-6"> 
					<div class="row" style="margin-left: -31px;"> 
					<h4 style="padding-left: 20px;">Bio Data </h4>
					<div class="col-md-6"> 
					
					<table class="table table-hover" style="width:100%" >
						  <tr  class="table-active">
							<th><label>DOB:</label></th>
							<td><input type="text" autocomplete="off" class="form-control date" name="candidate_dob" value="" id="candidate_dob"  placeholder="Enter DOB"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>DOE:</label></th>
							<td><input type="text" autocomplete="off" class="form-control date" name="candidate_doe" value="" id="candidate_doe"  placeholder="Enter DOE"> </td>
						  </tr>
						 <tr  class="table-active">
							<th><label>POE:</label></th>
							<td><input type="text" autocomplete="off" class="form-control date" name="candidate_poe" value="" id="candidate_poe"  placeholder="Enter POE"> </td>
						  </tr>
						   <tr  class="table-active">
							<th><label>ADDRESS:</label></th>
							<td><input type="text" class="form-control" id="candidate_address" name="candidate_address" value=""  placeholder="Enter ADDRESS"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>BLOOD GROUP:</label></th>
							<td><input type="text" class="form-control" name="candidate_blood_group" value="" id="candidate_blood_group"  placeholder="Enter BLOOD GROUP"> </td>
						  </tr>
						</table>
						
					</div>
					<div class="col-md-6" style="padding:0px"> 
					<table class="table table-hover" style="width:100%">
					  <tr  class="table-active">
							<th><label>Religion:</label></th>
							<td><input type="text" class="form-control" name="candidate_religion" value="" id="candidate_religion"  placeholder="Enter Religion"> </td>
						  </tr>
					<tr  class="table-active">
							<th><label>Caste:</label></th>
							<td><input type="text" class="form-control" name="candidate_caste" value="" id="candidate_caste"  placeholder="Enter Caste"> </td>
						  </tr>
					  <tr  class="table-active">
							<th><label>Village:</label></th>
							<td><input type="text" class="form-control" name="candidate_village" value="" id="candidate_village"  placeholder="Enter Village"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>Post office:<label></th>
							<td><input type="text" class="form-control" name="candidate_post_office" value="" id="candidate_post_office"  placeholder="Enter Post office"> </td>
						  </tr>
						  <tr  class="table-active">
							<th><label>District:</label></th>
							<td><input type="text" class="form-control" name="candidate_district" value="" id="candidate_district"  placeholder="Enter District"> </td>
						  </tr>
						   <tr  class="table-active">
							<th><label>State:</label></th>
							<td><input type="text" class="form-control" id="candidate_state" value="" name="candidate_state"  placeholder="Enter State"> </td>
						  </tr>
					</table>
					</div>
					
					</div>
					<div class="row">
					<h4> Faimly details </h4>
					<hr>
					<div class="row rel_data" >
						   <div class="col-md-3"><label>Relation</label></div>
						   <div class="col-md-4"><label>Name</label></div>
						   <div class="col-md-3"><label>DOB</label></div>
						   <div class="col-md-2"><label>Age</label></div>
					</div> 
					<div class="TextBoxContainer2">	
					<div class="row rel_data remove-added-para" style="margin-bottom: 11px;">
						   <div class="col-md-3"><input style="width: 100%;" class="candidate_relation" type="text"  id="candidate_relation_father" name="candidate_relation_father"  placeholder="Relation" value="Father" required></div>
						   <div class="col-md-4"><input type="text" class="candidate_relation_name" id="candidate_relation_name_father" name="candidate_relation_name_father"  placeholder="Name" required></div>
						   <div class="col-md-3" style="padding: 0px;"><input style="width: 100%;" type="date" class="candidate_relation_dob" id="candidate_relation_dob_father"  name="candidate_relation_dob_father" required placeholder="DOB"></div>
						   <div class="col-md-2"><input type="text" class="candidate_relation_age" id="candidate_relation_age_father" name="candidate_relation_age_father"  placeholder="Age" style="width: 100%;" data-msg="Enter age" required></div>
					</div>  
					<div class="row rel_data remove-added-para" style="margin-bottom: 11px;">
							   <div class="col-md-3"><input style="width: 100%;" type="text" class="candidate_relation" id="candidate_relation_mother" name="candidate_relation_mother"  placeholder="Relation" required value="Mother"></div>
						   <div class="col-md-4"><input type="text" class="candidate_relation_name" id="candidate_relation_name_mother" name="candidate_relation_name_mother"  placeholder="Name" required></div>
						   <div class="col-md-3" style="padding: 0px;"><input style="width: 100%;" type="date" class="candidate_relation_dob" id="candidate_relation_dob_mother"  name="candidate_relation_dob_mother" required placeholder="DOB"></div>
						   <div class="col-md-2"><input type="text" class="candidate_relation_age" id="candidate_relation_age_mother" name="candidate_relation_age_mother"  placeholder="Age" style="width: 100%;" data-msg="Enter age" required></div>
					</div>
                    <div class="row rel_data remove-added-para" style="margin-bottom: 11px;">					
							   <div class="col-md-3"><input style="width: 100%;" type="text" class="candidate_relation" id="candidate_relation_brother" name="candidate_relation_brother"  placeholder="Relation" value="Brother" required></div>
						   <div class="col-md-4"><input type="text" class="candidate_relation_name" id="candidate_relation_name_brother" name="candidate_relation_name_brother"  placeholder="Name" required></div>
						   <div class="col-md-3" style="padding: 0px;"><input style="width: 100%;" type="date" class="candidate_relation_dob" id="candidate_relation_dob_brother"  name="candidate_relation_dob_brother" required placeholder="DOB"></div>
						   <div class="col-md-2"><input type="text" class="candidate_relation_age" id="candidate_relation_age_brother" name="candidate_relation_age_brother" placeholder="Age"  style="width: 100%;" data-msg="Enter age" required></div>
					</div>
                       <div class="row rel_data remove-added-para" style="margin-bottom: 11px;">					
							   <div class="col-md-3"><input style="width: 100%;" type="text" class="candidate_relation" id="candidate_relation_sister" name="candidate_relation_sister"  placeholder="Relation" required value="Sister"></div>
						   <div class="col-md-4"><input type="text" class="candidate_relation_name" id="candidate_relation_name_sister" name="candidate_relation_name_sister"  placeholder="Name" required></div>
						   <div class="col-md-3" style="padding: 0px;"><input style="width: 100%;" type="date" class="candidate_relation_dob" id="candidate_relation_dob_sister"  name="candidate_relation_dob_sister" required placeholder="DOB"></div>
						   <div class="col-md-2"><input type="text" class="candidate_relation_age" id="candidate_relation_age_sister" name="candidate_relation_age_sister"  placeholder="Age" style="width: 100%;" data-msg="Enter age" required></div>
					</div>
										
						  
						  </div>
                         <div class="col-md-12" style="margin-left: -22px;">
						  <input id="addProSpeci" class="btn btn-success" type="button" value="&#43" />
					    </div> 						
						</div>
					</div>
						<div class="col-md-6">
						<h4> Instructer</h4>
						<hr>
						 <div class="row">
						  <table class="table" style="width:100%">
							  <tr>
								<th>APPT</th>
								<th>Rank</th>
								<th>Name</th>
								<th>From Date</th>
							  </tr>
							  <tr>
								<td><label>Company commander</label></td>
								<td><input class="form-control"  type="text" value="" id="company_commander_rank" name="company_commander_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="" id="company_commander_name" name="company_commander_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="" id="company_commander_form_date" name="company_commander_form_date" placeholder="From Date"></td>
							  </tr>
							   <tr>
								<td><label>Platoon commander</label></td>
								<td><input class="form-control"  type="text" value="" id="platoon_commander_rank" name="platoon_commander_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="" id="platoon_commander_name" name="platoon_commander_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="" id="platoon_commander_form_date" name="platoon_commander_form_date" placeholder="From Date"></td>
							  </tr>
							   <tr>
								<td><label>Platoon havaldar 1</label></td>
								<td><input class="form-control"  type="text" value="" id="platoon_havaldar_first_rank" name="platoon_havaldar_first_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="" id="platoon_havaldar_first_name" name="platoon_havaldar_first_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="" id="platoon_havaldar_first_form_date" name="platoon_havaldar_first_form_date" placeholder="From Date"></td>
							  </tr>
							   <tr>
								<td><label>Platoon havaldar 2</label></td>
								<td><input class="form-control"  type="text" value="" id="platoon_havaldar_second_rank" name="platoon_havaldar_second_rank" placeholder="Rank"></td>
								<td><input class="form-control"  type="text" value="" id="platoon_havaldar_second_name" name="platoon_havaldar_second_name" placeholder="Name"></td>
								<td><input class="form-control date"  type="text" value="" id="platoon_havaldar_second_form_date" name="platoon_havaldar_second_form_date" placeholder="From Date"></td>
							  </tr>
							</table>
						 </div>
						 <div class="row">
						 <h4> Education qualification</h4>
						<hr>
					<div class="col-md-12" style="margin-bottom: 11px;">
						   <div class="col-md-2"><label>Civil</label></div>
						   <div class="col-md-4"><input class="form-control"  type="text" value="" id="qualification_civil" name="qualification_civil" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date" data-msg="Enter date" required  type="text" value="" name="qualification_civil_date" id="qualification_civil_date"  placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" data-msg="Enter order" required type="text" id="qualification_civil_part_order"  name="qualification_civil_part_order" value="" placeholder="Part-II order" style="width: 100%;"></div>
					</div>  
					<div class="col-md-12" style="margin-bottom: 11px;">
						   <div class="col-md-2"><label>MR</label></div>
						   <div class="col-md-4"><input class="form-control" type="text" value="" id="qualification_mr" name="qualification_mr" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date" type="text" data-msg="Enter date" required value=""  id="qualification_mr_date" name="qualification_mr_date" placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" type="text" data-msg="Enter order" required   id="qualification_mr_part_order" name="qualification_mr_part_order" value="" placeholder="Part-II order" style="width: 100%;"></div>
					</div>
                    <div class="col-md-12" style="margin-bottom: 11px;">					
						   <div class="col-md-2"><label>AEC</label></div>
						   <div class="col-md-4"><input class="form-control" type="text" value="" name="qualification_aec" id="qualification_aec" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date" type="text" data-msg="Enter date" required value="" id="qualification_aec_date" name="qualification_aec_date" placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" type="text" data-msg="Enter order" required  name="qualification_aec_part_order" id="qualification_aec_part_order" value="" placeholder="Part-II order" style="width: 100%;"></div>
						</div>
                       <div class="col-md-12" style="margin-bottom: 11px;">					
						   <div class="col-md-2"><label>TTT</label></div>
						   <div class="col-md-4"><input class="form-control" type="text" value="" name="qualification_ttt" id="qualification_ttt" placeholder="qualification"></div>
						   <div class="col-md-3"><input class="form-control date" data-msg="Enter date" type="text" value="" name="qualification_ttt_date" id="qualification_ttt_date" placeholder="date"></div>
						   <div class="col-md-3"><input class="form-control" type="text"  data-msg="Enter order" required  name="qualification_ttt_part_order" id="qualification_ttt_part_order" value="" placeholder="Part-II order" style="width: 100%;"></div>
						</div>	
                        <div class="col-md-12">					
						   <div class="col-md-2"><label>Attestation</label></div>
						   <div class="col-md-4"><input  class="form-control" type="text" value="" name="qualification_attestation" id="qualification_attestation"  placeholder="qualification"></div>
						   <div class="col-md-3"><input   class="form-control date" data-msg="Enter date" type="text" value="" name="qualification_attestation_date" id="qualification_attestation_date" placeholder="date"></div>
						   <div class="col-md-3"><input  class="form-control" type="text" data-msg="Enter order" required  name="qualification_attestation_part_order" id="qualification_attestation_part_order" value="" placeholder="Part-II order" style="width: 100%;"></div>
						</div>						
						</div>
						 </div>
					</div>
					</br>
					<hr>
					<div class="row">
                    <h4 class="box-title" style="margin-left: 11px;"> Physical Parameters </h4>					
					<div class="col-md-6">
					<table style="width:100%" class="table">
						  <tr>
							<th><label>Height:</label></th>
							<td><input type="text" class="form-control" name="candidate_height" id="candidate_height" placeholder="Height" value=""></td>
						  </tr>
						  <tr>
							<th><label>Permissible Wt:</label></th>
							<td><input type="text" class="form-control" required name="candidate_permissible_wt" id="candidate_permissible_wt" placeholder="Permissible Wt" value=""></td>
						  </tr>
						  <tr>
							<th><label>Actual Wt:</label></th>
							<td><input type="text" class="form-control" name="candidate_actual_wt" id="candidate_actual_wt" placeholder="Actual Wt" value=""></td>
						  </tr>
						  <tr>
							<th><label>Over Wt / Under Wt:</label></th>
							<td><input type="text" class="form-control" id="candidate_over_under_wt" name="candidate_over_under_wt" placeholder="Over Wt / Under Wt"></td>
						  </tr>
					</table>
					</div>
					<div class="col-md-6">
					  <table style="width:100%" class="table">
						  <tr>
							<th><label>Identification Mark - 1:</label></th>
							<td><input type="text" class="form-control" id="candidate_identification_mark_one" name="candidate_identification_mark_one" placeholder="Identification Mark - 1" value="" ></td>
						  </tr>
						  <tr>
							<th><label>Identification Mark - 2:</label></th>
							<td><input type="text" class="form-control"  name="candidate_identification_mark_two" id="candidate_identification_mark_two" placeholder="Identification Mark - 2" value=""></td>
						  </tr>
						 
					</table>
					</div>
					</div>
					<hr>
					<div class="row">
					<h4 style="margin-left: 11px;"> TRG BN CDR PPT TEST </h4>
					  <div class="col-md-6" style="padding:0px">
					  <h5> 1st Attempt </h5>
                       <table class="table " style="width:100%; border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="trg_one" name_trg="2,4 KM Run">
									<td ><label >2,4 KM Run</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_2_4_km_mark" name="trg_bn_ppt_2_4_km_mark"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_2_4_km_grade" name="trg_bn_ppt_2_4_km_grade" required ></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_2_4_km_sign" name="trg_bn_ppt_2_4_km_sign" required ></td>
								  </tr>
								   <tr  class="trg_one" name_trg="Bent Knee Sit ups">
									<td><label>Bent Knee Sit ups</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value=""  id="trg_bn_ppt_bent_knee_sit_up_mark" name="trg_bn_ppt_bent_knee_sit_up_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_bent_knee_sit_up_grade" name="trg_bn_ppt_bent_knee_sit_up_grade" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_bent_knee_sit_up_sign" name="trg_bn_ppt_bent_knee_sit_up_sign" required></td>
								  </tr>
								   <tr  class="trg_one" name_trg="5 M Shuttle">
									<td><label>5 M Shuttle</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_5m_shuttle_mark" name="trg_bn_ppt_5m_shuttle_mark"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_5m_shuttle_grade" name="trg_bn_ppt_5m_shuttle_grade"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_5m_shuttle_sign" name="trg_bn_ppt_5m_shuttle_sign" required></td>
								  </tr >
								   <tr  class="trg_one" name_trg="Toe Touch">
									<td><label>Toe Touch</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value=""  id="trg_bn_ppt_toe_touch_mark" name="trg_bn_ppt_toe_touch_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value=""  id="trg_bn_ppt_toe_touch_grade" name="trg_bn_ppt_toe_touch_grade" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value=""  id="trg_bn_ppt_toe_touch_sign" name="trg_bn_ppt_toe_touch_sign" required></td>
								  </tr>
								   <tr  class="trg_one" name_trg="100 M Sprint">
									<td><label>100 M Sprint</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_100m_sprint_mark" name="trg_bn_ppt_100m_sprint_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_100m_sprint_grade" name="trg_bn_ppt_100m_sprint_grade" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_100m_sprint_sign" name="trg_bn_ppt_100m_sprint_sign" required ></td>
								  </tr>
								   <tr  class="trg_one" name_trg="Chin up">
									<td><label>Chin up</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_chinup_mark"  name="trg_bn_ppt_chinup_mark" required></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_chinup_grade" name="trg_bn_ppt_chinup_grade"  required></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_chinup_sign" name="trg_bn_ppt_chinup_sign" required ></td>
								  </tr>
								  <tr  >
									<td><label>Over All</label></td>
									<td><input type="text" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_overallmark" name="trg_bn_ppt_overallmark" ></td>
									<td><input type="text" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_overallgrade" name="trg_bn_ppt_overallgrade" ></td>
									<td><input type="text" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_overallsign" name="trg_bn_ppt_overallsign"  ></td>
								  </tr>
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="trg_2nd" name_trg="2,4 KM Run">
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_2_4_km_mark_2nd" name="trg_bn_ppt_2_4_km_mark_2nd"  required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_2_4_km_grade_2nd" name="trg_bn_ppt_2_4_km_grade_2nd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_2_4_km_sign_2nd" name="trg_bn_ppt_2_4_km_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="Bent Knee Sit ups">
									<td><input type="text"  data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value=""  id="trg_bn_ppt_bent_knee_sit_up_mark_2nd" name="trg_bn_ppt_bent_knee_sit_up_mark_2nd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_bent_knee_sit_up_grade_2nd" name="trg_bn_ppt_bent_knee_sit_up_grade_2nd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_bent_knee_sit_up_sign_2nd" name="trg_bn_ppt_bent_knee_sit_up_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="5 M Shuttle">
									<td><input type="text"  data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_5m_shuttle_mark_2nd" name="trg_bn_ppt_5m_shuttle_mark_2nd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_5m_shuttle_grade_2nd"  name="trg_bn_ppt_5m_shuttle_grade_2nd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_5m_shuttle_sign_2nd" name="trg_bn_ppt_5m_shuttle_sign_2nd" required></td>
								  </tr >
								   <tr  class="trg_2nd" name_trg="Toe Touch">
									<td><input type="text"  data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value=""  id="trg_bn_ppt_toe_touch_mark_2nd" name="trg_bn_ppt_toe_touch_mark_2nd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value=""  id="trg_bn_ppt_toe_touch_grade_2nd" name="trg_bn_ppt_toe_touch_grade_2nd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value=""  id="trg_bn_ppt_toe_touch_sign_2nd" name="trg_bn_ppt_toe_touch_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="100 M Sprint">
									<td><input type="text"  data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_100m_sprint_mark_2nd" name="trg_bn_ppt_100m_sprint_mark_2nd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_100m_sprint_grade_2nd" name="trg_bn_ppt_100m_sprint_grade_2nd" required></td>
									<td><input type="text"  data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_100m_sprint_sign_2nd" name="trg_bn_ppt_100m_sprint_sign_2nd" required></td>
								  </tr>
								   <tr  class="trg_2nd" name_trg="Chin up">
									<td><input type="text"  data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_chinup_mark_2nd"  name="trg_bn_ppt_chinup_mark_2nd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_chinup_grade_2nd" name="trg_bn_ppt_chinup_grade_2nd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_chinup_sign_2nd" name="trg_bn_ppt_chinup_sign_2nd" required></td>
								  </tr>
								  <tr  >
									<td><input type="text"  data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_mark"  ></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_grade" ></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_sign" ></td>
								  </tr>
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								  <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="trg_3rd" name_trg="2,4 KM Run">
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_2_4_km_mark_3rd"  name="trg_bn_ppt_2_4_km_mark_3rd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_2_4_km_grade_3rd" name="trg_bn_ppt_2_4_km_grade_3rd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_2_4_km_sign_3rd" name="trg_bn_ppt_2_4_km_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="Bent Knee Sit ups">
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value=""  id="trg_bn_ppt_bent_knee_sit_up_mark_3rd" name="trg_bn_ppt_bent_knee_sit_up_mark_3rd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_bent_knee_sit_up_grade_3rd" name="trg_bn_ppt_bent_knee_sit_up_grade_3rd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_bent_knee_sit_up_sign_3rd" name="trg_bn_ppt_bent_knee_sit_up_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="5 M Shuttle">
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_5m_shuttle_mark_3rd"  name="trg_bn_ppt_5m_shuttle_mark_3rd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_5m_shuttle_grade_3rd"  name="trg_bn_ppt_5m_shuttle_grade_3rd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_5m_shuttle_sign_3rd" name="trg_bn_ppt_5m_shuttle_sign_3rd" required></td>
								  </tr >
								   <tr  class="trg_3rd" name_trg="Toe Touch">
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value=""  id="trg_bn_ppt_toe_touch_mark_3rd" name="trg_bn_ppt_toe_touch_mark_3rd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value=""  id="trg_bn_ppt_toe_touch_grade_3rd" name="trg_bn_ppt_toe_touch_grade_3rd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value=""  id="trg_bn_ppt_toe_touch_sign_3rd" name="trg_bn_ppt_toe_touch_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="100 M Sprint">
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_100m_sprint_mark_3rd" name="trg_bn_ppt_100m_sprint_mark_3rd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_100m_sprint_grade_3rd" name="trg_bn_ppt_100m_sprint_grade_3rd" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_100m_sprint_sign_3rd" name="trg_bn_ppt_100m_sprint_sign_3rd" required></td>
								  </tr>
								   <tr  class="trg_3rd" name_trg="Chin up">
									<td><input type="text" data-msg="Enter marks"  class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_chinup_mark_3rd"  name="trg_bn_ppt_chinup_mark_3rd" required></td>
									<td><input type="text"  data-msg="Enter grade" class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_chinup_grade_3rd"  name="trg_bn_ppt_chinup_grade_3rd"  required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_chinup_sign_3rd" name="trg_bn_ppt_chinup_sign_3rd" required></td>
								  </tr>
								  <tr  >
									<td><input type="text"  class="form-control trg_bn_ppt_mark"  value="" id="trg_bn_ppt_mark"  ></td>
									<td><input type="text"   class="form-control trg_bn_ppt_grade"  value="" id="trg_bn_ppt_grade" ></td>
									<td><input type="text"  class="form-control trg_bn_ppt_sign"  value="" id="trg_bn_ppt_sign" ></td>
								  </tr>
								  
								</table>
					  </div>
					</div>
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> TRG BN CDR TEST </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								 
								  <tr class="trg_bn" name_trg="Drill">
									<td><label>Drill</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_drilldate" name="trg_bn_cdr_drilldate" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_drillmarks"  name="trg_bn_cdr_drillmarks" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_drillgrade" name="trg_bn_cdr_drillgrade" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_drillsign" name="trg_bn_cdr_drillsign" value="" required></td>
								  </tr >
								   <tr class="trg_bn" name_trg="Firing Day">
									<td><label>Firing Day</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_firingday_date" name="trg_bn_cdr_firingday_date" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_firingdaymarks" name="trg_bn_cdr_firingdaymarks" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_firingdaygrade"  name="trg_bn_cdr_firingdaygrade" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_firingdaysign" name="trg_bn_cdr_firingdaysign" value="" required></td>
								  </tr>
								   <tr class="trg_bn" name_trg="Firing Night">
									<td><label>Firing Night</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_firingnightdate" name="trg_bn_cdr_firingnightdate" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_firingnightmarks" name="trg_bn_cdr_firingnightmarks" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_firingnightgrade" name="trg_bn_cdr_firingnightgrade" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_firingnightsign" name="trg_bn_cdr_firingnightsign" value="" required></td>
								  </tr>
								   <tr class="trg_bn" name_trg="FC & BC">
									<td><label>FC & BC</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_fc_bcdate" name="trg_bn_cdr_fc_bcdate" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_fc_bcmarks" name="trg_bn_cdr_fc_bcmarks" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_fc_bcgrade" name="trg_bn_cdr_fc_bcgrade" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_fc_bcsign" name="trg_bn_cdr_fc_bcsign" value="" required></td>
								  </tr>
								   <tr class="trg_bn" name_trg="BYT">
									<td><label>BYT</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_bytdate" name="trg_bn_cdr_bytdate" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_bytmarks" name="trg_bn_cdr_bytmarks" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_bytgrade" name="trg_bn_cdr_bytgrade" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_bytsign" name="trg_bn_cdr_bytsign" value="" required></td>
								  </tr>
								   <tr class="trg_bn" name_trg="WT(tsoet)">
									<td><label>WT(tsoet)</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_wt_tsoetdate" name="trg_bn_cdr_wt_tsoetdate" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_wt_tsoetmarks" name="trg_bn_cdr_wt_tsoetmarks" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_wt_tsoetgrade" name="trg_bn_cdr_wt_tsoetgrade" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_wt_tsoetsign" name="trg_bn_cdr_wt_tsoetsign" value="" required></td>
								  </tr>
								  <tr class="trg_bn" name_trg="Written">
									<td><label>Written</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_writtendate" name="trg_bn_cdr_writtendate" value=""  required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_writtenmarks" name="trg_bn_cdr_writtenmarks" value=""  required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_writtengrade" name="trg_bn_cdr_writtengrade" value=""  required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_writtensign" name="trg_bn_cdr_writtensign" value=""  required></td>
								  </tr>
								   <tr class="trg_bn" name_trg="Swimming">
									<td><label>Swimming</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_swimmingdate" name="trg_bn_cdr_swimmingdate" value=""  required ></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks"  id="trg_bn_cdr_swimmingmarks" name="trg_bn_cdr_swimmingmarks" value=""   required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade"  id="trg_bn_cdr_swimminggrade" name="trg_bn_cdr_swimminggrade" value="" required  ></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign"  id="trg_bn_cdr_swimmingsign" name="trg_bn_cdr_swimmingsign" value=""  required ></td>
								  </tr>
								   <tr class="trg_bn" name_trg="OT">
									<td><label>OT</label></td>
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_otdate" name="trg_bn_cdr_otdate" value=""required ></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_otmarks" name="trg_bn_cdr_otmarks" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_otgrade" name="trg_bn_cdr_otgrade" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_otsign" name="trg_bn_cdr_otsign" value="" required></td>
								  </tr>
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
								  
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="trg_bn_2nd" name_trg="Drill">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_drilldate_2nd" name="trg_bn_cdr_drilldate_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_drillmarks_2nd" name="trg_bn_cdr_drillmarks_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_drillgrade_2nd" name="trg_bn_cdr_drillgrade_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_drillsign_2nd" name="trg_bn_cdr_drillsign_2nd" value="" required></td>
								  </tr >
								   <tr class="trg_bn_2nd" name_trg="Firing Day">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_firingdaydate_2nd" name="trg_bn_cdr_firingdaydate_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_firingdaymarks_2nd"  name="trg_bn_cdr_firingdaymarks_2nd" value=""required ></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_firingdaygrade_2nd" name="trg_bn_cdr_firingdaygrade_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_firingdaysign_2nd" name="trg_bn_cdr_firingdaysign_2nd" value="" required></td>
								  </tr>
								   <tr class="trg_bn_2nd" name_trg="Firing Night">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_firingnightdate_2nd" name="trg_bn_cdr_firingnightdate_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_firingnightmarks_2nd"  name="trg_bn_cdr_firingnightmarks_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_firingnightgrade_2nd" name="trg_bn_cdr_firingnightgrade_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_firingnightsign_2nd" name="trg_bn_cdr_firingnightsign_2nd" value="" required></td>
								  </tr>
								   <tr class="trg_bn_2nd" name_trg="FC & BC">
									<td style="padding-top: 19px;"><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_fc_bcdate_2nd" name="trg_bn_cdr_fc_bcdate_2nd" value="" required></td>
									<td style="padding-top: 19px;"><input type="text"  data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_fc_bcmarks_2nd" name="trg_bn_cdr_fc_bcmarks_2nd" value="" required></td>
									<td style="padding-top: 19px;"><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_fc_bcgrade_2nd" name="trg_bn_cdr_fc_bcgrade_2nd" value="" required></td>
									<td style="padding-top: 19px;"><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_fc_bcsign_2nd" name="trg_bn_cdr_fc_bcsign_2nd" value="" required></td>
								  </tr>
								   <tr class="trg_bn_2nd" name_trg="BYT">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_bytdate_2nd" name="trg_bn_cdr_bytdate_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_bytmarks_2nd"  name="trg_bn_cdr_bytmarks_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_bytgrade_2nd" name="trg_bn_cdr_bytgrade_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_bytsign_2nd" name="trg_bn_cdr_bytsign_2nd" value="" required></td>
								  </tr>
								   <tr class="trg_bn_2nd" name_trg="WT(tsoet)">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_wt_tsoetdate_2nd" name="trg_bn_cdr_wt_tsoetdate_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_wt_tsoetmarks_2nd" name="trg_bn_cdr_wt_tsoetmarks_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_wt_tsoetgrade_2nd" name="trg_bn_cdr_wt_tsoetgrade_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_wt_tsoetsign_2nd" name="trg_bn_cdr_wt_tsoetsign_2nd" value="" required></td>
								  </tr>
								  <tr class="trg_bn_2nd" name_trg="Written">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_writtendate_2nd" name="trg_bn_cdr_writtendate_2nd" value=""  required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_writtenmarks_2nd" name="trg_bn_cdr_writtenmarks_2nd" value=""  required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_writtengrade_2nd" name="trg_bn_cdr_writtengrade_2nd" value=""  required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_writtensign_2nd"  name="trg_bn_cdr_writtensign_2nd"value="" required ></td>
								  </tr>
								   <tr class="trg_bn_2nd" name_trg="Swimming">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_swimmingdate_2nd" name="trg_bn_cdr_swimmingdate_2nd" value=""  required ></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks"  id="trg_bn_cdr_swimmingmarks_2nd"  name="trg_bn_cdr_swimmingmarks_2nd" value=""  required ></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade"  id="trg_bn_cdr_swimminggrade_2nd"   name="trg_bn_cdr_swimminggrade_2nd" value=""  required ></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign"  id="trg_bn_cdr_swimmingsign_2nd" name="trg_bn_cdr_swimmingsign_2nd" value=""  required ></td>
								  </tr>
								   <tr class="trg_bn_2nd" name_trg="OT">
									<td><input type="text" data-msg="Enter date" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_otdate_2nd" name="trg_bn_cdr_otdate_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter marks" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_otmarks_2nd"  name="trg_bn_cdr_otmarks_2nd"value="" required></td>
									<td><input type="text" data-msg="Enter grade" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_otgrade_2nd"  name="trg_bn_cdr_otgrade_2nd" value="" required></td>
									<td><input type="text" data-msg="Enter sign" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_otsign_2nd"  name="trg_bn_cdr_otsign_2nd"value="" required></td>
								  </tr>
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								  <tr>
								    <th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								 <tr class="trg_bn_3rd" name_trg="Drill">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_drilldate_3rd" name="trg_bn_cdr_drilldate_3rd"  value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_drillmarks_3rd" name="trg_bn_cdr_drillmarks_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_drillgrade_3rd" name="trg_bn_cdr_drillgrade_3rd" value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_drillsign_3rd" name="trg_bn_cdr_drillsign_3rd" value="" required></td>
								  </tr >
								   <tr class="trg_bn_3rd" name_trg="Firing Day">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_firingdaydate_3rd" name="trg_bn_cdr_firingdaydate_3rd"  value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_firingdaymarks_3rd" name="trg_bn_cdr_firingdaymarks_3rd" value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_firingdaygrade_3rd"  name="trg_bn_cdr_firingdaygrade_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_firingdaysign_3rd"  name="trg_bn_cdr_firingdaysign_3rd" value="" required></td>
								  </tr>
								   <tr class="trg_bn_3rd" name_trg="Firing Night">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_firingnightdate_3rd"  name="trg_bn_cdr_firingnightdate_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_firingnightmarks_3rd" name="trg_bn_cdr_firingnightmarks_3rd" value="" required></td>
									<td><input type="text"  data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_firingnightgrade_3rd"  name="trg_bn_cdr_firingnightgrade_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_firingnightsign_3rd"  name="trg_bn_cdr_firingnightsign_3rd"value="" required></td>
								  </tr>
								   <tr class="trg_bn_3rd" name_trg="FC & BC">
									<td style="padding-top: 19px;"><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_fc_bcdate_3rd" name="trg_bn_cdr_fc_bcdate_3rd" value="" required></td>
									<td style="padding-top: 19px;"><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_fc_bcmarks_3rd" name="trg_bn_cdr_fc_bcmarks_3rd" value="" required></td>
									<td style="padding-top: 19px;"><input type="text" data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_fc_bcgrade_3rd"  name="trg_bn_cdr_fc_bcgrade_3rd"value="" required></td>
									<td style="padding-top: 19px;"><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_fc_bcsign_3rd"  name="trg_bn_cdr_fc_bcsign_3rd"value="" required></td>
								  </tr>
								   <tr class="trg_bn_3rd" name_trg="BYT">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_bytdate_3rd"  name="trg_bn_cdr_bytdate_3rd" value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_bytmarks_3rd" name="trg_bn_cdr_bytmarks_3rd" value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_bytgrade_3rd"  name="trg_bn_cdr_bytgrade_3rd" value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_bytsign_3rd"  name="trg_bn_cdr_bytsign_3rd" value="" required></td>
								  </tr>
								   <tr class="trg_bn_3rd" name_trg="WT(tsoet)">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_wt_tsoetdate_3rd" name="trg_bn_cdr_wt_tsoetdate_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_wt_tsoetmarks_3rd" name="trg_bn_cdr_wt_tsoetmarks_3rd"value="" required></td>
									<td><input type="text"  data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_wt_tsoetgrade_3rd" name="trg_bn_cdr_wt_tsoetgrade_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_wt_tsoetsign_3rd"  name="trg_bn_cdr_wt_tsoetsign_3rd"value="" required></td>
								  </tr>
								  <tr class="trg_bn_3rd" name_trg="Written">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_writtendate_3rd" name="trg_bn_cdr_writtendate_3rd" value=""  required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_writtenmarks_3rd" name="trg_bn_cdr_writtenmarks_3rd" value=""  required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_writtengrade_3rd"  name="trg_bn_cdr_writtengrade_3rd"value=""  required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_writtensign_3rd"  name="trg_bn_cdr_writtensign_3rd"value=""  required></td>
								  </tr>
								   <tr class="trg_bn_3rd" name_trg="Swimming">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date"  id="trg_bn_cdr_swimmingdate_3rd"  name="trg_bn_cdr_swimmingdate_3rd"value=""   required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks"  id="trg_bn_cdr_swimmingmarks_3rd"  name="trg_bn_cdr_swimmingmarks_3rd" value=""  required ></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_grade"  id="trg_bn_cdr_swimminggrade_3rd"  name="trg_bn_cdr_swimminggrade_3rd"value=""  required ></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign"  id="trg_bn_cdr_swimmingsign_3rd"  name="trg_bn_cdr_swimmingsign_3rd"value=""  required ></td>
								  </tr>
								   <tr class="trg_bn_3rd" name_trg="OT">
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_date date" id="trg_bn_cdr_otdate_3rd"  name="trg_bn_cdr_otdate_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_marks" id="trg_bn_cdr_otmarks_3rd"  name="trg_bn_cdr_otmarks_3rd"value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_grade" id="trg_bn_cdr_otgrade_3rd" name="trg_bn_cdr_otgrade_3rd" value="" required></td>
									<td><input type="text" data-msg="Required" class="form-control trg_bn_cdr_sign" id="trg_bn_cdr_otsign_3rd"  name="trg_bn_cdr_otsign_3rd" value="" required></td>
								  </tr>
								  
								</table>
					  </div>
					</div>
					
					<div class="row" style="margin-left: 1px;">
					<h4 class="box-title"> COMMANDANT TEST BPET </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="commandent_test_bpet" name_trg="5km run">
									<td><label>5km run</label></td>
									<td><input type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_5km_mark" name="commandant_test_bpet_5km_mark" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_5km_grade" name="commandant_test_bpet_5km_grade" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_5km_sign" name="commandant_test_bpet_5km_sign" required></td>
								 </tr>
								   <tr class="commandent_test_bpet" name_trg="H/rope">
									<td><label>H/rope</label></td>
									<td><input type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_hrope_mark" name="commandant_test_bpet_hrope_mark" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_hrope_grade"  name="commandant_test_bpet_hrope_grade" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_hrope_sign" name="commandant_test_bpet_hrope_sign" required></td>
								 </tr>
								   <tr class="commandent_test_bpet" name_trg="V/rope">
									<td><label>V/rope</label></td>
									<td><input type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_vrope_mark" name="commandant_test_bpet_vrope_mark" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_vrope_grade" name="commandant_test_bpet_vrope_grade" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_vrope_sign" name="commandant_test_bpet_vrope_sign" required></td>
								</tr>
								   <tr class="commandent_test_bpet" name_trg="9'Ditch">
									<td><label>9'Ditch</label></td>
									<td><input type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_9ditch_mark" name="commandant_test_bpet_9ditch_mark" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_9ditch_grade"  name="commandant_test_bpet_9ditch_grade" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_9ditch_sign" name="commandant_test_bpet_9ditch_sign" required></td>
								</tr>
								   <tr class="commandent_test_bpet" name_trg="60M sprint">
									<td><label>60M sprint</label></td>
									<td><input type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_60m_sprint_mark" name="commandant_test_bpet_60m_sprint_mark" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_60m_sprint_grade"  name="commandant_test_bpet_60m_sprint_grade" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_60m_sprint_sign"  name="commandant_test_bpet_60m_sprint_sign" required></td>
									</tr>
								   <tr class="commandent_test_bpet" name_trg="Overall">
									<td><label>Overall</label></td>
									<td><input type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_overall_mark" name="commandant_test_bpet_overall_mark" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_grade" value=""  id="commandant_test_bpet_overall_grade"  name="commandant_test_bpet_overall_grade" required></td>
									<td><input type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_overall_sign"  name="commandant_test_bpet_overall_sign" required></td>
								</tr>
								 
								   
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="commandent_test_bpet_2nd" name_trg="5km run">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_5km_mark_2nd"  name="commandant_test_bpet_5km_mark_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_5km_grade_2nd"  name="commandant_test_bpet_5km_grade_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_5km_sign_2nd"  name="commandant_test_bpet_5km_sign_2nd" required></td>
								 </tr>
								   <tr class="commandent_test_bpet_2nd" name_trg="H/rope">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_hrope_mark_2nd"  name="commandant_test_bpet_hrope_mark_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_hrope_grade_2nd"  name="commandant_test_bpet_hrope_grade_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_hrope_sign_2nd"  name="commandant_test_bpet_hrope_sign_2nd" required></td>
								 </tr>
								   <tr class="commandent_test_bpet_2nd" name_trg="V/rope">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_vrope_mark_2nd"  name="commandant_test_bpet_vrope_mark_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_vrope_grade_2nd"  name="commandant_test_bpet_vrope_grade_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_vrope_sign_2nd"  name="commandant_test_bpet_vrope_sign_2nd" required></td>
								</tr>
								   <tr class="commandent_test_bpet_2nd" name_trg="9'Ditch">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_9ditch_mark_2nd"  name="commandant_test_bpet_9ditch_mark_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_9ditch_grade_2nd"  name="commandant_test_bpet_9ditch_grade_2nd" required ></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_9ditch_sign_2nd"  name="commandant_test_bpet_9ditch_sign_2nd" required></td>
								</tr>
								   <tr class="commandent_test_bpet_2nd" name_trg="60M sprint">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_60m_sprint_mark_2nd"  name="commandant_test_bpet_60m_sprint_mark_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_60m_sprint_grade_2nd"  name="commandant_test_bpet_60m_sprint_grade_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_60m_sprint_sign_2nd"  name="commandant_test_bpet_60m_sprint_sign_2nd" required></td>
									</tr>
								   <tr class="commandent_test_bpet_2nd" name_trg="Overall">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_overall_mark_2nd"  name="commandant_test_bpet_overall_mark_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value=""  id="commandant_test_bpet_overall_grade_2nd"   name="commandant_test_bpet_overall_grade_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_overall_sign_2nd"  name="commandant_test_bpet_overall_sign_2nd" required></td>
								</tr>
								 
								
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Marks</label></th>
									<th><label>Grade</label></th>
									<th><label>Signature</label></th>
								  </tr>
								 <tr class="commandent_test_bpet_3rd" name_trg="5km run">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_5km_mark_3rd" name="commandant_test_bpet_5km_mark_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_5km_grade_3rd"  name="commandant_test_bpet_5km_grade_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_5km_sign_3rd"  name="commandant_test_bpet_5km_sign_3rd" required></td>
								 </tr>
								   <tr class="commandent_test_bpet_3rd" name_trg="H/rope">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_hrope_mark_3rd"  name="commandant_test_bpet_hrope_mark_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_hrope_grade_3rd"  name="commandant_test_bpet_hrope_grade_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_hrope_sign_3rd"  name="commandant_test_bpet_hrope_sign_3rd" required></td>
								 </tr>
								   <tr class="commandent_test_bpet_3rd" name_trg="V/rope">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_vrope_mark_3rd"  name="commandant_test_bpet_vrope_mark_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_vrope_grade_3rd"  name="commandant_test_bpet_vrope_grade_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_vrope_sign_3rd"  name="commandant_test_bpet_vrope_sign_3rd" required></td>
								</tr>
								   <tr class="commandent_test_bpet_3rd" name_trg="9'Ditch">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_9ditch_mark_3rd"  name="commandant_test_bpet_9ditch_mark_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_9ditch_grade_3rd"  name="commandant_test_bpet_9ditch_grade_3rd" required ></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_9ditch_sign_3rd"  name="commandant_test_bpet_9ditch_sign_3rd" required></td>
								</tr>
								   <tr class="commandent_test_bpet_3rd" name_trg="60M sprint">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_60m_sprint_mark_3rd"  name="commandant_test_bpet_60m_sprint_mark_3rd" required ></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value="" id="commandant_test_bpet_60m_sprint_grade_3rd"  name="commandant_test_bpet_60m_sprint_grade_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_60m_sprint_sign_3rd" name="commandant_test_bpet_60m_sprint_sign_3rd" required></td>
									</tr>
								   <tr class="commandent_test_bpet_3rd" name_trg="Overall">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_mark" value="" id="commandant_test_bpet_overall_mark_3rd"  name="commandant_test_bpet_overall_mark_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_grade" value=""  id="commandant_test_bpet_overall_grade_3rd"   name="commandant_test_bpet_overall_grade_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_bpet_sign" value="" id="commandant_test_bpet_overall_sign_3rd" name="commandant_test_bpet_overall_sign_3rd" required></td>
								</tr>
								
								  
								</table>
					  </div>
					</div>
					
					
					
					<div class="row" style="margin-left: 1px;">
					<h4 class="box-title"> COMMANDANT TEST FIRING </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;" >
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="test_firing" name_trg="Lnsas Lmg day">
									<td><label>Lnsas Lmg day</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" value="" id="commandant_test_firing_lnsas_Lmg_day_date" name="commandant_test_firing_lnsas_Lmg_day_date" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" value="" id="commandant_test_firing_lnsas_Lmg_day_mark"  name="commandant_test_firing_lnsas_Lmg_day_mark" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" value="" id="commandant_test_firing_lnsas_Lmg_day_result"  name="commandant_test_firing_lnsas_Lmg_day_result"  required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" value="" id="commandant_test_firing_lnsas_Lmg_day_sign"  name="commandant_test_firing_lnsas_Lmg_day_sign" required></td>
								 </tr>
								   <tr class="test_firing" name_trg="Lnsas Lmg">
									<td><label>Lnsas Lmg</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" value="" id="commandant_test_firing_lnsas_lmg_date" name="commandant_test_firing_lnsas_lmg_date" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" value="" id="commandant_test_firing_lnsas_lmg_mark"  name="commandant_test_firing_lnsas_lmg_mark" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" value="" id="commandant_test_firing_lnsas_lmg_result"  name="commandant_test_firing_lnsas_lmg_result" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" value="" id="commandant_test_firing_lnsas_lmg_sign"  name="commandant_test_firing_lnsas_lmg_sign" required></td>
								 </tr>
								   
								   
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="test_firing_2nd" name_trg="Lnsas Lmg day">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" value="" id="commandant_test_firing_lnsas_Lmg_day_date_2nd"  name="commandant_test_firing_lnsas_Lmg_day_date_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" value="" id="commandant_test_firing_lnsas_Lmg_day_mark_2nd" name="commandant_test_firing_lnsas_Lmg_day_mark_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" value="" id="commandant_test_firing_lnsas_Lmg_day_result_2nd" name="commandant_test_firing_lnsas_Lmg_day_result_2nd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" value="" id="commandant_test_firing_lnsas_Lmg_day_sign_2nd"  name="commandant_test_firing_lnsas_Lmg_day_sign_2nd" required></td>
								 </tr>
								   <tr class="test_firing_2nd" name_trg="Lnsas Lmg">
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" value="" id="commandant_test_firing_lnsas_lmg_date_2nd" name="commandant_test_firing_lnsas_lmg_date_2nd" required></td>
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" value="" id="commandant_test_firing_lnsas_lmg_mark_2nd"  name="commandant_test_firing_lnsas_lmg_mark_2nd" required></td>
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" value="" id="commandant_test_firing_lnsas_lmg_result_2nd"  name="commandant_test_firing_lnsas_lmg_result_2nd" required></td>
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" value="" id="commandant_test_firing_lnsas_lmg_sign_2nd"  name="commandant_test_firing_lnsas_lmg_sign_2nd" required></td>
								 </tr>
								
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="test_firing_3rd" name_trg="Lnsas Lmg day">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" value="" id="commandant_test_firing_lnsas_Lmg_day_date_3rd" name="commandant_test_firing_lnsas_Lmg_day_date_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" value="" id="commandant_test_firing_lnsas_Lmg_day_mark_3rd" name="commandant_test_firing_lnsas_Lmg_day_mark_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" value="" id="commandant_test_firing_lnsas_Lmg_day_result_3rd"  name="commandant_test_firing_lnsas_Lmg_day_result_3rd" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" value="" id="commandant_test_firing_lnsas_Lmg_day_sign_3rd"  name="commandant_test_firing_lnsas_Lmg_day_sign_3rd" required></td>
								 </tr>
								   <tr class="test_firing_3rd" name_trg="Lnsas Lmg">
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" value="" id="commandant_test_firing_lnsas_lmg_date_3rd"  name="commandant_test_firing_lnsas_lmg_date_3rd" required></td>
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" value="" id="commandant_test_firing_lnsas_lmg_mark_3rd" name="commandant_test_firing_lnsas_lmg_mark_3rd" required></td>
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" value="" id="commandant_test_firing_lnsas_lmg_result_3rd"  name="commandant_test_firing_lnsas_lmg_result_3rd" required></td>
									<td style="padding-top: 19px;"><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" value="" id="commandant_test_firing_lnsas_lmg_sign_3rd"  name="commandant_test_firing_lnsas_lmg_sign_3rd" required></td>
								 </tr>
								 
								  
								</table>
					  </div>
					</div>
					
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> COMMANDANT TEST FIRING </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="test_firing_other" name_trg="Drill">
									<td><label>Drill</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_drilldate" name="commandant_test_firing_drilldate" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_drillmark" name="commandant_test_firing_drillmark" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_drillresult"  name="commandant_test_firing_drillresult" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_drillsign" name="commandant_test_firing_drillsign" value="" required></td>
								
								 </tr>
								   <tr class="test_firing_other" name_trg="FC & BC">
									<td><label>FC & BC</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_fc_bc_date" name="commandant_test_firing_fc_bc_date" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_fc_bc_mark"  name="commandant_test_firing_fc_bc_mark" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_fc_bc_result"  name="commandant_test_firing_fc_bc_result" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_fc_bc_sign" name="commandant_test_firing_fc_bc_sign" value="" required></td>
								</tr>
								    <tr class="test_firing_other" name_trg="Written">
									<td><label>Written</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_writtendate" name="commandant_test_firing_writtendate" value="" required></td>
									<td><input data-msg="Required" data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_writtenmark"  name="commandant_test_firing_writtenmark" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_writtenresult"  name="commandant_test_firing_writtenresult" value="" required></td>
									<td><input  data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_writtensign"  name="commandant_test_firing_writtensign" value="" required></td>
								</tr>
								  <tr class="test_firing_other" name_trg="Swimming">
									<td><label>Swimming</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_swimmingdate" name="commandant_test_firing_swimmingdate" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_swimmingmark" name="commandant_test_firing_swimmingmark" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_swimmingresult" name="commandant_test_firing_swimmingresult" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_swimmingsign" name="commandant_test_firing_swimmingsign" value="" required></td>
								</tr>
								  <tr class="test_firing_other" name_trg="ASLT">
									<td><label>ASLT</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_asltdate"  name="commandant_test_firing_asltdate"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_asltmark"  name="commandant_test_firing_asltmark"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_asltresult" name="commandant_test_firing_asltresult" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_asltsign" name="commandant_test_firing_asltsign" value="" required></td>
								</tr>
								  <tr class="test_firing_other" name_trg="WT(SOET)">
									<td><label>WT(SOET)</label></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_wt_soetdate" name="commandant_test_firing_wt_soetdate" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_wt_soetmark"  name="commandant_test_firing_wt_soetmark"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_wt_soetresult" name="commandant_test_firing_wt_soetresult" value="" required ></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_wt_soetsign" name="commandant_test_firing_wt_soetsign" value="" required></td>
								</tr>
								   
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="test_firing_other_2nd" name_trg="Drill">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_drilldate_2nd" name="commandant_test_firing_drilldate_2nd"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_drillmark_2nd"  name="commandant_test_firing_drillmark_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_drillresult_2nd"  name="commandant_test_firing_drillresult_2nd"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_drillsign_2nd" name="commandant_test_firing_drillsign_2nd"value="" required></td>
								
								 </tr>
								   <tr class="test_firing_other_2nd" name_trg="FC & BC">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_fc_bc_date_2nd" name="commandant_test_firing_fc_bc_date_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_fc_bc_mark_2nd" name="commandant_test_firing_fc_bc_mark_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_fc_bc_result_2nd"  name="commandant_test_firing_fc_bc_result_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_fc_bc_sign_2nd" name="commandant_test_firing_fc_bc_sign_2nd" value="" required></td>
								</tr>
								    <tr class="test_firing_other_2nd" name_trg="Written">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_writtendate_2nd" name="commandant_test_firing_writtendate_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_writtenmark_2nd" name="commandant_test_firing_writtenmark_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_writtenresult_2nd"  name="commandant_test_firing_writtenresult_2nd"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_writtensign_2nd" name="commandant_test_firing_writtensign_2nd" value="" required></td>
								</tr>
								  <tr class="test_firing_other_2nd" name_trg="Swimming">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_swimmingdate_2nd"name="commandant_test_firing_swimmingdate_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_swimmingmark_2nd" name="commandant_test_firing_swimmingmark_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_swimmingresult_2nd" name="commandant_test_firing_swimmingresult_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_swimmingsign_2nd" name="commandant_test_firing_swimmingsign_2nd" value="" required></td>
								</tr>
								  <tr class="test_firing_other_2nd" name_trg="ASLT">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_asltdate_2nd" name="commandant_test_firing_asltdate_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_asltmark_2nd"  name="commandant_test_firing_asltmark_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_asltresult_2nd" name="commandant_test_firing_asltresult_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_asltsign_2nd" name="commandant_test_firing_asltsign_2nd" value="" required></td>
								</tr>
								  <tr class="test_firing_other_2nd" name_trg="WT(SOET)">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_wt_soetdate_2nd"  name="commandant_test_firing_wt_soetdate_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_wt_soetmark_2nd"  name="commandant_test_firing_wt_soetmark_2nd"  value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_wt_soetresult_2nd"  name="commandant_test_firing_wt_soetresult_2nd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_wt_soetsign_2nd"  name="commandant_test_firing_wt_soetsign_2nd"  value="" required></td>
								</tr>
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								   <tr class="test_firing_other_3rd" name_trg="Drill">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_drilldate_3rd" name="commandant_test_firing_drilldate_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_drillmark_3rd" name="commandant_test_firing_drillmark_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_drillresult_3rd" name="commandant_test_firing_drillresult_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_drillsign_3rd" name="commandant_test_firing_drillsign_3rd" value="" required></td>
								
								 </tr>
								   <tr class="test_firing_other_3rd" name_trg="FC & BC">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_fc_bc_date_3rd"  name="commandant_test_firing_fc_bc_date_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_fc_bc_mark_3rd"  name="commandant_test_firing_fc_bc_mark_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_fc_bc_result_3rd"  name="commandant_test_firing_fc_bc_result_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_fc_bc_sign_3rd" name="commandant_test_firing_fc_bc_sign_3rd" value="" required></td>
								</tr>
								    <tr class="test_firing_other_3rd" name_trg="Written">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_writtendate_3rd" name="commandant_test_firing_writtendate_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_writtenmark_3rd" name="commandant_test_firing_writtenmark_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_writtenresult_3rd"  name="commandant_test_firing_writtenresult_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_writtensign_3rd" name="commandant_test_firing_writtensign_3rd" value="" required></td>
								</tr>
								  <tr class="test_firing_other_3rd" name_trg="Swimming">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_swimmingdate_3rd" name="commandant_test_firing_swimmingdate_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_swimmingmark_3rd"  name="commandant_test_firing_swimmingmark_3rd" value="" required ></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_swimmingresult_3rd"  name="commandant_test_firing_swimmingresult_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_swimmingsign_3rd" name="commandant_test_firing_swimmingsign_3rd" value="" required></td>
								</tr>
								  <tr class="test_firing_other_3rd" name_trg="ASLT">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_asltdate_3rd"  name="commandant_test_firing_asltdate_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_asltmark_3rd"  name="commandant_test_firing_asltmark_3rd"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_asltresult_3rd"  name="commandant_test_firing_asltresult_3rd"value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_asltsign_3rd"  name="commandant_test_firing_asltsign_3rd" value="" required></td>
								</tr>
								  <tr class="test_firing_other_3rd" name_trg="WT(SOET)">
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_date date" id="commandant_test_firing_wt_soetdate_3rd" name="commandant_test_firing_wt_soetdate_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_mark" id="commandant_test_firing_wt_soetmark_3rd"  name="commandant_test_firing_wt_soetmark_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_result" id="commandant_test_firing_wt_soetresult_3rd" name="commandant_test_firing_wt_soetresult_3rd" value="" required></td>
									<td><input data-msg="Required" type="text" class="form-control commandant_test_firing_sign" id="commandant_test_firing_wt_soetsign_3rd" name="commandant_test_firing_wt_soetsign_3rd" value="" required ></td>
								</tr>
								  
								</table>
					  </div>
					</div>
					
					
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> BASIC FOUNDATION COURSE </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Course</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="basic_course" name_trg="Hindi">
									<td><label> Hindi </label></td>
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_hindidate" name="basic_course_hindidate" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark " id="basic_course_hindimark" name="basic_course_hindimark" value="" required></td>
									<td><input type="text" class="form-control basic_course_result"  id="basic_course_hindiresult"  name="basic_course_hindiresult" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_hindisign" name="basic_course_hindisign" value="" required></td>
								 </tr>
								   <tr class="basic_course" name_trg="English">
									<td><label> English </label></td>
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_englishdate" name="basic_course_englishdate" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark " id="basic_course_englishmark" name="basic_course_englishmark" value="" required ></td>
									<td><input type="text" class="form-control basic_course_result " id="basic_course_englishresult" name="basic_course_englishresult" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign " id="basic_course_englishsign" name="basic_course_englishsign" value="" required></td>
								 </tr>
								    <tr class="basic_course" name_trg="Maths">
									<td><label> Maths </label></td>
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_mathsdate"  name="basic_course_mathsdate" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark " id="basic_course_mathsmark" name="basic_course_mathsmark" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_mathsresult" name="basic_course_mathsresult" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_mathssign"  name="basic_course_mathssign" value="" required></td>
								 </tr>
								  <tr class="basic_course" name_trg="GA">
									<td><label> GA </label></td>
								    <td><input type="text" class="form-control basic_course_date date" id="basic_course_gadate" name="basic_course_gadate" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_gamark" name="basic_course_gamark" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_garesult" name="basic_course_garesult" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_gasign" name="basic_course_gasign" value="" required></td>
								 </tr>
								  <tr class="basic_course" name_trg="G'Sc">
									<td><label> G'Sc </label></td>
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_gscdate" name="basic_course_gscdate" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_gscmark" name="basic_course_gscmark" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_gscresult" name="basic_course_gscresult" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_gscsign" name="basic_course_gscsign" value="" required></td>
								 </tr>
								  <tr class="basic_course" name_trg="OIM">
									<td><label> OIM </label></td>
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_oimdate" name="basic_course_oimdate" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_oimmark" name="basic_course_oimmark" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_oimresult" name="basic_course_oimresult" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_oimsign" name="basic_course_oimsign" value="" required></td>
								 </tr>
								     <tr class="basic_course" name_trg="MH">
									<td><label> MH </label></td>
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_mhdate" name="basic_course_mhdate" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_mhmark" name="basic_course_mhmark" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_mhresult" name="basic_course_mhresult" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_mhsign" name="basic_course_mhsign" value="" required></td>
								 </tr>
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								    <tr class="basic_course_2nd" name_trg="Hindi">
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_hindidate_2nd" name="basic_course_hindidate_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_hindimark_2nd" name="basic_course_hindimark_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_result"  id="basic_course_hindiresult_2nd" name="basic_course_hindiresult_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_hindisign_2nd" name="basic_course_hindisign_2nd" value="" required></td>
								 </tr>
								   <tr class="basic_course_2nd" name_trg="English">
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_englishdate_2nd" name="basic_course_englishdate_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark " id="basic_course_englishmark_2nd" name="basic_course_englishmark_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_result " id="basic_course_englishresult_2nd"  name="basic_course_englishresult_2nd"value="" required></td>
									<td><input type="text" class="form-control basic_course_sign " id="basic_course_englishsign_2nd"  name="basic_course_englishsign_2nd" value="" required></td>
								 </tr>
								    <tr class="basic_course_2nd" name_trg="Maths">
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_mathsdate_2nd" name="basic_course_mathsdate_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_mathsmark_2nd" name="basic_course_mathsmark_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_mathsresult_2nd" name="basic_course_mathsresult_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_mathssign_2nd" name="basic_course_mathssign_2nd" value="" required></td>
								 </tr>
								  <tr class="basic_course_2nd" name_trg="GA">
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_gadate_2nd" name="basic_course_gadate_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_gamark_2nd" name="basic_course_gamark_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_garesult_2nd" name="basic_course_garesult_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_gasign_2nd"  name="basic_course_gasign_2nd"value="" required></td>
								 </tr>
								  <tr class="basic_course_2nd" name_trg="G'Sc">
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_gscdate_2nd" name="basic_course_gscdate_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_gscmark_2nd"  name="basic_course_gscmark_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_gscresult_2nd"  name="basic_course_gscresult_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_gscsign_2nd"  name="basic_course_gscsign_2nd" value="" required></td>
								 </tr>
								  <tr class="basic_course_2nd" name_trg="OIM">
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_oimdate_2nd"  name="basic_course_oimdate_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_oimmark_2nd"  name="basic_course_oimmark_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_oimresult_2nd"  name="basic_course_oimresult_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_oimsign_2nd"  name="basic_course_oimsign_2nd" value="" required></td>
								 </tr>
								     <tr class="basic_course_2nd" name_trg="MH">
									<td><input type="text" class="form-control basic_course_date date" id="basic_course_mhdate_2nd" name="basic_course_mhdate_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_mark" id="basic_course_mhmark_2nd" name="basic_course_mhmark_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_result" id="basic_course_mhresult_2nd" name="basic_course_mhresult_2nd" value="" required></td>
									<td><input type="text" class="form-control basic_course_sign" id="basic_course_mhsign_2nd" name="basic_course_mhsign_2nd" value="" required></td>
								 </tr>
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								    <tr class="basic_course_3rd" name_trg="Hindi">
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_date date" id="basic_course_hindidate_3rd" name="basic_course_hindidate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_mark" id="basic_course_hindimark_3rd" name="basic_course_hindimark_3rd" value="" required ></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_result"  id="basic_course_hindiresult_3rd"  name="basic_course_hindiresult_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_sign" id="basic_course_hindisign_3rd" name="basic_course_hindisign_3rd" value="" required></td>
								 </tr>
								   <tr class="basic_course_3rd" name_trg="English">
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_date date" id="basic_course_englishdate_3rd" name="basic_course_englishdate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_mark " id="basic_course_englishmark_3rd" name="basic_course_englishmark_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_result " id="basic_course_englishresult_3rd" name="basic_course_englishresult_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_sign " id="basic_course_englishsign_3rd" name="basic_course_englishsign_3rd" value="" required></td>
								 </tr>
								    <tr class="basic_course_3rd" name_trg="Maths">
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_date date" id="basic_course_mathsdate_3rd"  name="basic_course_mathsdate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_mark" id="basic_course_mathsmark_3rd"  name="basic_course_mathsmark_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_result" id="basic_course_mathsresult_3rd"  name="basic_course_mathsresult_3rd" value="" required></td>
									<td><input data-msg="This field Required"  type="text" class="form-control basic_course_sign" id="basic_course_mathssign_3rd"  name="basic_course_mathssign_3rd" value="" required></td>
								 </tr>
								  <tr class="basic_course_3rd" name_trg="GA">
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_date date" id="basic_course_gadate_3rd"  name="basic_course_gadate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_mark" id="basic_course_gamark_3rd" name="basic_course_gamark_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_result" id="basic_course_garesult_3rd"  name="basic_course_garesult_3rd"  value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_sign" id="basic_course_gasign_3rd"  name="basic_course_gasign_3rd" value="" required></td>
								 </tr>
								  <tr class="basic_course_3rd" name_trg="G'Sc">
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_date date" id="basic_course_gscdate_3rd"  name="basic_course_gscdate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_mark" id="basic_course_gscmark_3rd"  name="basic_course_gscmark_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_result" id="basic_course_gscresult_3rd"  name="basic_course_gscresult_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_sign" id="basic_course_gscsign_3rd" name="basic_course_gscsign_3rd" value="" required></td>
								 </tr>
								  <tr class="basic_course_3rd" name_trg="OIM">
									<td><input  data-msg="This field Required" type="text" class="form-control basic_course_date date" id="basic_course_oimdate_3rd" name="basic_course_oimdate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_mark" id="basic_course_oimmark_3rd" name="basic_course_oimmark_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_result" id="basic_course_oimresult_3rd" name="basic_course_oimresult_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_sign" id="basic_course_oimsign_3rd" name="basic_course_oimsign_3rd" value="" required></td>
								 </tr>
								     <tr class="basic_course_3rd" name_trg="MH">
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_date date" id="basic_course_mhdate_3rd" name="basic_course_mhdate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_mark" id="basic_course_mhmark_3rd" name="basic_course_mhmark_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_result" id="basic_course_mhresult_3rd" name="basic_course_mhresult_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control basic_course_sign" id="basic_course_mhsign_3rd" name="basic_course_mhsign_3rd" value="" required></td>
								 </tr>
								</table>
					  </div>
					</div>
					
					
					<div class="row"  style="margin-left: 1px;">
					<h4 class="box-title"> MAP Reading Test </h4>
					  <div class="col-md-5" style="padding:0px">
					  <h5 style="margin-left: 11px;"> 1st Attempt </h5>
                       <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Event</label></th>
									<th><label>Date</label></th>
									<th><label>Mark</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="map_reading" name_trg="Practical">
									<td><label>Practical</label></td>
									<td><input type="text" class="form-control map_reading_date date"  id="map_reading_practicaldate"  name="map_reading_practicaldate"  value="" required/></td>
									<td><input type="text" class="form-control map_reading_mark"  id="map_reading_practicalmark"  name="map_reading_practicalmark" value="" required></td>
									<td><input type="text" class="form-control map_reading_result"  id="map_reading_practicalresult"  name="map_reading_practicalresult"  value="" required></td>
									<td><input type="text" class="form-control map_reading_sign"  id="map_reading_practicalsign" name="map_reading_practicalsign"  value="" required></td>
								 </tr>
								   <tr class="map_reading" name_trg="Written">
									<td><label>Written</label></td>
									<td><input type="text" class="form-control map_reading_date date"   id="map_reading_writtendate"   name="map_reading_writtendate" value="" required></td>
									<td><input type="text" class="form-control map_reading_mark"  id="map_reading_writtenmark"  name="map_reading_writtenmark"  value="" required></td>
									<td><input type="text" class="form-control map_reading_result"  id="map_reading_writtenresult"  name="map_reading_writtenresult"  value="" required></td>
									<td><input type="text" class="form-control map_reading_sign"  id="map_reading_writtensign"  name="map_reading_writtensign"  value="" required></td>
								 </tr>
								   
								   
								</table>
					  </div>
					  <div class="col-md-4" style="padding:0px">
					  <h5> 2nd Attempt </h5>
                      <table class="table" style="width:100%;border-right: 1px solid;">
								  <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="map_reading_2nd" name_trg="Practical">
									<td><input type="text" class="form-control map_reading_date date"  id="map_reading_practicaldate_2nd" name="map_reading_practicaldate_2nd"  value="" required></td>
									<td><input type="text" class="form-control map_reading_mark"  id="map_reading_practicalmark_2nd" name="map_reading_practicalmark_2nd"  value="" required></td>
									<td><input type="text" class="form-control map_reading_result"  id="map_reading_practicalresult_2nd" name="map_reading_practicalresult_2nd"  value="" required></td>
									<td><input type="text" class="form-control map_reading_sign"  id="map_reading_practicalsign_2nd" name="map_reading_practicalsign_2nd"  value="" required></td>
								 </tr>
								   <tr class="map_reading_2nd" name_trg="Written">
									<td><input type="text" class="form-control map_reading_date date"   id="map_reading_writtendate_2nd" name="map_reading_writtendate_2nd" value="" required></td>
									<td><input type="text" class="form-control map_reading_mark"  id="map_reading_writtenmark_2nd" name="map_reading_writtenmark_2nd"  value="" required></td>
									<td><input type="text" class="form-control map_reading_result"  id="map_reading_writtenresult_2nd" name="map_reading_writtenresult_2nd"  value="" required></td>
									<td><input type="text" class="form-control map_reading_sign"  id="map_reading_writtensign_2nd"  name="map_reading_writtensign_2nd"  value="" required></td>
								 </tr>
								
								</table>
					  </div>
					  <div class="col-md-3" style="padding:0px">
					  <h5> 3rd Attempt </h5>
                      <table class="table" style="width:100%">
								   <tr>
									<th><label>Date</label></th>
									<th><label>Marks</label></th>
									<th><label>Result</label></th>
									<th><label>Signature</label></th>
								  </tr>
								  <tr class="map_reading_3rd" name_trg="Practical">
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_date date"  id="map_reading_practicaldate_3rd" name="map_reading_practicaldate_3rd"  value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_mark"  id="map_reading_practicalmark_3rd" name="map_reading_practicalmark_3rd"  value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_result"  id="map_reading_practicalresult_3rd" name="map_reading_practicalresult_3rd"  value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_sign"  id="map_reading_practicalsign_3rd" name="map_reading_practicalsign_3rd"   value="" required></td>
								 </tr>
								   <tr class="map_reading_3rd" name_trg="Written">
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_date date"   id="map_reading_writtendate_3rd"   name="map_reading_writtendate_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_mark"  id="map_reading_writtenmark_3rd"   name="map_reading_writtenmark_3rd" value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_result"  id="map_reading_writtenresult_3rd" name="map_reading_writtenresult_3rd"  value="" required></td>
									<td><input data-msg="This field Required" type="text" class="form-control map_reading_sign"  id="map_reading_writtensign_3rd"  name="map_reading_writtensign_3rd" value="" required></td>
								 </tr>
								 
								  
								</table>
					  </div>
					</div>
					
					
					
				</div>
				<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
				</form>
			</div>
		</div>
		</div>
		
	</div>
	</div>
</div>

<script>	
$('document').ready(function(){
           
		
	
	   
	
	$("body").on("click",'#addProSpeci', function () {
		console.log('TextBoxContainer2');
		var div = $("<div class='remove-added-para'>");
		div.html(GetDynamicTextBoxforSpeci());
		$(".TextBoxContainer2").append(GetDynamicTextBoxforSpeci());
		
		/* $('.TextBoxContainer2').find(".inputdate").datepicker({
               changeMonth: true,
            changeYear: true,
			dateFormat: 'yy-mm-dd',
            yearRange: 'c-75:c+75',
        }); */
	});
	
	$("body").on("click", ".removepara", function () {
		$(this).parent().parent().remove();
	});
	 function GetDynamicTextBoxforSpeci() {
		var content = '<div class="row remove-added-para" style="margin-left: -22px;">\n\
		                <div class="col-md-3" style="padding: 0px;"><span class="removepara" style="float: left;font-size: 25px;color: red;">&times; </span><input required style="float: right;width: 83%;" type="text" class="" placeholder="Relation" name="candidate_relation" id="candidate_relation" value=""></div>\n\
						<div class="col-md-4"><input required type="text"  class="" placeholder="Name" id="candidate_relation_name" name="candidate_relation_name" ></div>\n\
						<div class="col-md-3" style="padding: 0px;"><input style="width:100%" type="date" required class=" inputdate" placeholder="DOB" id="candidate_relation_dob" name="candidate_relation_dob" ></div>\n\
                        <div class="col-md-2"><input type="text" required class="" placeholder="AGE" name="candidate_relation_age" id="candidate_relation_age" style="width: 100%;padding: 0px;float: left;"></div>\n\
                        </div>';
		return 	content;			
	}
 
	$('#register_form').validate({
		ignore: [],
         rules: {
             candidate_army_no: {
                required: true,
            },
			candidate_course: {
                required: true,
            },
			candidate_company: {
                required: true,
            },
			candidate_name: {
                required: true,
            },
			candidate_section: {
                required: true,
            },
			candidate_year: {
                required: true,
            },
			candidate_rank: {
                required: true,
            },
			candidate_platoon: {
                required: true,
            },
			candidate_aro_uhq: {
                required: true,
            },
			candidate_dob: {
                required: true,
            },
			candidate_doe: {
                required: true,
            },
			candidate_poe: {
                required: true,
            },
			candidate_blood_group: {
                required: true,
            },
			candidate_religion: {
                required: true,
            },
			candidate_caste: {
                required: true,
            },
			candidate_address: {
                required: true,
            },
			candidate_village: {
                required: true,
            },
			candidate_post_office: {
                required: true,
            },
			candidate_district: {
                required: true,
            },
			candidate_state: {
                required: true,
            },
			company_commander_rank: {
                required: true,
            },
			company_commander_name: {
                required: true,
            },
			company_commander_form_date: {
                required: true,
            },
			platoon_commander_rank: {
                required: true,
            },
			platoon_commander_name: {
                required: true,
            },
			platoon_commander_form_date: {
                required: true,
            },
			platoon_havaldar_first_rank: {
                required: true,
            },
			platoon_havaldar_first_name: {
                required: true,
            },
			platoon_havaldar_first_form_date: {
                required: true,
            },
			platoon_havaldar_second_rank: {
                required: true,
            },
			platoon_havaldar_second_name: {
                required: true,
            },
			platoon_havaldar_second_form_date: {
                required: true,
            },
			qualification_civil: {
                required: true,
            },
			qualification_mr: {
                required: true,
            },
			qualification_aec: {
                required: true,
            },
			qualification_ttt: {
                required: true,
            },
			qualification_attestation: {
                required: true,
            },
			qualification_civil_date: {
                required: true,
            },
			qualification_civil_part_order: {
                required: true,
            },
			qualification_mr_date: {
                required: true,
            },
			qualification_mr_part_order: {
                required: true,
            },
			qualification_aec_date: {
                required: true,
            },
			qualification_aec_part_order: {
                required: true,
            },
			qualification_ttt_date: {
                required: true,
            },
			qualification_ttt_part_order: {
                required: true,
            },
			qualification_attestation_date: {
                required: true,
            },
			qualification_attestation_part_order: {
                required: true,
            },
			candidate_height: {
                required: true,
            },
			candidate_permissible: {
                required: true,
            },
			candidate_actual_wt: {
                required: true,
            },
			candidate_over_under_wt: {
                required: true,
            },
			candidate_over_under_wt: {
                required: true,
            },
			candidate_identification_mark_one: {
                required: true,
            },
			candidate_identification_mark_two: {
                required: true,
            },
			qualification_civil_date: {
                required: true,
            },
			trg_bn_ppt_2_4_km_mark: {
                required: true,
            },
			trg_bn_ppt_2_4_km_grade: {
                required: true,
            },
			trg_bn_ppt_2_4_km_sign: {
                required: true,
            },
            trg_bn_ppt_bent_knee_sit_up_mark: {
                required: true,
            },
			trg_bn_ppt_bent_knee_sit_up_grade: {
                required: true,
            },
			trg_bn_ppt_bent_knee_sit_up_sign: {
                required: true,
            },
			trg_bn_ppt_5m_shuttle_mark: {
                required: true,
            },
			trg_bn_ppt_5m_shuttle_grade: {
                required: true,
            },
			trg_bn_ppt_5m_shuttle_sign: {
                required: true,
            },
			qualification_civil_date: {
                required: true,
            },
		}, 
		 messages: {
			
			blog_title: {
                required: "Chapter Name is required.",
            },
			content: {
                required: "History/Description is required.",
            },
			slider_image: {
                required: "Blog Image is required.",
            },
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			var trg_arr = [];
			  $('.trg_one').each(function(key,val){
              
			   trg_arr.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_ppt_mark : $(this).find('td').find('.trg_bn_ppt_mark').val(),
					trg_bn_ppt_grade : $(this).find('td').find('.trg_bn_ppt_grade').val(),
					trg_bn_ppt_sign : $(this).find('td').find('.trg_bn_ppt_sign').val(),
				}); 
            });
			
			var trg_2nd = [];
			  $('.trg_2nd').each(function(key,val){
              
			   trg_2nd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_ppt_mark : $(this).find('td').find('.trg_bn_ppt_mark').val(),
					trg_bn_ppt_grade : $(this).find('td').find('.trg_bn_ppt_grade').val(),
					trg_bn_ppt_sign : $(this).find('td').find('.trg_bn_ppt_sign').val(),
				}); 
            });
			
			var trg_3rd = [];
			  $('.trg_3rd').each(function(key,val){
              
			   trg_3rd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_ppt_mark : $(this).find('td').find('.trg_bn_ppt_mark').val(),
					trg_bn_ppt_grade : $(this).find('td').find('.trg_bn_ppt_grade').val(),
					trg_bn_ppt_sign : $(this).find('td').find('.trg_bn_ppt_sign').val(),
				}); 
            });
			
			var trg_bn = [];
			  $('.trg_bn').each(function(key,val){
              
			   trg_bn.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_cdr_date : $(this).find('td').find('.trg_bn_cdr_date').val(),
					trg_bn_cdr_marks : $(this).find('td').find('.trg_bn_cdr_marks').val(),
					trg_bn_cdr_grade : $(this).find('td').find('.trg_bn_cdr_grade').val(),
					trg_bn_cdr_sign : $(this).find('td').find('.trg_bn_cdr_sign').val(),
				}); 
            });
			var trg_bn_2nd = [];
			  $('.trg_bn_2nd').each(function(key,val){
              
			   trg_bn_2nd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_cdr_date : $(this).find('td').find('.trg_bn_cdr_date').val(),
					trg_bn_cdr_marks : $(this).find('td').find('.trg_bn_cdr_marks').val(),
					trg_bn_cdr_grade : $(this).find('td').find('.trg_bn_cdr_grade').val(),
					trg_bn_cdr_sign : $(this).find('td').find('.trg_bn_cdr_sign').val(),
				}); 
            });
			
			var trg_bn_3rd = [];
			  $('.trg_bn_3rd').each(function(key,val){
              
			   trg_bn_3rd.push({
				    name_trg : $(this).attr('name_trg'),
					trg_bn_cdr_date : $(this).find('td').find('.trg_bn_cdr_date').val(),
					trg_bn_cdr_marks : $(this).find('td').find('.trg_bn_cdr_marks').val(),
					trg_bn_cdr_grade : $(this).find('td').find('.trg_bn_cdr_grade').val(),
					trg_bn_cdr_sign : $(this).find('td').find('.trg_bn_cdr_sign').val(),
				}); 
            });
			
			
			var commandent_test_bpet = [];
			  $('.commandent_test_bpet').each(function(key,val){
              
			   commandent_test_bpet.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_bpet_mark : $(this).find('td').find('.commandant_test_bpet_mark').val(),
					commandant_test_bpet_grade : $(this).find('td').find('.commandant_test_bpet_grade').val(),
					commandant_test_bpet_sign : $(this).find('td').find('.commandant_test_bpet_sign').val(),
				}); 
            });
			var commandent_test_bpet_2nd = [];
			  $('.commandent_test_bpet_2nd').each(function(key,val){
              
			   commandent_test_bpet_2nd.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_bpet_mark : $(this).find('td').find('.commandant_test_bpet_mark').val(),
					commandant_test_bpet_grade : $(this).find('td').find('.commandant_test_bpet_grade').val(),
					commandant_test_bpet_sign : $(this).find('td').find('.commandant_test_bpet_sign').val(),
				}); 
            });
			var commandent_test_bpet_3rd = [];
			  $('.commandent_test_bpet_3rd').each(function(key,val){
              
			   commandent_test_bpet_3rd.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_bpet_mark : $(this).find('td').find('.commandant_test_bpet_mark').val(),
					commandant_test_bpet_grade : $(this).find('td').find('.commandant_test_bpet_grade').val(),
					commandant_test_bpet_sign : $(this).find('td').find('.commandant_test_bpet_sign').val(),
				}); 
            });
			
			var test_firing = [];
			  $('.test_firing').each(function(key,val){
              
			   test_firing.push({
				    name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_2nd = [];
			  $('.test_firing_2nd').each(function(key,val){
              
			   test_firing_2nd.push({
				     name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_3rd = [];
			  $('.test_firing_3rd').each(function(key,val){
              
			   test_firing_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			
			
			
			var test_firing_other = [];
			  $('.test_firing_other').each(function(key,val){
              
			   test_firing_other.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_other_2nd = [];
			  $('.test_firing_other_2nd').each(function(key,val){
              
			   test_firing_other_2nd.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			var test_firing_other_3rd = [];
			  $('.test_firing_other_3rd').each(function(key,val){
              
			   test_firing_other_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					commandant_test_firing_date : $(this).find('td').find('.commandant_test_firing_date').val(),
					commandant_test_firing_mark : $(this).find('td').find('.commandant_test_firing_mark').val(),
					commandant_test_firing_result : $(this).find('td').find('.commandant_test_firing_result').val(),
					commandant_test_firing_sign : $(this).find('td').find('.commandant_test_firing_sign').val(),
				}); 
            });
			
			var basic_course = [];
			  $('.basic_course').each(function(key,val){
              
			   basic_course.push({
				      name_trg : $(this).attr('name_trg'),
					basic_course_date : $(this).find('td').find('.basic_course_date').val(),
					basic_course_mark : $(this).find('td').find('.basic_course_mark').val(),
					basic_course_result : $(this).find('td').find('.basic_course_result').val(),
					basic_course_sign : $(this).find('td').find('.basic_course_sign').val(),
				}); 
            });
			var basic_course_2nd = [];
			  $('.basic_course_2nd').each(function(key,val){
              
			   basic_course_2nd.push({
				      name_trg : $(this).attr('name_trg'),
					basic_course_date : $(this).find('td').find('.basic_course_date').val(),
					basic_course_mark : $(this).find('td').find('.basic_course_mark').val(),
					basic_course_result : $(this).find('td').find('.basic_course_result').val(),
					basic_course_sign : $(this).find('td').find('.basic_course_sign').val(),
				}); 
            });
			var basic_course_3rd = [];
			  $('.basic_course_3rd').each(function(key,val){
              
			   basic_course_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					basic_course_date : $(this).find('td').find('.basic_course_date').val(),
					basic_course_mark : $(this).find('td').find('.basic_course_mark').val(),
					basic_course_result : $(this).find('td').find('.basic_course_result').val(),
					basic_course_sign : $(this).find('td').find('.basic_course_sign').val(),
				}); 
            });
			
			
			var map_reading = [];
			  $('.map_reading').each(function(key,val){
               
			   map_reading.push({
				      name_trg : $(this).attr('name_trg'),
					map_reading_date : $(this).find('td').find('.map_reading_date').val(),
					map_reading_mark : $(this).find('td').find('.map_reading_mark').val(),
					map_reading_result : $(this).find('td').find('.map_reading_result').val(),
					map_reading_sign : $(this).find('td').find('.map_reading_sign').val(),
				}); 
            });
			var map_reading_2nd = [];
			  $('.map_reading_2nd').each(function(key,val){
              
			   map_reading_2nd.push({
				      name_trg : $(this).attr('name_trg'),
					map_reading_date : $(this).find('td').find('.map_reading_date').val(),
					map_reading_mark : $(this).find('td').find('.map_reading_mark').val(),
					map_reading_result : $(this).find('td').find('.map_reading_result').val(),
					map_reading_sign : $(this).find('td').find('.map_reading_sign').val(),
				}); 
            });
			var map_reading_3rd = [];
			  $('.map_reading_3rd').each(function(key,val){
              
			   map_reading_3rd.push({
				      name_trg : $(this).attr('name_trg'),
					map_reading_date : $(this).find('td').find('.map_reading_date').val(),
					map_reading_mark : $(this).find('td').find('.map_reading_mark').val(),
					map_reading_result : $(this).find('td').find('.map_reading_result').val(),
					map_reading_sign : $(this).find('td').find('.map_reading_sign').val(),
				}); 
            });
			
			var physical_parameters = [];
			   physical_parameters.push({
			       candidate_height: $('#candidate_height').val(),
                   candidate_permissible_wt: $('#candidate_permissible_wt').val(),
                   candidate_actual_wt: $('#candidate_actual_wt').val(),
                   candidate_over_under_wt: $('#candidate_over_under_wt').val(),
                   candidate_identification_mark_one: $('#candidate_identification_mark_one').val(),
                   candidate_identification_mark_two: $('#candidate_identification_mark_two').val(),
                 });
			
			var instructer_info = [];
			   instructer_info.push({
			       company_commander_rank: $('#company_commander_rank').val(),
                   company_commander_name: $('#company_commander_name').val(),
                   company_commander_form_date: $('#company_commander_form_date').val(),
                   platoon_commander_rank: $('#platoon_commander_rank').val(),
                   platoon_commander_name: $('#platoon_commander_name').val(),
                   platoon_commander_form_date: $('#platoon_commander_form_date').val(),
                   platoon_havaldar_first_rank: $('#platoon_havaldar_first_rank').val(),
                   platoon_havaldar_first_name: $('#platoon_havaldar_first_name').val(),
                   platoon_havaldar_first_form_date: $('#platoon_havaldar_first_form_date').val(),
                   platoon_havaldar_second_rank: $('#platoon_havaldar_second_rank').val(),
                   platoon_havaldar_second_name: $('#platoon_havaldar_second_name').val(),
                   platoon_havaldar_second_form_date: $('#platoon_havaldar_second_form_date').val(),
		        });
			 var edu_info = [];
			   edu_info.push({
			       qualification_civil: $('#qualification_civil').val(),
                   qualification_civil_date: $('#qualification_civil_date').val(),
                   qualification_civil_part_order: $('#qualification_civil_part_order').val(),
                   qualification_mr: $('#qualification_mr').val(),
                   qualification_mr_date: $('#qualification_mr_date').val(),
                   qualification_mr_part_order: $('#qualification_mr_part_order').val(),
                   qualification_aec: $('#qualification_aec').val(),
                   qualification_aec_date: $('#qualification_aec_date').val(),
                   qualification_aec_part_order: $('#qualification_aec_part_order').val(),
                   qualification_ttt: $('#qualification_ttt').val(),
                   qualification_ttt_date: $('#qualification_ttt_date').val(),
                   qualification_ttt_part_order: $('#qualification_ttt_part_order').val(),
                   qualification_attestation: $('#qualification_attestation').val(),
                   qualification_attestation_date: $('#qualification_attestation_date').val(),
                   qualification_attestation_part_order: $('#qualification_attestation_part_order').val(),
                }); 
			
			var family_data = [];
			$('.TextBoxContainer2 .remove-added-para').each(function(key,val){
				 family_data.push({
					candidate_relation : $(this).find('.candidate_relation').val(),
					candidate_relation_name : $(this).find('.candidate_relation_name').val(),
					candidate_relation_dob : $(this).find('.candidate_relation_dob').val(),
					candidate_relation_age : $(this).find('.candidate_relation_age').val(),
				});  
			});
			$.post(APP_URL + 'admin/registration/add_candidate', {
                trg_arr: trg_arr,
                trg_2nd: trg_2nd,
                trg_3rd: trg_3rd,
                trg_bn: trg_bn,
                trg_bn_2nd: trg_bn_2nd,
                trg_bn_3rd: trg_bn_3rd,
                test_firing: test_firing,
                test_firing_2nd: test_firing_2nd,
                test_firing_3rd: test_firing_3rd,
                test_firing_other: test_firing_other,
                test_firing_other_2nd: test_firing_other_2nd,
                test_firing_other_3rd: test_firing_other_3rd,
                basic_course: basic_course,
                basic_course_2nd: basic_course_2nd,
                basic_course_3rd: basic_course_3rd,
                map_reading: map_reading,
                map_reading_2nd: map_reading_2nd,
                map_reading_3rd: map_reading_3rd,
                commandent_test_bpet: commandent_test_bpet,
                commandent_test_bpet_2nd: commandent_test_bpet_2nd,
                commandent_test_bpet_3rd: commandent_test_bpet_3rd,
                physical_parameters: physical_parameters,
                edu_info: edu_info,
                family_data: family_data,
                instructer_info: instructer_info,
                candidate_army_no: $('#candidate_army_no').val(),
                candidate_course: $('#candidate_course').val(),
                candidate_company: $('#candidate_company').val(),
                candidate_name: $('#candidate_name').val(),
                candidate_section: $('#candidate_section').val(),
                candidate_year: $('#candidate_year').val(),
                candidate_rank: $('#candidate_rank').val(),
                candidate_platoon: $('#candidate_platoon').val(),
                candidate_aro_uhq: $('#candidate_aro_uhq').val(),
                candidate_dob: $('#candidate_dob').val(),
                candidate_doe: $('#candidate_doe').val(),
                candidate_poe: $('#candidate_poe').val(),
                candidate_address: $('#candidate_address').val(),
                candidate_blood_group: $('#candidate_blood_group').val(),
                candidate_religion: $('#candidate_religion').val(),
                candidate_caste: $('#candidate_caste').val(),
                candidate_village: $('#candidate_village').val(),
                candidate_post_office: $('#candidate_post_office').val(),
                candidate_district: $('#candidate_district').val(),
                candidate_state: $('#candidate_state').val(),
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
                    var candidate_id = response.candidate_id;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.href = APP_URL+'admin/registration/print_view?id='+candidate_id;
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				
			}, 'json');
		return false;
		},
	});
	
	

	
	
	
});
</script>	
