<style>
.marBot20{
	margin-bottom:20px;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

	
	<?php
	$heading = '';
	
	if($profile_data ){
		$heading = 'Edit User Profile';
		$user_id =$profile_data[0]['user_id']; 
		$user_name =$profile_data[0]['user_name']; 
		$user_email =$profile_data[0]['user_email']; 
		$phone_no =$profile_data[0]['phone_no']; 
		$profile_picture =$profile_data[0]['profile_picture']; 
		
		
	}else{
		$heading = 'Add New User Profile';
		$user_id =0;
		$user_name ='';
		$user_email ='';
		$phone_no ='';
		$profile_picture ='';
		
	}
		
	?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $heading?></h3>
			  <a href="<?php echo base_url();?>admin/Profile/user_profile_list" class="btn btn-default pull-right">All User List</a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="plan_form" method="post" role="form">
              <div class="box-body">
				<input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id;?>"> 
			  <div id="err_add_plan"></div>
                <div class="form-group">
                  <label for="user_name">User name</label>
                  <input type="text" class="form-control" id="user_name" name="user_name" placeholder="User name" value="<?php echo $user_name;?>">
                </div>
				<div class="form-group">
                  <label for="user_email">User email</label>
                  <input type="text" class="form-control" id="user_email" name="user_email" placeholder="User email" value="<?php echo $user_email;?>">
                </div>
				<div class="form-group">
                  <label for="plan_charge">User Phone no</label>
                  <input type="number" class="form-control"  id="phone_no" min="0" name="phone_no" placeholder="User Phone no" value="<?php echo $phone_no;?>">
                </div>
               <div class="form-group">
					<label class="col-lg-3 control-label"  for="slider_image">User Profile image</label>
					<div class="col-lg-8"  >
						<a id="slider_image_btn" style="text-transform: none;    font-size: 13px;" name="slider_image_btn" class="btn btn-success slider_image_btn" data-toggle="modal" data-target="#browseImage" >Upload image </a>
						
						<input class="form-control slider_image" id="slider_image" name="slider_image" type="hidden" value="<?php echo $profile_picture; ?>">
						<div class="image-preview inline_block">
						<?php if($profile_picture ){
						?>
						
						<img src="<?php echo base_url();?>uploads/<?php echo $profile_picture ?>" style="width:200px;">
						<?php }else{
						?>
						<img src="" style="width:200px;">
						<?php }
						?>
						</div>
					</div>
				</div>
				
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
  						<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
			<div class="modal-body">
				<div id="head1_msg"></div>
				<input type="hidden" value="blog" name="image_cat" class="image_cat">
				<input type="hidden" value="" name="sub_folder_name" class="sub_folder_name">
                <div class="row demo-columns">
					<div class="col-md-8 col-md-offset-2">
						<p>Image type should be GIF,JPG,PNG</p>
						<p>Image should be 2 MB or smaller</p>
						<!-- D&D Zone-->
						<div id="drag-and-drop-zone" class="uploader">
							<div>Drag &amp; Drop Images Here</div>
							<div class="or">-or-</div>
							<div class="browser">
								<label>
									<span>Click to open the file Browser</span>
									<input type="file" name="files[]" multiple="multiple" title='Click to add Files'>
								</label>
							</div>
						</div>
						<!-- /D&D Zone -->
						
					</div>
					<div class="clearfix"></div>
					<!-- / Left column -->
					<div class="col-md-8 col-md-offset-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Uploads</h3>
							</div>
							<div class="panel-body demo-panel-files minHeight" id='demo-files'>
								<span class="demo-note">No Files have been selected/droped yet...</span>
							</div>
						</div>
					</div>
					<!-- / Right column -->
				</div>
			</div><!-- / modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
	   
        </div>
    </div>
</div>
  			  <script>
$('document').ready(function () {
		
	
	
	
	
	/*uploading drag and drop image*/
	$('#drag-and-drop-zone').dmUploader({
        url:'<?php echo base_url();?>admin/profile/upload_image',
        dataType: 'json',
		extraData: {
		  image_cat:$('.image_cat').val(),
		  sub_folder_name:$('.sub_folder_name').val(),
		},
		maxFiles :'1',
		fileName : 'myFile',
		//maxFileSize :'1',
        allowedTypes: 'image/*',
        /*extFilter: 'jpg;png;gif',*/
        onInit: function(){
          $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
        },
        onBeforeUpload: function(id){
          
		  $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

          $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function(id, file){
			$('.demo-panel-files').empty();
			$.danidemo.addFile('#demo-files', id, file);
        },
        onComplete: function(){
          
		  $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
        },
        onUploadProgress: function(id, percent){
          var percentStr = percent + '%';

          $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data){
			var status= data.status;
			var filename_ = data.filename;
			if(status == 200){
				$('.image-preview').find('img').removeClass('display_none');
				$('.image-preview').find('img').attr('src',APP_URL+'uploads/'+filename_);
				$('#slider_image').val(filename_);
				$('#slider_image-error').css("display","none");
				$('#browseImage').modal('hide');
			}else{
                $('#browseImage').modal('hide');
                alert(data.message);
			}
		  $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

          $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

          $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

          $.danidemo.updateFileProgress(id, '100%');
        },
        onUploadError: function(id, message){
			alert(message);
          $.danidemo.updateFileStatus(id, 'error', message);

          $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
        },
        onFileTypeError: function(file){
			alert('File \'' + file.name + '\' cannot be added: must be an image');
			$.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
        },
        onFileSizeError: function(file){
			alert('File \'' + file.name + '\' cannot be added: size excess limit');
			$.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
        },
        /*onFileExtError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
        },*/
        onFallbackMode: function(message){
			alert('info', 'Browser not supported(do something else here!): ' + message);
          $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
        },
		onFilesMaxError: function(file){
			alert(' cannot be added to queue due to upload limits.');
		  $.danidemo.addLog(file.name + ' cannot be added to queue due to upload limits.');
		}
      });
	
});
			  </script>
 <script>
 $('document').ready(function(){

	
	
	
	
	$('#plan_form').validate({
		ignore:[],
        rules: {
           
            slider_image: {
                required: true,
            },
            phone_no: {
                required: true,
            },
			user_email: {
                required: true,
            },
			user_name: {
                required: true,
            }

        },
        messages: {
            
            user_name: {
                required: "User name is required",
            },
            user_email: {
                required: "User email is required",
            },
			phone_no: {
                required: "User phone no select period",
            },
			slider_image: {
                required: "Image is required",
            }
        },
        submitHandler: function (form) {
			$.blockUI({ message: '<h1><img src="'+APP_URL + 'assets/img/loading.gif" /> Just a moment...</h1>' });
			
            var user_id = $('#user_id').val();
            var user_name = $('#user_name').val();
            var user_email = $('#user_email').val();
            var phone_no = $('#phone_no').val();
            var profile_picture = $('#slider_image').val();
           
            $.post(APP_URL + 'admin/Profile/update_user_profile', {
                user_id: user_id,
                profile_picture: profile_picture,
                phone_no: phone_no,
                user_email: user_email,
                user_name: user_name,
                
            },
            function (response) {
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#err_add_plan').empty();
                if (response.status == 200) {
                    $('#err_add_plan').empty();
                    $('#err_add_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_add_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_add_plan').empty();
						window.location.href = APP_URL+'admin/Profile/user_profile_list';
					});
			   } else {
                    $('#err_add_plan').empty();
                    $('#err_add_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_add_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_add_plan').empty();
					});
				}
				
            }, 'json');
            return false;
        }
    });
});
 </script>