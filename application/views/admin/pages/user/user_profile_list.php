

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">User List</h3>
			   <a href="<?php echo base_url();?>admin/profile/index" class="btn btn-default pull-right">Add New User</a>
            </div>
            <!-- /.box-header -->
			  <div class="box-body">
			  <div id="err_dlt_plan" ></div>
				<table align="left" class="table table-hover">
					<thead>
						<tr>
							<th class="text-center"> S. No</th>
							<th class="text-center"> User Name</th>
							<th class="text-center"> User Email</th>
							<th class="text-center"> User Phone no</th>
							<th class="text-center"> Edit Password</th>
							<th class="text-center"> Action </th>
						</tr>
					</thead>
					<tbody>
		
					<?php $i=1;
					if($profile_list !=''){
						foreach($profile_list as $value){  ?>
							<tr>
							<td class="text-center"><?php echo $i;?></td>
							<td class="text-center"><?php echo $value['user_name'];?></td>
							<td class="text-center"><?php echo $value['user_email'];?></td>
							<td class="text-center"><?php echo $value['phone_no'];?></td>
							<td class="text-center"><a href="#" class="edit_module" data-toggle="modal" data-target="#browseNewModule" id='editpass'  value="<?php echo $value['user_id'];?>"> &nbsp;<i class="fa fa-edit"></i></a></td>
						    <td class="text-center"><a href="<?php echo base_url();?>admin/profile/index?user_id=<?php echo $value['user_id'];?>&action=edit" class="label bg-primary " data-toggle="tooltip" title="Edit" ><i class="fa fa-edit"></i></a>
							<a href="javascript:void(0);" class="define_access" name="<?php echo $value['user_id']; ?>" value=""><span class="label label-info">Define Access</span></a>
							<a href="#" value="<?php echo $value['user_id'];?>" class="label bg-red remove_plan" data-toggle="tooltip" title="Remove"  ><i class="fa fa-trash"></i></a> </td>
							</tr>
							<?php	$i++;
						}
					}else{ ?>
						<tr><td style="color: red;" colspan="6" class="text-center"><?php echo 'No data Found In database';?></td></tr>
					<?php }?>
					</tbody>
				</table>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <div class="modal fade" id="browseNewModule" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Edit Password</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well" id="menu_form" method="post" enctype="multipart/form-data">
					<input class="form-control" id="query_id2" name="query_id2"  value="" type="hidden">
					  Password <input type="text" class="form-control" id="password" name="password" placeholder="Password" />
					
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" aria-hidden="true" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>
 <!---------------------------- Modal for Browse Access level-------------------------->
<div class="modal fade" id="browseAccessModal" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Update Employee Access</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form id="access_define_form" method="post" class="form-horizontal">
						<input type="hidden" name="emp_id2" id="emp_id2" value="0">
						<div class="clearfix"></div>
						<div class="form-group col-md-12">
							<label class="control-label col-md-3" for="emp_name2">Employee Name<span class="required">*</span></label>
							<div class="col-md-9">
								<span class="form-control" id="emp_name2" name=""></span>
							</div>
						</div>
						<div class="form-group col-md-12">
							<label class="control-label col-md-3" for="access_define">Access Define<span class="required">*</span></label>
							<div class="col-md-9">
								<div class="">
									<?php
									if($module_list){
											$content = '';
											foreach($module_list as $menu){
												$status = 0;
												$content2 = '';
												/*checking this menu has submenu or not*/
												
												if($submodule_list){
													$emp_access_detail =false;
													foreach($submodule_list as $submenu){
														
														if($submenu['FK_module_id']==$menu['module_id']){
															$content2 .= '<label class="checkbox-inline">';
															/*for edit case*/
															$checked = '';
															if($emp_access_detail){
																foreach($emp_access_detail as $access_define){
																	if($access_define['module_id']==$menu['module_id'] && $access_define['submodule_id']==$submenu['submodule_id']){
																		$checked = 'checked';
																	}
																}
															}
															/*end edit case*/	
															$content2 .= '<input '.$checked.' type="checkbox" name="submenu" manu="'.$submenu['FK_module_id'].'" class="submenu" value="'.$submenu['submodule_id'].'" ><span>'.$submenu['submodule_name'].'</span>';
															$content2 .= '</label>';
															$status = 1;
														}
													}
												}
												if($status==1){
													$content .= '<div class="row access_define_row mrgBot10">';
													$content .= '<span class="menu" name="'.$menu['module_id'].'"><strong>'.$menu['module_name'].'</strong></span>';
													$content .= '<div class="mrgBot10">';
													$content .= $content2;
													$content .= '</div>';
													$content .= '</div>';
													$status = 0;
												}
											}
											echo $content;
										}
										
									
										echo '</div>';
									?>
									
								</div>
								<div class="submenu-error"></div>
							</div>
						</div>
						<div class="clearfix"></div>
                        
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
					
				</div>
			</div>
        </div>
    </div>
</div>

 
 
 
 
 <script>
 $('document').ready(function(){
	
	  
  $('body').on('click','.edit_module',function(){
 		var organisation_id = $(this).attr('value');
 		//var password = $(this).attr('status');
 		$('#query_id2').val(organisation_id);
 		//$('#password').val(password);	
 
 	});  
  $('#menu_form').validate({
			rules: {
				password: {
					required: true,
				},
			},
			messages: {        
				status: {
					required: "password is required",
				},
			},
 		submitHandler: function (form) {
			$.blockUI();
			//$('#menu_form').find('button[type="submit"]').prop('disabled',true);
			var user_id = $('#query_id2').val();
			var password = $('#password').val();
           
            $.post(APP_URL + 'admin/profile/update_user_password', {
            	user_id: user_id,
                password: password,
            },
			function (response) {
					$.unblockUI();
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#err_dlt_plan').empty();
					if (response.status == 200) {
						$('#err_dlt_plan').empty();
						$('#browseNewModule').modal('hide');
						$('#err_dlt_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
							$('#err_dlt_plan').empty();
							//window.location.href = APP_URL+'account/event/user_organisation_list';
						});

				   } else {
						$('#err_dlt_plan').empty();
						$('#err_dlt_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
							$('#err_dlt_plan').empty();
						});
					}
					
				}, 'json');
		return false;
		}
	});

 //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit emp
     */

    $('body').on('click', '.define_access', function () {
		$('input[type="checkbox"]').prop('checked',false);
        $.blockUI();
		$('#headerMsg').empty();
        $('#add_emp_form label.error').empty();
        $('#add_emp_form input.error').removeClass('error');
        $('#add_emp_form select.error').removeClass('error');
		$('#emp_id2').val(0);
        $('#emp_name2').text('');
		$('input[type="checkbox"].submenu').prop('checked',false);
        var emp_id = $(this).attr('name');
        var emp_name = $(this).closest('tr').find('td:eq(1)').text();
		
        $('#emp_id2').val(emp_id);
        $('#emp_name2').text(emp_name);
		
		/*Now Fetching emp access detail*/
	   $.post(APP_URL+'admin/account/get_emp_accessibility_detail',{
			emp_id : emp_id,
		},function(response){
			$.unblockUI();
			if(response.status==200){
				$(response.data).each(function(key,val){
					var module_id = val['module_id'];
					var submodule_id = val['submodule_id'];
					$('input[type="checkbox"][manu="'+module_id+'"][value="'+submodule_id+'"]').prop('checked',true);
				});
			}else{
				
				return false;
			}
		},'json');
		/*now opening the modal*/
		$('#browseAccessModal').modal('show');
    });
	
 
 //-----------------------------------------------------------------------
    /* 
     * validation of access_define
     */
	$('#access_define_form').validate({
		ignore: [],
        rules: {
            submenu: {
                required: true,
            },
        },
		 messages: {
			submenu: {
                required: "At least one Sub Menu is required.",
            },
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('submenu')) {
					error.insertAfter(element.closest('div.form-group').find('.submenu-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			$.blockUI();
			$('#access_define_form').find('button[type="submit"]').prop('disabled',true);
			var emp_id = $('#emp_id2').val();
			var access_define = [];
			$('input[type="checkbox"]:checked').each(function(){
				console.log($(this).parent().find('span').text());
				access_define.push({
					module_id : $(this).attr('manu'),
					submodule_id : $(this).val(),
				});
			});
			/*
			var subject_list = [];
			$('.checked_category:checked').each(function(key,value){
				var subject_id = $(this).attr('category_id');
				subject_list[key] = subject_id;
			});
			console.log(subject_list);
			*/
            $.post(APP_URL + 'admin/account/update_accessibility', {
                emp_id: emp_id,
                access_define: access_define,
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status == 200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'>  <button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong></div>");
                    $("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
						window.location.href = APP_URL+'admin/profile/user_profile_list';
					});
				} else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                    $("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					}); 
                }				
				$.unblockUI();
				$('#access_define_form').find('button[type="submit"]').prop('disabled',false);
				$('#browseAccessModal').modal('hide');
			}, 'json');
		return false;
		},
	});
	
 
	$('body').on('click', '.remove_plan',function(){
		var user_id = $(this).attr('value');
		$(this).closest('tr').remove();	
		
		 $.post(APP_URL + 'admin/profile/remove_user_profile', {
                user_id: user_id,
            },
            function (response) {
				$('#err_dlt_plan').empty();
                if (response.status == 200) {
                    $('#err_dlt_plan').empty();
                    $('#err_dlt_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
						window.location.href = APP_URL+'admin/Profile/user_profile_list';
					});
			   } else {
                    $('#err_dlt_plan').empty();
                    $('#err_dlt_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
					});
				}				
            }, 'json');
			
	});
 //------------------------------------------------------------------------
    /*
     * This script is used to empty the model  when click on add new emp
     */
    $('body').on('click', '.addNewEmp', function () {
		
		$('#emp_id').val(0);
        $('#emp_name').val('');
        $('#emp_email').val('');
		$('#emp_email').prop('disabled',false);
        $('#phone_no').val('');
		$('#passwd').parent().parent().removeClass('display_none');
		$('#conf_passwd').parent().parent().removeClass('display_none');
		
	});
 
 
 //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit emp
     */

    $('body').on('click', '.edit_emp', function () {
        $('#headerMsg').empty();
        $('#add_emp_form label.error').empty();
        $('#add_emp_form input.error').removeClass('error');
        $('#add_emp_form select.error').removeClass('error');

        var user_id = $(this).attr('name');
        var user_name = $(this).closest('tr').find('td:eq(1)').text();
        var user_email = $(this).closest('tr').find('td:eq(2)').text();
        var phone_no = $(this).closest('tr').find('td:eq(3)').text();
        
        $('#emp_id').val(user_id);
        $('#emp_name').val(user_name);
        $('#emp_email').val(user_email);
		$('#emp_email').prop('disabled',true);
        $('#phone_no').val(phone_no);
		$('#passwd').parent().parent().addClass('display_none');
		$('#conf_passwd').parent().parent().addClass('display_none');
        
    });
 
});
 </script>