<style>
.marBot20{
	margin-bottom:20px;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

	
	<?php
	$heading = '';
	if($plan_data !=''){
		$heading = 'Edit Plan';
		$plan_id =$plan_data[0]['plan_id']; 
		$plan_name =$plan_data[0]['plan_name']; 
		$plan_charge =$plan_data[0]['plan_charge']; 
		$plan_discount =$plan_data[0]['plan_discount'];
		$plan_days =$plan_data[0]['plan_days'];
		$plan_actual_money =$plan_data[0]['plan_actual_money'];
		$plan_tag =$plan_data[0]['plan_tag'];
		
	}else{
		$heading = 'Add New Plan';
		$plan_id =0;
		$plan_name ='';
		$plan_charge ='';
		$plan_discount =0;
		$plan_days ='';
		$plan_actual_money =0;
		$plan_tag ='';
	}
		
	?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $heading?></h3>
			  <a href="<?php echo base_url();?>admin/plan/plan_list" class="btn btn-default pull-right">All Plans</a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="plan_form" method="post" role="form">
              <div class="box-body">
				<input type="hidden" class="form-control" id="plan_id" name="plan_id" value="<?php echo $plan_id;?>"> 
			  <div id="err_add_plan"></div>
                <div class="form-group">
                  <label for="plan_name">Plane name</label>
                  <input type="text" class="form-control" id="plan_name" name="plan_name" placeholder="Plane Name" value="<?php echo $plan_name;?>">
                </div>
				<div class="form-group">
                  <label for="plan_tag">Tag line</label>
                  <input type="text" class="form-control" id="plan_tag" name="plan_tag" placeholder="Tag Line" value="<?php echo $plan_tag;?>">
                </div>
				<div class="form-group">
                  <label for="plan_charge">Plan charges</label>
                  <input type="number" class="form-control"  id="plan_charge" min="0" name="plan_charge" placeholder="Plan charges" value="<?php echo $plan_charge;?>">
                </div>
                <div class="form-group">
                  <label for="plan_discount">Discount (In %)</label>
                  <input type="number" class="form-control" id="plan_discount" min="0" max="100" name="plan_discount" placeholder="Discount" value="<?php echo $plan_discount;?>">
                <p> Prize after discount is =<span class="total_amount" ><?php echo $plan_actual_money;?></span></p>
				</div>
				<div class="form-group">
                  <label for="plan_days">Plane duration</label>
				  <select class="form-control" id="plan_days" name="plan_days">
					  <option value="" >Select No of Days</option>
					  <option value="30" <?php if($plan_days==30){echo 'selected';}?> >30 Days</option>
					  <option value="90" <?php if($plan_days==90){echo 'selected';}?> >90 Days</option>
					  <option value="120" <?php if($plan_days==120){echo 'selected';}?> >120 Days</option>
					  <option value="120" <?php if($plan_days==180){echo 'selected';}?> >180 Days</option>
					  <option value="360" <?php if($plan_days==360){echo 'selected';}?> >360 Days</option>
				  </select>
                </div>
				<div class="form-group">
					<label for="plan_feature">Features</label>
					<div class="col-md-12">
						<input id="addProSpeci" class="btn btn-success" type="button" value="&#43" />
						<br />
						<br />
						<!--inputs will be added here -->
						<div id="TextBoxContainer2" style="margin-left: 50px;">
							<?php 
							if($plan_feature !=''){
								foreach($plan_feature as $speci){
									?>
									<div class="remove-added-para">
										<div class="col-md-8 marBot20">
											<input type="text" class="form-control plan_feature" id="" name="plan_feature" placeholder="Specification  Name" value="<?php echo $speci['plan_feature'];?>">
										</div>
										<div class="col-md-2 marBot20">
											<button class="btn btn-warning removepara radius50">&times;</button>
										</div>
									</div>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
 $('document').ready(function(){
	 
	$("body").on("keyup",'#plan_discount', function () { 
		var plan_ = $('#plan_charge').val();
		var plan_2 = $('#plan_discount').val();
		var ALL_amount = plan_ -(plan_2 * plan_/100);
		$('.total_amount').html(ALL_amount);
	});
	$("body").on("keyup",'#plan_discount', function () { 
		var plan_ = $('#plan_charge').val();
		var plan_2 = $('#plan_discount').val();
		var ALL_amount = plan_ -(plan_2 * plan_/100);
		$('.total_amount').html(ALL_amount);
	});
	 	//------------------------------------------------------------------------//
    /**
     * This part of script is used to add the text box for add section.
     */
	$("body").on("click",'#addProSpeci', function () {
		var div = $("<div class='remove-added-para'>");
		div.html(GetDynamicTextBoxforSpeci());
		$("#TextBoxContainer2").append(div);
	});
	$("body").on("click", ".removepara", function () {
		$(this).parent().parent().remove();
	});
	
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to add new text box for add event
     */
	 function GetDynamicTextBoxforSpeci() {
		var content = '<div class="col-md-12" >\n\
						<div class="col-md-2"><label>Sister</label></div>\n\
						<div class="col-md-4"><input type="text" name="father" ></div>\n\
						<div class="col-md-4"><input type="text" name="father" ></div>\n\
                        <div class="col-md-2"><input type="text" name="father" style="width: 100%;"></div>\n\
                         </div>\n\
                         </div>';
		return 	content;			
	}
	
	$('#plan_form').validate({
        rules: {
            plan_name: {
                required: true
            },
            plan_charge: {
                required: true,
            },
            plan_discount: {
                required: true,
            },
			plan_days: {
                required: true,
            },
			plan_tag: {
                required: true,
            }

        },
        messages: {
            plan_name: {
                required: "Name is required"
            },
            plan_charge: {
                required: "Charges is required",
            },
            plan_discount: {
                required: "Discount is required",
            },
			plan_days: {
                required: "please select period",
            },
			plan_tag: {
                required: "Tag Line is required",
            }
        },
        submitHandler: function (form) {
			$.blockUI({ message: '<h1><img src="'+APP_URL + 'assets/img/loading.gif" /> Just a moment...</h1>' });
			
            var plan_id = $('#plan_id').val();
            var plan_name = $('#plan_name').val();
            var plan_charge = $('#plan_charge').val();
            var plan_discount = $('#plan_discount').val();
            var plan_days = $('#plan_days').val();
            var plan_tag = $('#plan_tag').val();
            var plan_actual_money = plan_charge -(plan_discount * plan_charge/100);
			
			var pro_speci = [];
			$('#TextBoxContainer2 .remove-added-para').each(function(key,val){
				pro_speci.push({
					plan_feature : $(this).find('.plan_feature').val(),
				}); 
			});
                  
			console.log(pro_speci);
			if(pro_speci.length==0){
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#err_add_plan').empty();
                    $('#err_add_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>Please add type with click + icon.</strong></div>");
					$("#err_add_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_add_plan').empty();
					});
				return false ;
			}
            
				  
            $.post(APP_URL + 'admin/plan/update_plans', {
                plan_id: plan_id,
                plan_name: plan_name,
                plan_charge: plan_charge,
                plan_discount: plan_discount,
                plan_days: plan_days,
                plan_tag: plan_tag,
                plan_actual_money: plan_actual_money,
                pro_speci: pro_speci,
            },
            function (response) {
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#err_add_plan').empty();
                if (response.status == 200) {
                    $('#err_add_plan').empty();
                    $('#err_add_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_add_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_add_plan').empty();
						window.location.href = APP_URL+'admin/plan/plan_list';
					});

			   } else {
                    $('#err_add_plan').empty();
                    $('#err_add_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_add_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_add_plan').empty();
					});
				}
				
            }, 'json');
            return false;
        }
    });
});
 </script>