
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Plan List</h3>
			   <a href="<?php echo base_url();?>admin/plan/index" class="btn btn-default pull-right">Add New Plan</a>
            </div>
            <!-- /.box-header -->
			  <div class="box-body">
			  <div id="err_dlt_plan" ></div>
				<table align="left" class="table table-hover">
					<thead>
						<tr>
							<th class="text-center"> S. No</th>
							<th class="text-center"> Plan Name</th>
							<th class="text-center"> Plan Price</th>
							<th class="text-center"> Plan Discount (%)</th>
							<th class="text-center"> Plan duration (Days)</th>
							<th class="text-center"> Action </th>
						</tr>
					</thead>
					<tbody>
					<?php $i=1;
					if($plan_list !=''){
						foreach($plan_list as $value){  ?>
							<tr>
								<td class="text-center"><?php echo $i;?></td>
								<td class="text-center"><?php echo $value['plan_name'];?></td>
								<td class="text-center"><?php echo $value['plan_charge'];?></td>
								<td class="text-center"><?php echo $value['plan_discount'];?></td>
								<td class="text-center"><?php echo $value['plan_days'];?></td>
								<td class="text-center"><a href="<?php echo base_url();?>admin/plan/index?plan_id=<?php echo $value['plan_id'];?>&action=edit" class="label bg-primary " data-toggle="tooltip" title="Edit" ><i class="fa fa-edit"></i></a>
								&nbsp;<a href="#" value="<?php echo $value['plan_id'];?>" class="label bg-red remove_plan" data-toggle="tooltip" title="Remove"  ><i class="fa fa-trash"></i></a> </td>	
							</tr>
							<?php	$i++;
						}
					}else{ ?>
						<tr><td style="color: red;" colspan="6" class="text-center"><?php echo 'No data Found In database';?></td></tr>
					<?php }?>
					</tbody>
				</table>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
 $('document').ready(function(){

	$('body').on('click', '.remove_plan',function(){
		var plan_id = $(this).attr('value');
		$(this).closest('tr').remove();	
		
		 $.post(APP_URL + 'admin/plan/remove_plans', {
                plan_id: plan_id,
            },
            function (response) {
				$('#err_dlt_plan').empty();
                if (response.status == 200) {
                    $('#err_dlt_plan').empty();
                    $('#err_dlt_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
						window.location.href = APP_URL+'admin/plan/plan_list';
					});
			   } else {
                    $('#err_dlt_plan').empty();
                    $('#err_dlt_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
					});
				}
				
            }, 'json');
			
	});
 
});
 </script>