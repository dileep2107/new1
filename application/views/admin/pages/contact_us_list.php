
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Contact Us List</h3>
			</div>
            <!-- /.box-header -->
			  <div class="box-body">
			  <div id="headerMsg" ></div>
				<table align="left" class="table table-hover">
					<thead>
						<tr>
							<th class="text-center">  S. No.  </th>
							<th class="text-center">  Name </th>
							<th class="text-center">  Email  </th>
							<th class="text-center">  Message  </th>
							<th class="text-center">  Date  </th>
							<th class="text-center">  Action </th>
						</tr>
					</thead>
					<tbody>
		
					<?php
			var_dump($contact_lists);
						$i = 1;
						if ($contact_lists == 0) {
							echo 'No record found into database';
						  } 
						else {
							$content = '';
							foreach ($contact_lists as $value) {
						$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
						$content .= '<td class="text-center">' . $value['contact_name'] . '</td>';
						$content .= '<td class="text-center">' . $value['contact_email'] . '</td>';
						$content .= '<td class="text-center">' . $value['contact_subject'] . '</td>';
						$content .= '<td class="text-center">' . $value['contact_message'] . '</td>';
						$content .= '<td class="text-center">' . $value['date'] . '</td>';
						$content .= '<td class="text-center">' . $value['contact_status'] . '</td>';
						$content .= '<td class="text-center"><a href="" class="remove_image label bg-red remove_plan" data-toggle="tooltip" name=' . $value['contact_id'] . ' ><i class="fa fa-trash"></i></a></td></tr>';
					$i++;
				}
				echo $content;
			}
			
			?>
					</tbody>
				</table>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
$('document').ready(function(){
	$('body').on('click', '.remove_image', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
		//$.blockUI();
        var contact_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'admin/configure_access/remove_contact_us', {contact_id: contact_id}, function (response) {
            $('#headerMsg').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                $('.remove_image[name=' + contact_id + ']').closest("tr").remove();
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					window.location.reload();
				});
            } else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
        }, 'json');
		//$.unblockUI();
        return false;
    });

});
</script>
