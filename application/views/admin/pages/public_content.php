<script src="https://cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>

<?php
	$content_id = 0;
	$content_cat_ = '';
	if(isset($_GET['content_cat_id'])){
		$content_id = explode('-',$_GET['content_cat_id'])[0];
		$content_cat_ = explode('-',$_GET['content_cat_id'])[1];
	}
	
	if($content_detail){
		$content_id = $content_detail[0]['content_id'];
		$content = $content_detail[0]['content'];
	}else{
		$content_id = 0;
		$content = '';
	}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
           <div class="box-header with-border">
        <h1 style="display: inline;">Public Content</h1>
		
		</div>
            <!-- /.box-header -->
			  <div class="box-body">
			  	<div id="headMsg"></div>
			  		<form id="add_content_form" method="post" class="form-horizontal">
							<input type="hidden" name="content_id" id="content_id" value="<?php echo $content_id;?>">
							<div class="form-group">
								<label class="control-label col-md-3" for="content">Content Category<span class="required">*</span></label>
								<div class="col-md-6">
									<select class="form-control content_cat_" id="content_cat_" name="content_cat_">
										<option value="">Select Category</option>
										<option name="1" value="About Us" <?php if($content_id==1) echo 'selected';?>>About Us</option>
										<option name="2" value="Terms and Services" <?php if($content_id==2) echo 'selected';?>>Terms and Services</option>
										<option name="3" value="Privacy Policy" <?php if($content_id==3) echo 'selected';?>>Privacy Policy</option>
										<option name="4" value="Contact US" <?php if($content_id==4) echo 'selected';?>>Contact US</option>
									</select>
								</div>
							</div>	
							
							<div class="form-group">

								<label class="control-label col-md-3" for="content">Content<span class="required">*</span></label>
								<div class="col-md-9">
									<textarea rows="10" class="form-control ckeditor1 content" id="content" name="content" placeholder="Content"><?php echo $content;?></textarea>
									<div class="content-error"></div>
								</div>
							</div>	
							<div class="form-group">
								 <div class="input-group col-md-6 col-md-offset-3" style="margin-top: 20px;"> 
									<button id="btn-signup" type="submit" class="btn btn-primary">Submit</button>
								 </div>
							</div>	
						</form>
						

			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 

<style>
.page-title a{	margin-top: 11px;background: none;margin-right: 10px;}
.page-title h1 {display: inline-block;float: left;}
.page-title{
	margin-bottom: 50px;
}
.inline_block{
	display: inline-block !important;
}
.img-preview{
	margin-left: 20px;
    width: 150px;
	border: 1px solid;
}
.mrgBot50{
	margin-bottom:50px;
}
</style>
	
		
			
		
<script>
	
	CKEDITOR.replace( 'content', {
			language: 'de'
		} );



$('document').ready(function () {
	
	/**
	 * This script is used to save the data from add_content_form in db
	 */
	 $('#add_content_form').validate({
		ignore: [],
		rules: {
			content_cat: {
				required: true,
			},
			content: {
				required: function(textarea) {
					  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
					  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
					  return editorcontent.length === 0;
				  }
			},
		},
		messages: {
			content_cat: {
				required: "Category is required"
			},
			content: {
				required: "Content is required"
			},
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			$.blockUI();
			console.log('start');
			var content_id = $('#content_id').val();
			//var content_cat_id = $('#content_cat').val();
			var content_head = $('#content_cat_ option:selected').attr('value');
			var content = $('textarea.ckeditor1').val();
			console.log(content);
			
			//console.log( $('#content').html(data));
			//console.log($('textarea').ckeditor());
			//return  false;
			$.post(APP_URL+'admin/configure_access/update_public_content',{
				content_id: content_id,
				content_cat_id: content_id,
				content: content,
				content_head: content_head,
			},function(response){
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headMsg').empty();
                if (response.status == 200) {
					message = response.message;
					if(content_id!=0){
						message = " Public Content has been updated successfully!"; 
					}
					$('#headMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headMsg').empty();
						window.location.reload();
					});
				}
                else if (response.status == 201) {
                    $('#headMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headMsg').empty();
					});
                }
           },'json');
		   $.unblockUI();
			return  false;
		}
	});
	 
	//---------------------------------------------------------------------
    /*
     * This script is used to remove blog  from the list
     */
	$('body').on('change', '.content_cat_', function () {
        if($(this).val==''){
			return false;
		}
		$.blockUI();
		var content_cat = $(this).val();
		var content_cat_id = $(this).find('option:selected').attr('name');
		var url = APP_URL+'admin/configure_access/public_content?content_cat_id='+content_cat_id+'-'+content_cat;
		window.location.href= url;
		
	});
	
	
});
</script>

