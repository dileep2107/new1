
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>


<script>
  $('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
  });
</script>
<style>
.marBot20{
  margin-bottom:20px;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

  
  <?php
  $heading = ''; 
 if($template_data !=''){
    $heading = 'Edit Email Template';
    $email_template_id =$template_data[0]['email_template_id']; 
    $email_template_heading =$template_data[0]['email_template_heading']; 
    $email_template_subject =$template_data[0]['email_template_subject']; 
    $email_template_body =$template_data[0]['email_template_body']; 

  }else{
    $heading = 'Add Email Template';
    $email_template_id =0;
    $email_template_heading ='';
    $email_template_subject ='';
    $email_template_body ='';

  }

  ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $heading?></h3>
        <a href="<?php echo base_url();?>admin/email_template/email_template_list" 
          class="btn btn-default pull-right">All Email Template List</a>
            </div>
            <!-- /.box-header -->
            <div id="err_email_keyword"></div>
            <!-- form start -->
            <form id="email_keyword_form" method="post" role="form">
              <div class="box-body">
                <input type="hidden" class="form-control" id="email_template_id" name="email_template_id" value="<?php echo $email_template_id; ?>" > 

      
                <div class="form-group">
                  <label for="email_template_heading">Email Template Heading</label>
                  <input type="text" class="form-control" id="email_template_heading" name="email_template_heading" value="<?php echo $email_template_heading; ?>" placeholder="Email Template Heading">
                </div>
        
                <div class="form-group">
                  <label for="email_template_subject">Email Template Subject</label>
                  <input type="text" class="form-control" id="email_template_subject" name="email_template_subject" value="<?php echo $email_template_subject; ?>" placeholder="Email Template Subject">
                </div>

                  <div class="form-group">
                    <label for="comment">Email Template Body</label>
                    <textarea class="form-control ckeditor email_template_body" rows="5" id="email_template_body"
                     name="email_template_body" placeholder="Email Temple Body" ><?php echo $email_template_body;?>
                   </textarea>
                   <div class="email_template_body-error"></div>
                  </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
 $('document').ready(function(){
   
  /*validation email keywrd form */
  $('#email_keyword_form').validate({
        rules: {
            email_template_heading: {
                required: true
            },
            email_template_subject: {
                required: true,
            },

            email_template_body:{
              required:function(textarea) {
                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                return editorcontent.length === 0;
              },
            }
        },
        messages: {
            email_template_heading: {
                required: "Email Template Headind is required"
            },
            email_template_subject: {
                required: "Email Template Subject is required",
            },

            email_template_body: {
                required: "Email Template Body is required",
            },

        },
        errorPlacement: function(error, element) {
                if (element.hasClass('email_template_body')) {
              error.insertAfter(element.closest('div.form-group').find('.email_template_body-error'));
          }else  {
                    error.insertAfter(element);
                }
        },
        submitHandler: function (form) {
          //$.blockUI({ message: '<h1><img src="'+APP_URL + 'assets/img/loading.gif" /> Just a moment...</h1>' });
        
            var email_template_id = $('#email_template_id').val();
            var email_template_heading = $('#email_template_heading').val();
            var email_template_subject = $('#email_template_subject').val();
            var email_template_body =  CKEDITOR.instances['email_template_body'].getData();
            

            $.post(APP_URL + 'admin/email_template/update_new_email_template', {
                email_template_id: email_template_id,
                email_template_heading: email_template_heading,
                email_template_subject: email_template_subject,
                email_template_body: email_template_body,
       

            },
            function (response) {
        $.unblockUI();
        $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_email_keyword').empty();
                if (response.status == 200) {
                    $('#err_email_keyword').empty();
                    $('#err_email_keyword').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
              $("#err_email_keyword").fadeTo(2000, 500).slideUp(500, function(){
                $('#err_email_keyword').empty();
                window.location.href = APP_URL+'admin/email_template/email_template_list';
              });

         } else {
                    $('#err_email_keyword').empty();
                    $('#err_email_keyword').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
          $("#err_email_keyword").fadeTo(2000, 500).slideUp(500, function(){
            $('#err_email_keyword').empty();
          });
        }
        
            }, 'json');
            return false;
        }
    });
});
 </script>