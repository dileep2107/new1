<style>
.marBot20{
  margin-bottom:20px;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

  
  <?php
  $heading = ''; 
  if($email_data !=''){
    $heading = 'Add Email keywords';
    $email_keyword_id =$email_data[0]['email_keyword_id']; 
    $email_keyword_name =$email_data[0]['email_keyword_name']; 
    $email_keyword_meaning =$email_data[0]['email_keyword_meaning']; 

   }else{
    $heading = 'Edit Email keywords';
    $email_keyword_id =0;
    $email_keyword_name ='';
    $email_keyword_meaning ='';
  }
    
  ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $heading?></h3>
        <a href="<?php echo base_url();?>admin/email_template/email_keyword_list" class="btn btn-default pull-right">All Keyword List</a>
            </div>
            <!-- /.box-header -->
            <div id="err_email_keyword"></div>
            <!-- form start -->
            <form id="email_keyword_form" method="post" role="form">
              <div class="box-body">
                <input type="hidden" class="form-control" id="email_keyword_id" name="email_keyword_id" value="<?php echo $email_keyword_id;?>" > 

      
                <div class="form-group">
                  <label for="email_keyword_name">Email Keyword</label>
                  <input type="text" class="form-control" id="email_keyword_name" name="email_keyword_name" placeholder="Email Keyword" value="<?php echo $email_keyword_name;?>">
                </div>
        
                <div class="form-group">
                  <label for="email_keyword_meaning">Email keyword meaning</label>
                  <input type="text" class="form-control" id="email_keyword_meaning" name="email_keyword_meaning" placeholder="Email keyword meaning" value="<?php echo $email_keyword_meaning;?>">
                </div>

                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
 $('document').ready(function(){
   
  /*validation email keywrd form */
  $('#email_keyword_form').validate({
        rules: {
            email_keyword_name: {
                required: true
            },
            email_keyword_meaning: {
                required: true,
            },
        },
        messages: {
            email_keyword_name: {
                required: "Email keyword is required"
            },
            email_keyword_meaning: {
                required: "Keyword meaning is required",
            },
        },
        submitHandler: function (form) {
          //$.blockUI({ message: '<h1><img src="'+APP_URL + 'assets/img/loading.gif" /> Just a moment...</h1>' });
      
            var email_keyword_id = $('#email_keyword_id').val();
            var email_keyword_name = $('#email_keyword_name').val();
            var email_keyword_meaning = $('#email_keyword_meaning').val();

            $.post(APP_URL + 'admin/email_template/update_email_keyword', {
                email_keyword_id: email_keyword_id,
                email_keyword_name: email_keyword_name,
                email_keyword_meaning: email_keyword_meaning,
            },
            function (response) {
        $.unblockUI();
        $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_email_keyword').empty();
                if (response.status == 200) {
                    $('#err_email_keyword').empty();
                    $('#err_email_keyword').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
              $("#err_email_keyword").fadeTo(2000, 500).slideUp(500, function(){
                $('#err_email_keyword').empty();
                window.location.href = APP_URL+'admin/email_template/email_keyword_list';
              });

         } else {
                    $('#err_email_keyword').empty();
                    $('#err_email_keyword').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
          $("#err_email_keyword").fadeTo(2000, 500).slideUp(500, function(){
            $('#err_email_keyword').empty();
          });
        }
        
            }, 'json');
            return false;
        }
    });
});
 </script>