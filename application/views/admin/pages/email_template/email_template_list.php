
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Email List</h3>
			   <a href="<?php echo base_url();?>admin/email_template/new_email_template" class="btn 
			   	 btn-default pull-right">Add New Template</a>
            </div>
            <!-- /.box-header -->
			  <div class="box-body">
			  <div id="err_dlt_plan" ></div>
				<table align="left" class="table table-hover">
					<thead>
						<tr>
							<th class="text-center"> S. No</th>
							<th class="text-center">Email Template Heading</th>
							<th class="text-center">Email Template Subject</th>
							<th class="text-center"> Action </th>
						</tr>
					</thead>
					<tbody>
					<?php $i=1;
					if($email_template_list !=''){
						foreach($email_template_list as $value){  ?>
						<tr>
							<td class="text-center"><?php echo $i;?></td>
							<td class="text-center"><?php echo $value['email_template_heading'];?></td>
							<td class="text-center"><?php echo $value['email_template_subject'];?></td>
							<td class="text-center"><a href="<?php echo base_url();?>admin/email_template/new_email_template?email_template_id=<?php echo $value['email_template_id'];?>&action=edit" class="label bg-primary " data-toggle="tooltip" title="Edit" ><i class="fa fa-edit"></i></a>
								&nbsp;<a href="#" value="<?php echo $value['email_template_id'];?>" class="label bg-red remove_email_template" data-toggle="tooltip" title="Remove"  ><i class="fa fa-trash"></i></a> </td>	
							</tr>
							<?php	$i++;
						}
					}else{ ?>
						<tr><td style="color: red;" colspan="4" class="text-center"><?php echo 'No data Found In database';?></td></tr>
					<?php }?>
					</tbody>
				</table>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
 $('document').ready(function(){

	$('body').on('click', '.remove_email_template',function(){
		console.log('>>>>>>>>>>>remove_email_template');
		var email_template_id = $(this).attr('value');
		$(this).closest('tr').remove();	
		
		 $.post(APP_URL + 'admin/email_template/remove_email_template', {
                email_template_id: email_template_id,
            },
            function (response) {
				$('#err_dlt_plan').empty();
                if (response.status == 200) {
                    $('#err_dlt_plan').empty();
                    $('#err_dlt_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
               	$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
                window.location.href = APP_URL+'admin/email_template/email_template_list';
					});

			   }
                else {
                    $('#err_dlt_plan').empty();
                    $('#err_dlt_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
					});
				}
				
            }, 'json');
			
	});
 
});
 </script>