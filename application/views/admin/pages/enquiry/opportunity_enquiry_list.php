


<style>
.error,.required{
	color:red;
}
</style>

			
<style>
#headerMsg{
	margin:20px 0px;
}
.dataTables-example th{
	text-align:center;
}
.display_none{
	display:none;
}
th{text-align:center;}

</style>

<div class="content-wrapper animated fadeInRight">
<div class="content">
	<div class="row">
	<div id="err_contact_us_form"></div>
		<div class="col-lg-12">
		<div class="box ">
		
		<div class="ibox float-e-margins">
			<div class="box-header with-border">
				  <h3 class="box-title"> Enquiry </h3>
			
			</div>
		
			<div class="ibox-content">
		
			<div id="remove_enquiry"></div>
			
			<div class="table-responsive">
			<table id="contact_table" class="table table-striped table-bordered table-hover" >
			<thead>
			<tr>
				<th> S. No.</th>
				<th> Name</th>
				<th>Contact Number</th>
				<th>email</th>
				<th>Message</th>
				<th>Added Date</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 1;
			if ($enquiries_list == 0) {
				echo 'No record found into database';
			} else {
				
				$content = '';
				foreach ($enquiries_list as $value) {
					$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
				
					$content .= '<td class="text-center">' . $value['name'] . '</td>';
					$content .= '<td class="text-center">' . $value['number'] . '</td>';
					$content .= '<td class="text-center">' . $value['email'] . '</td>';
					$content .= '<td class="text-center">' . $value['message'] . '</td>';
					$content .= '<td class="text-center">' . $value['date'] . '</td>';
					
					$content .= '<td class="text-center">';
					$content .= '&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_enquiry"  name=' . $value['enquiries_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
					$i++;
				}
				echo $content;
			}
			?>
			
			</tbody>
			
			</table>
				</div>

			</div>
		</div>
	</div>
	</div>
	</div>
	
</div>
</div>



<!-- Page-Level Scripts -->
<script>
	$('document').ready(function(){
		//------------------------------------------------------
	/* 
     * click on addNewServices button to go on services page
     */
	
	
	//---------------------------------------------------------------------
    /**
     * This script is used to remove ads from the list
     */
	$('body').on('click', '.remove_enquiry', function () {
       
        var enquiries_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'admin/enquiry/remove_opportunities_enquiry', {enquiries_id: enquiries_id}, function (response) {
            $('#remove_enquiry').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#remove_enquiry').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"admin/configure_access/saving_rental'>Refress Page</a></div>");

                $('.remove_enquiry[name=' + enquiries_id + ']').closest("tr").remove();
				
				$("#remove_enquiry").fadeTo(2000, 500).slideUp(500, function(){
					$('#remove_enquiry').remove();
					window.location.href = APP_URL+'admin/enquiry/opportunities_enquiry_list';
				});
            }
            else {
                $('#remove_enquiry').html("<div class='alert alert-danger fade in'>button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"admin/configure_access/saving_rental'>Refress Page</a></div>");
				$("#remove_enquiry").fadeTo(2000, 500).slideUp(500, function(){
					$('#remove_enquiry').remove();
				});
            }
        }, 'json');
        return false;
    });
	
	});
</script>
