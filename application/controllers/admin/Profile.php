<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	/**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 2)) {
            redirect(base_url().'admin/admin');
        }		
    }
	
	/**
	 * Index Page(plans) for this controller.
	 */
	public function index()	{
		$segment = 0;
		if(isset($_GET['user_id'])){
			$segment = $_GET['user_id'];
		}else{
			$segment =0;
		}
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
		
		$this->load->model('User_profile_model', 'obj_up', TRUE);
		
		$data['profile_data'] = $this->obj_up->get_profile_details_by_id($segment);
		$data['module_list'] = $this->obj_aa->get_list_of_modules();
		
		
		$this->load->view('admin/components/header');
		$this->load->view('admin/components/menu',$data);
		$this->load->view('admin/pages/user/add_user_profile',$data);
	    /*if($return_val){
			$this->load->view('admin/pages/user/add_user_profile',$data);
		}else{
			$this->load->view('error_msg');
		}*/
		$this->load->view('admin/components/footer');
	}
	
	public function update_user_password() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);
		
        #load required model
		$this->load->model('User_profile_model', 'obj_emp', TRUE);
        
        $return_val = $this->obj_emp->update_user_password();
		if ($return_val) {
			$arr_response['status'] = SUCCESS;
			$arr_response['message'] = 'Member Password has been updated successfully';
		} else {
			$arr_response['status'] = DB_ERROR;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}
		echo json_encode($arr_response);
    }
	/**
	 * admin Add plan for this controller.
	*/
	public function update_user_profile() {
        //array to store ajax responses
        $arr_response = array('status' => 500);
		//Load Required modal
		$this->load->model('User_profile_model', 'obj_up', TRUE);
		$return_val = $this->obj_up->update_user_profile();
		if ($return_val) {
			$arr_response['status'] = 200;
			if($_POST['user_id'] != 0){
				$arr_response['message'] = 'Profile updated successfully';				
			}else{
				$arr_response['message'] = 'Profile added successfully';	
			}			
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($arr_response);
    }
	/**
	 * admin List of plans for this controller.
	*/
	public function user_profile_list(){
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
		$this->load->model('User_profile_model', 'obj_up', TRUE);
		
		$data['profile_list'] = $this->obj_up->get_all_user_profile_list();
		$data['module_list'] = $this->obj_aa->get_list_of_modules();
		$data['submodule_list'] = $this->obj_aa->get_list_of_submodules();
		//$data['category_list'] = $this->obj_qa->get_list_of_categories();
	    
		
		$this->load->view('admin/components/header');
		$this->load->view('admin/components/menu',$data);
		
		if($return_val){
			$this->load->view('admin/pages/user/user_profile_list',$data);
		}else{
			$this->load->view('error_msg');
		}
		$this->load->view('admin/components/footer');
		
	}
	
	/**
	 * admin remove plan for this controller.
	*/
	public function remove_user_profile() {
        //array to store ajax responses
        $arr_response = array('status' => 500);
		//Load Required modal
		$this->load->model('User_profile_model', 'obj_up', TRUE);
		$return_val = $this->obj_up->remove_user_profile();
		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Profile removed successfully';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($arr_response);
    }
	
	
		//--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_image() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 20485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);

            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }
}
