<?php

if (!defined('BASEPATH'))
    exit('Not a valid request!');
	
/**
 * Controller class to configure RBAC system.
 * 
 */
class configure_access extends CI_Controller {

    /**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 2)) {
            redirect(base_url().'admin/');
        }
		//Load Required modal
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);
		$this->load->model('Page_validation_model', 'obj_pvm', TRUE);
    }

    //-------------------------------------------------------------
    /**
     * This function is used to load admin dashboard
     * 
     * @access		public
     * @since		1.0.0
     */
    public function index1() {
		echo 'configure_access';
	}
	
	//-------------------------------------------------------------
    /**
     * This function is used to dashborad page
     * 
     * @access		public
     * @since		1.0.0
     */
    public function index() {		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
		$this->load->view('admin/dashboard_header',$data);
		if($return_val){
			$this->load->view('admin/dashboard',$data);
		}else{
			$this->load->view('error_msg');
		}
		$this->load->view('admin/dashboard_footer');
	}
	
	//-------------------------------------------------------------
    /**
     * This function is used to change_password page
     * 
     * @access		public
     * @since		1.0.0
     */
    public function change_password() {		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
		$this->load->view('admin/dashboard_header',$data);
		if($return_val){
			$this->load->view('admin/change_password',$data);
		}else{
			$this->load->view('error_msg');
		}
		$this->load->view('admin/dashboard_footer');
	}
	
	//--------------------------------------------------------------
    /**
     * This function is used to to update password.
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_password() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

		//Load Required modal
		$this->load->model('admin/configure_access_model', 'obj_ca', TRUE);

		$return_val = $this->obj_ca->update_password();
		if ($return_val) {
			$arr_response['status'] = SUCCESS;
			$arr_response['message'] = 'Password has been updated successfully';
		} else {
			$arr_response['status'] = DB_ERROR;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($arr_response);
    }
	
	//-------------------------------------------------------------
    /**
     * This function is used to load page for profile
     * 
     * @access		public
     * @since		1.0.0
     */
    public function profile() {
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
		$this->load->view('admin/dashboard_header',$data);
		
		
		if($return_val){
			$this->load->view('admin/pages/profile',$data);
		}else{
			$this->load->view('error_msg');
		}
		
		$this->load->view('admin/dashboard_footer');
    }
	
	
	//-------------------------------------------------------------------------
    /**
     * Controller to handle logout request
     * 
     * @version 0.0.1
     * @since 0.0.1
     */
    public function logout() {
        $this->session->sess_destroy();
        $arr_response['redirect'] = base_url() . 'admin/admin/';
        echo json_encode($arr_response);
    }
	
	//--------------------------------------------------------------------------
    /**
     * This function is used to uploaded profile image.
     */
     public function upload_csvFile() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file
        if (($ext == "csv" || $ext == "CSV")&& ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_profile'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);            
            
			$arr_response['status'] = SUCCESS;
			$arr_response['filename'] = $filename;
            
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File.";
        }
        echo json_encode($arr_response);
    }
	
	//------------------------------------------------------------------------
    /*
     * This function is used to do parsing the csv file
     */
    public function html_parser_csv_file() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);
		
		ini_set('memory_limit', '-1');
        #load required model
		$this->load->model('admin/Configure_access_model', 'obj_emp', TRUE);
        
        $return_val = $this->obj_emp->read_csv();
		if ($return_val) {
			$arr_response['status'] = SUCCESS;
			$arr_response['message'] = 'file has been updated successfully';
		} else {
			$arr_response['status'] = DB_ERROR;
			$arr_response['message'] = 'Failed to upload! Please try again';
		}
		echo json_encode($arr_response);
    }
	public function upload_image() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 20485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            if(isset($_POST['sub_folder_name']) && $_POST['sub_folder_name']!=''){
                $target = './uploads/'.$_POST['sub_folder_name'].'/' . $filename;
            }else{
                $target = './uploads/' . $filename;
            }
            
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);

            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }
	
	public function add_gallery(){
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
        $this->load->model('Configure_access_model', 'obj_re', TRUE);
		$data['gallery_list'] = $this->obj_re->get_list_of_gallerys();
        
        $this->load->view('admin/components/header');
        $this->load->view('admin/components/menu',$data);
        
		if($return_val){
			$this->load->view('admin/pages/add_gallery',$data);
		}else{
			$this->load->view('error_msg');
		}
        $this->load->view('admin/components/footer');
        
    }
	 public function update_gallery() {
        $this->load->model('Configure_access_model', 'obj_re', TRUE);
        $arr_response = array('status' => 500);

            $return_val = $this->obj_re->update_gallery();
            if ($return_val) {
                $arr_response['status'] = 200;
                $arr_response['message'] = 'Image has been added In Gallery successfully';
            } else {
                $arr_response['status'] = 201;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }
	public function remove_gallery() {
        #array to hold value to be display
        $arr_response = array();
		$this->load->model('Configure_access_model', 'obj_re', TRUE);
        $return_val = $this->obj_re->remove_gallery();
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = "Image has been removed successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	public function update_public_gallery() {
        $this->load->model('Configure_access_model', 'obj_re', TRUE);
        $arr_response = array('status' => 500);

            $return_val = $this->obj_re->update_public_gallery();
            if ($return_val) {
                $arr_response['status'] = 200;
                $arr_response['message'] = 'Image has been added In Gallery successfully';
            } else {
                $arr_response['status'] = 201;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }
	
	
	public function add_gallery_with_link(){
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
		$this->load->model('Configure_access_model', 'obj_re', TRUE);
		$data['gallery_list'] = $this->obj_re->get_list_of_public_gallerys();
		
		$this->load->view('admin/components/header');
        $this->load->view('admin/components/menu',$data);
		
		if($return_val){
			$this->load->view('admin/pages/add_gallery_with_link',$data);
		}else{
			$this->load->view('error_msg');
		}
		$this->load->view('admin/components/footer');
		
	}
	
	public function remove_public_gallery() {
        #array to hold value to be display
        $arr_response = array();
		$this->load->model('Configure_access_model', 'obj_re', TRUE);
        $return_val = $this->obj_re->remove_public_gallery();
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = "Image has been removed successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	public function contact_us_list(){
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
        $this->load->model('Configure_access_model', 'obj_re', TRUE);
        $data['contact_lists'] = $this->obj_re->get_list_of_contact_us();
        var_dump($data);
        $this->load->view('admin/components/header');
        $this->load->view('admin/components/menu',$data);
       
		if($return_val){
			$this->load->view('admin/pages/contact_us_list',$data);
		}else{
			$this->load->view('error_msg');
		}
        $this->load->view('admin/components/footer');  
    }
	
    public function remove_contact_us() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_re', TRUE);
        $return_val = $this->obj_re->remove_contact_us();
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = "Contact has been removed successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	public function public_content() {    
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
        $content_cat_id = 0;
        if(isset($_GET['content_cat_id'])){
            $content_cat_id = explode('-',$_GET['content_cat_id'])[0];
        }
		
        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_re', TRUE);
        $data['content_detail'] = $this->obj_re->get_public_content_by_content_cat_id($content_cat_id);

        $this->load->view('admin/components/header');
        $this->load->view('admin/components/menu',$data);
      
		if($return_val){
			$this->load->view('admin/pages/public_content',$data);
		}else{
			$this->load->view('error_msg');
		}
        $this->load->view('admin/components/footer');
    }
	public function update_public_content() {
        //array to store ajax responses
        $arr_response = array('status' => 500);
        $this->load->model('Configure_access_model', 'obj_re', TRUE);
        
        $return_val = $this->obj_re->update_public_content();
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = 'Content has been updated successfully';
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	
	
}

  