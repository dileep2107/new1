<?php

if (!defined('BASEPATH'))
    exit('Not a valid request!');
	
/**
 * Controller class to configure RBAC system.
 * 
 */
class Account extends CI_Controller {

    /**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 2)) {
            redirect(base_url().'admin');
        }
		//Load Required modal
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
		//$this->load->model('admin/page_validation_model', 'obj_pvm', TRUE);
    }
	
	//-------------------------------------------------------------
    /**
     * This function is used to load module page by rohit raj
     * 
     * @access		public
     * @since		1.0.0
     */
    public function module() {
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		
		//Load Required modal
		$data['module_list'] = $this->obj_aa->get_list_of_modules();
		$this->load->view('admin/components/header');
		$this->load->view('admin/components/menu',$data);
	    //if($return_val){
			 $this->load->view('admin/pages/account/module',$data);
		//}else{
			//$this->load->view('error_msg');
		//}
		$this->load->view('admin/components/footer');
		
    }
	
	
	
	/**
     * Handles requests for update_category details in database.
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_module() {
        //array to store ajax responses
        //$arr_response = array('status' => ERR_DEFAULT);

            $return_val = $this->obj_aa->update_module();
            if ($return_val) {
                $arr_response['status'] = 200;
                $arr_response['message'] = 'Module has been added successfully';
            } else {
                $arr_response['status'] = 201;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_module from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_module() {
        #array to hold value to be display
        $arr_response = array();
        
        $return_val = $this->obj_aa->remove_module();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Module has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	//-------------------------------------------------------------
    /**
     * This function is used to load tag page by ?Rohit raj
     * 
     * @access		public
     * @since		1.0.0
     */
     public function submodule() {
		
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		//Load Required modal
		
		$data['module_list'] = $this->obj_aa->get_list_of_modules();
		$data['submodule_list'] = $this->obj_aa->get_list_of_submodules();
		$this->load->view('admin/components/header');
		$this->load->view('admin/components/menu',$data);
	    if($return_val){
			$this->load->view('admin/pages/account/submodule',$data);
		}else{
			$this->load->view('error_msg');
		}
		$this->load->view('admin/components/footer');
		
		
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_submodule from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_submodule() {
        #array to hold value to be display
        $arr_response = array();
        
        $return_val = $this->obj_aa->remove_submodule();
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = "Sub Module has been removed successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	
	/**
     * Handles requests for update_submodule details in database.
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_submodule() {
        //array to store ajax responses
        $arr_response = array('status' => 500);

            $return_val = $this->obj_aa->update_submodule();
            if ($return_val) {
                $arr_response['status'] = 200;
                $arr_response['message'] = 'Sub Module has been added successfully';
            } else {
                $arr_response['status'] = 201;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }
	
	//------------------------------------------------------------------------------
    /**
     * This function is used to check weather entered email is already exists or not.
     */
    public function check_availability() {

        #array to hold the response values to be displayed
        $arr_response = array();

        #default response status message
        $arr_response['status'] = ERR_DEFAULT;

        #checking if parameter already exists
        $result = $this->obj_aa->check_availability();
        if ($result) {
            echo 'true';
        } else {
            echo 'false';
        }
        //echo json_encode($arr_response);
    }
	
    
	//-------------------------------------------------------------
    /**
     * This function is used to dashborad page
     * 
     * @access		public
     * @since		1.0.0
     */
    public function emp() {		
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		$this->load->model('admin/Question_access_model', 'obj_qa', TRUE);
		
		$data['module_list'] = $this->obj_aa->get_list_of_modules();
		$data['submodule_list'] = $this->obj_aa->get_list_of_submodules();
		
        $data['emp_subject'] = $this->obj_aa->get_list_of_emp_subject_access_by_id($user_id);
		
		$data['category_list'] = $this->obj_qa->get_list_of_categories();
		
		$data['emp_list'] = $this->obj_aa->get_list_of_employee();
		
		$this->load->view('admin/dashboard_header',$data);
		if($return_val){
			$this->load->view('admin/emp_detail',$data);
		}else{
			$this->load->view('error_msg');
		}
		$this->load->view('admin/dashboard_footer');
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_emp from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_emp() {
        #array to hold value to be display
        $arr_response = array();
        
        $return_val = $this->obj_aa->update_emp();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Employee has been added successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_emp from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_emp() {
        #array to hold value to be display
        $arr_response = array();
        
        $return_val = $this->obj_aa->remove_emp();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Employee has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_accessibility from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_accessibility() {
        #array to hold value to be display
        $arr_response = array();
        
        $return_val = $this->obj_aa->update_accessibility();
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = "Accessiblity has been updated successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_emp_accessibility_detail from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function get_emp_accessibility_detail() {
        $emp_id = $_POST['emp_id'];
		#array to hold value to be display
        $arr_response = array();
        
        $return_val = $this->obj_aa->get_list_of_emp_access_by_id($emp_id);
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['data'] = $return_val;
            $arr_response['message'] = "Accessiblity Details has been fetched successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	
	
	
	
	
	
    
	
	
	
	
	
	
	
}

  