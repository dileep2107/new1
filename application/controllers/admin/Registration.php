<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {
	/**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 2)) {
            redirect(base_url().'admin/admin');
        }
		//Load Required modal
	
	}
	
	
    public function index() {
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		/*reading the blog id by segment*/
		if(isset($_GET['blog_id'])){
			$segment = $_GET['blog_id'];
		}else{
			$segment =0;
		}
	
	    $this->load->view('admin/components/header');
		//$this->load->view('admin/components/menu',$data);
		
		 if($return_val){
			$this->load->view('admin/pages/registration/registration',$data);
		}else{
			$this->load->view('error_msg');
		}
		
		$this->load->view('admin/components/footer');
		
    }
	
	public function delete_candidate(){
		$candidate_id = $_POST['candidate_id'];
		if($candidate_id){
			$this->db->delete('candidate_registration' ,array('id'=>$candidate_id));
			$this->db->delete('basic_foundation_course' ,array('candidate_id'=>$candidate_id));
			$this->db->delete('candidate_family_info' ,array('candidate_id'=>$candidate_id));
			$this->db->delete('candidate_trg_bn_cdr_ppt_test' ,array('candidate_id'=>$candidate_id));
			$this->db->delete('commandent_test_bpet' ,array('candidate_id'=>$candidate_id));
			$this->db->delete('commandent_test_firing' ,array('candidate_id'=>$candidate_id));
			$this->db->delete('commandent_test_firing_another' ,array('candidate_id'=>$candidate_id));
			$this->db->delete('map_reading_test' ,array('candidate_id'=>$candidate_id));
			$this->db->delete('trg_bn_cdr_test' ,array('candidate_id'=>$candidate_id));
		 
		}else{
			
		}
		$arr_response['status'] = 200;
		$arr_response['message'] = 'Candidate information successfully deleted';
	  echo json_encode($arr_response);
	}
	
	 public function edit_candidate() {
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		/*reading the blog id by segment*/
		if(isset($_GET['id'])){
			$segment = $_GET['id'];
		}else{
			$segment =0;
		}
		$data['candidate_id'] = $segment;
		$data['candidate_info'] = $this->db->get_where('candidate_registration' , array('id' =>$segment ))->result();
		$data['candidate_family_info'] = $this->db->get_where('candidate_family_info' , array('candidate_id' =>$segment ))->result();
		$data['candidate_cdr_ppt_test'] = $this->db->get_where('candidate_trg_bn_cdr_ppt_test' , array('candidate_id' =>$segment ))->result();
		$data['trg_bn_cdr_test'] = $this->db->get_where('trg_bn_cdr_test' , array('candidate_id' =>$segment ))->result();
		$data['commandent_test_bpet'] = $this->db->get_where('commandent_test_bpet' , array('candidate_id' =>$segment ))->result();
		$data['commandent_test_firing'] = $this->db->get_where('commandent_test_firing' , array('candidate_id' =>$segment ))->result();
		$data['commandent_test_firing_another'] = $this->db->get_where('commandent_test_firing_another' , array('candidate_id' =>$segment ))->result();
		$data['basic_foundation_course'] = $this->db->get_where('basic_foundation_course' , array('candidate_id' =>$segment ))->result();
		$data['map_reading_test'] = $this->db->get_where('map_reading_test' , array('candidate_id' =>$segment ))->result();
	   
	   $this->load->view('admin/components/header');
		//$this->load->view('admin/components/menu',$data);
		
		 if($return_val){
			$this->load->view('admin/pages/registration/edit_registration',$data);
		}else{
			$this->load->view('error_msg');
		}
		
		$this->load->view('admin/components/footer');
		
    }
	
	
    public function add_candidate() {
      $data = array(
		     'candidate_army_no'=>$_POST['candidate_army_no'],
		     'candidate_course'=>$_POST['candidate_course'],
		     'candidate_company'=>$_POST['candidate_company'],
		     'candidate_name'=>$_POST['candidate_name'],
		     'candidate_section'=>$_POST['candidate_section'],
		     'candidate_year'=>$_POST['candidate_year'],
		     'candidate_rank'=>$_POST['candidate_rank'],
		     'candidate_platoon'=>$_POST['candidate_platoon'],
		     'candidate_aro_uhq'=>$_POST['candidate_aro_uhq'],
		     'candidate_dob'=>$_POST['candidate_dob'],
		     'candidate_doe'=>$_POST['candidate_doe'],
		     'candidate_poe'=>$_POST['candidate_poe'],
		     'candidate_address'=>$_POST['candidate_address'],
		     'candidate_blood_group'=>$_POST['candidate_blood_group'],
		     'candidate_religion'=>$_POST['candidate_religion'],
		     'candidate_caste'=>$_POST['candidate_caste'],
		     'candidate_village'=>$_POST['candidate_village'],
		     'candidate_post_office'=>$_POST['candidate_post_office'],
		     'candidate_district'=>$_POST['candidate_district'],
		     'candidate_state'=>$_POST['candidate_state'],
		     'create_date'=> date('Y-m-d'),
		     'create_time'=> date('H:i:s'),
		     'instructer_info'=>json_encode($_POST['instructer_info'][0]),
		     'edu_info'=>json_encode($_POST['edu_info'][0]),
		     'physical_parameters'=>json_encode($_POST['physical_parameters'][0]),
		   );
		   
		$this->db->insert('candidate_registration' , $data);
		  $candidate_id = $this->db->insert_id();
		  $data_tdr_test = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['trg_arr']),
		               'second_attempt_test'=>json_encode($_POST['trg_2nd']),
		               'third_attempt_test'=>json_encode($_POST['trg_3rd']),
		                   );
		  $this->db->insert('candidate_trg_bn_cdr_ppt_test' , $data_tdr_test);
		  
		  
		  ///
		   $data_tdr_bn_test = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['trg_bn']),
		               'second_attempt_test'=>json_encode($_POST['trg_bn_2nd']),
		               'third_attempt_test'=>json_encode($_POST['trg_bn_3rd']),
		                   );
		  $this->db->insert('trg_bn_cdr_test' , $data_tdr_bn_test);
		  ///
		   $data_commandent_test_bpet = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['commandent_test_bpet']),
		               'second_attempt_test'=>json_encode($_POST['commandent_test_bpet_2nd']),
		               'third_attempt_test'=>json_encode($_POST['commandent_test_bpet_3rd']),
		                   );
		  $this->db->insert('commandent_test_bpet' , $data_commandent_test_bpet);
		    ///
		   $data_commandent_test_firing = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['test_firing']),
		               'second_attempt_test'=>json_encode($_POST['test_firing_2nd']),
		               'third_attempt_test'=>json_encode($_POST['test_firing_3rd']),
		                   );
		  $this->db->insert('commandent_test_firing' , $data_commandent_test_firing);
		     ///
		   $data_commandent_test_firing_another = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['test_firing_other']),
		               'second_attempt_test'=>json_encode($_POST['test_firing_other_2nd']),
		               'third_attempt_test'=>json_encode($_POST['test_firing_other_3rd']),
		                   );
		  $this->db->insert('commandent_test_firing_another' , $data_commandent_test_firing_another);
		    ///
		   $data_basic_foundation_course = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['basic_course']),
		               'second_attempt_test'=>json_encode($_POST['basic_course_2nd']),
		               'third_attempt_test'=>json_encode($_POST['basic_course_3rd']),
		                   );
		  $this->db->insert('basic_foundation_course' , $data_basic_foundation_course);
           ///		  
		  $map_reading_test = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['map_reading']),
		               'second_attempt_test'=>json_encode($_POST['map_reading_2nd']),
		               'third_attempt_test'=>json_encode($_POST['map_reading_3rd']),
		                   );
		  $this->db->insert('map_reading_test' , $map_reading_test);
		  
		  
		  if($_POST['family_data']){
			  foreach($_POST['family_data'] as $val){
				
				  $data = array(
				         'candidate_id'=>$candidate_id,
				         'candidate_relation'=>$val['candidate_relation'],
				         'candidate_relation_name'=>$val['candidate_relation_name'],
				         'candidate_relation_dob'=>$val['candidate_relation_dob'],
				         'candidate_relation_age'=>$val['candidate_relation_age'],
				           );
				$this->db->insert('candidate_family_info', $data);
			  }
		  }
		  
		$arr_response['status'] = 200;
		$arr_response['candidate_id'] = $candidate_id;
		$arr_response['message'] = "Candidate registration successfully";
		
		echo json_encode($arr_response);
		
    }
	 public function update_candidate() {
		$candidate_id = $_POST['candidate_id'];
      $data = array(
		     'candidate_army_no'=>$_POST['candidate_army_no'],
		     'candidate_course'=>$_POST['candidate_course'],
		     'candidate_company'=>$_POST['candidate_company'],
		     'candidate_name'=>$_POST['candidate_name'],
		     'candidate_section'=>$_POST['candidate_section'],
		     'candidate_year'=>$_POST['candidate_year'],
		     'candidate_rank'=>$_POST['candidate_rank'],
		     'candidate_platoon'=>$_POST['candidate_platoon'],
		     'candidate_aro_uhq'=>$_POST['candidate_aro_uhq'],
		     'candidate_dob'=>$_POST['candidate_dob'],
		     'candidate_doe'=>$_POST['candidate_doe'],
		     'candidate_poe'=>$_POST['candidate_poe'],
		     'candidate_address'=>$_POST['candidate_address'],
		     'candidate_blood_group'=>$_POST['candidate_blood_group'],
		     'candidate_religion'=>$_POST['candidate_religion'],
		     'candidate_caste'=>$_POST['candidate_caste'],
		     'candidate_village'=>$_POST['candidate_village'],
		     'candidate_post_office'=>$_POST['candidate_post_office'],
		     'candidate_district'=>$_POST['candidate_district'],
		     'candidate_state'=>$_POST['candidate_state'],
		     'create_date'=> date('Y-m-d'),
		     'create_time'=> date('H:i:s'),
		     'instructer_info'=>json_encode($_POST['instructer_info'][0]),
		     'edu_info'=>json_encode($_POST['edu_info'][0]),
		     'physical_parameters'=>json_encode($_POST['physical_parameters'][0]),
		   );
		   
		$this->db->update('candidate_registration' , $data , array('id'=>$candidate_id));
		 
		  $data_tdr_test = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['trg_arr']),
		               'second_attempt_test'=>json_encode($_POST['trg_2nd']),
		               'third_attempt_test'=>json_encode($_POST['trg_3rd']),
		                   );
		  $this->db->update('candidate_trg_bn_cdr_ppt_test' , $data_tdr_test, array('candidate_id'=>$candidate_id));
		  
		  
		  ///
		   $data_tdr_bn_test = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['trg_bn']),
		               'second_attempt_test'=>json_encode($_POST['trg_bn_2nd']),
		               'third_attempt_test'=>json_encode($_POST['trg_bn_3rd']),
		                   );
		  $this->db->update('trg_bn_cdr_test' , $data_tdr_bn_test, array('candidate_id'=>$candidate_id));
		  ///
		   $data_commandent_test_bpet = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['commandent_test_bpet']),
		               'second_attempt_test'=>json_encode($_POST['commandent_test_bpet_2nd']),
		               'third_attempt_test'=>json_encode($_POST['commandent_test_bpet_3rd']),
		                   );
		  $this->db->update('commandent_test_bpet' , $data_commandent_test_bpet, array('candidate_id'=>$candidate_id));
		    ///
		   $data_commandent_test_firing = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['test_firing']),
		               'second_attempt_test'=>json_encode($_POST['test_firing_2nd']),
		               'third_attempt_test'=>json_encode($_POST['test_firing_3rd']),
		                   );
		  $this->db->update('commandent_test_firing' , $data_commandent_test_firing, array('candidate_id'=>$candidate_id));
		     ///
		   $data_commandent_test_firing_another = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['test_firing_other']),
		               'second_attempt_test'=>json_encode($_POST['test_firing_other_2nd']),
		               'third_attempt_test'=>json_encode($_POST['test_firing_other_3rd']),
		                   );
		  $this->db->update('commandent_test_firing_another' , $data_commandent_test_firing_another , array('candidate_id'=>$candidate_id));
		    ///
		   $data_basic_foundation_course = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['basic_course']),
		               'second_attempt_test'=>json_encode($_POST['basic_course_2nd']),
		               'third_attempt_test'=>json_encode($_POST['basic_course_3rd']),
		                   );
		  $this->db->update('basic_foundation_course' , $data_basic_foundation_course , array('candidate_id'=>$candidate_id));
           ///	
          		   
		  $map_reading_test = array(
		               'candidate_id'=>$candidate_id,
		               'first_attempt_test'=>json_encode($_POST['map_reading']),
		               'second_attempt_test'=>json_encode($_POST['map_reading_2nd']),
		               'third_attempt_test'=>json_encode($_POST['map_reading_3rd']),
		                   );
		  $this->db->update('map_reading_test' , $map_reading_test , array('candidate_id'=>$candidate_id));
		  
		  
		  if($_POST['family_data']){
			  $this->db->delete('candidate_family_info' , array('candidate_id'=>$candidate_id));
			  
			  foreach($_POST['family_data'] as $val){
				
				  $data = array(
				         'candidate_id'=>$candidate_id,
				         'candidate_relation'=>$val['candidate_relation'],
				         'candidate_relation_name'=>$val['candidate_relation_name'],
				         'candidate_relation_dob'=>$val['candidate_relation_dob'],
				         'candidate_relation_age'=>$val['candidate_relation_age'],
				           );
				$this->db->insert('candidate_family_info', $data);
			  }
		  }
		  
		$arr_response['status'] = 200;
		$arr_response['message'] = "Candidate registration successfully";
		
		echo json_encode($arr_response);
		
    }
	
	
	
	public function candidate_list(){
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		$filter_search = '';
		if( isset($_GET['ano']) OR isset($_GET['rank']) OR isset($_GET['name']) OR isset($_GET['course']) OR isset($_GET['section']) OR isset($_GET['platoon'])OR isset($_GET['campany'])  OR isset($_GET['arq'])){
			$filter_search .= 'where';
			
		}
		if(isset($_GET['ano'])){
			$ano = $_GET['ano'];
			$filter_search .= " candidate_army_no = $ano AND"; 
			$data['ano'] = $ano;
		}else{
			$data['ano'] = '';
		}
		if(isset($_GET['rank'])){
			$rank = $_GET['rank'];
			$filter_search .= "   candidate_rank = '$rank' AND"; 
			$data['rank'] = $rank;
		}else{
			$data['rank'] = '';
		}
		if(isset($_GET['name'])){
			$name = $_GET['name'];
			$filter_search .= "  candidate_name = '$name' AND"; 
			$data['name'] = $name;
		}else{
			$data['name'] = '';
		}
		if(isset($_GET['course'])){
			$course = $_GET['course'];
			$filter_search .= "  candidate_course = '$course' AND"; 
			$data['course'] = $course;
		}else{
			$data['course'] = '';
		}
		if(isset($_GET['section'])){
			$section = $_GET['section'];
			$filter_search .= "  candidate_section = '$section' AND"; 
			$data['section'] = $section;
		}else{
			$data['section'] ='';
		}
		if(isset($_GET['platoon'])){
			$platoon = $_GET['platoon'];
			$filter_search .= "  candidate_platoon = '$platoon' AND"; 
			$data['platoon'] = $platoon;
		}else{
			$data['platoon'] = '';
		}
		if(isset($_GET['campany'])){
			$campany = $_GET['campany'];
			$filter_search .= "  candidate_company = '$campany' AND"; 
			$data['campany'] = $campany;
		}else{
			$data['campany'] = '';
		}
		if(isset($_GET['arq'])){
			$arq = $_GET['arq'];
			$filter_search .= "  candidate_aro_uhq = '$arq' AND"; 
			$data['arq'] = $arq;
		}else{
			$data['arq'] = '';
		}
		$filter = substr($filter_search, 0, -3);
		
		$sql = "select * from candidate_registration  $filter";
		
		$data['candidate_list'] = $this->db->query($sql)->result();
	   
		
		$this->load->view('admin/components/header');
		$this->load->view('admin/components/menu',$data);
	
		if($return_val){
			$this->load->view('admin/pages/registration/candidate_list',$data);
		}else{
			$this->load->view('error_msg');
		}
		$this->load->view('admin/components/footer');
		
	}
	
	
	public function print_view() {
		$this->load->model('page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		/*reading the blog id by segment*/
		if(isset($_GET['id'])){
			$segment = $_GET['id'];
		}else{
			$segment =0;
		}
		$data['candidate_id'] = $segment;
		$data['candidate_info'] = $this->db->get_where('candidate_registration' , array('id' =>$segment ))->result();
		$data['candidate_family_info'] = $this->db->get_where('candidate_family_info' , array('candidate_id' =>$segment ))->result();
		$data['candidate_cdr_ppt_test'] = $this->db->get_where('candidate_trg_bn_cdr_ppt_test' , array('candidate_id' =>$segment ))->result();
		$data['trg_bn_cdr_test'] = $this->db->get_where('trg_bn_cdr_test' , array('candidate_id' =>$segment ))->result();
		$data['commandent_test_bpet'] = $this->db->get_where('commandent_test_bpet' , array('candidate_id' =>$segment ))->result();
		$data['commandent_test_firing'] = $this->db->get_where('commandent_test_firing' , array('candidate_id' =>$segment ))->result();
		$data['commandent_test_firing_another'] = $this->db->get_where('commandent_test_firing_another' , array('candidate_id' =>$segment ))->result();
		$data['basic_foundation_course'] = $this->db->get_where('basic_foundation_course' , array('candidate_id' =>$segment ))->result();
		$data['map_reading_test'] = $this->db->get_where('map_reading_test' , array('candidate_id' =>$segment ))->result();
	   
	   $this->load->view('admin/components/header');
		//$this->load->view('admin/components/menu',$data);
		
		 if($return_val){
			$this->load->view('admin/pages/registration/print_view',$data);
		}else{
			$this->load->view('error_msg');
		}
		
		$this->load->view('admin/components/footer');
		
    }
	
	
	

}
