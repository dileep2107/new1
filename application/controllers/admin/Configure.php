<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configure extends CI_Controller {

	/**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 2)) {
            redirect(base_url().'admin/admin');
        }		
    }
	
	/**
	 * Admin dasbhoard page.
	 */
	public function index()	{
		$this->load->model('Page_validation_model', 'obj_pvm', TRUE);
		
		$user_id = $this->session->userdata('userID');
		$data['access_detail'] = $this->obj_pvm->get_user_access_detail_given_user($user_id);
		$data['access_module'] = $this->obj_pvm->get_user_access_module_of_given_user($user_id);
		$return_val = $this->obj_pvm->is_permitted();
		//var_dump($data);
		$this->load->view('admin/components/header');
		$this->load->view('admin/components/menu',$data);
		$this->load->view('admin/pages/dashboard');
		$this->load->view('admin/components/footer');
	}
}
